package com.orbita.agendaComercialWS.services;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.AgendarCitaMasivo_IN;
import com.orbita.agendaComercialWS.dto.request.AgendarCita_IN;
import com.orbita.agendaComercialWS.dto.request.ValidarCliente_IN;
import com.orbita.agendaComercialWS.dto.response.AgendarCita_OUT;
import com.orbita.agendaComercialWS.dto.response.ValidarCliente_OUT;

public interface ConsultaCrediticiaService {

	public AgendarCita_OUT agendarCita_OUT(AgendarCita_IN agendarCita_IN) throws SQLException;

	public AgendarCita_OUT agendarCitaMasivo_OUT(AgendarCitaMasivo_IN agendarCitaMasivo_IN) throws SQLException;

	public ValidarCliente_OUT validarCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException;
	
	public ValidarCliente_OUT validarEjecutivoCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException;
	
}