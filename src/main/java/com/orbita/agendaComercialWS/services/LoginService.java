package com.orbita.agendaComercialWS.services;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.ValidarUsuarioAC_IN;
import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;

public interface LoginService {
	 ValidarUsuarioAC_OUT validarUsuarioAC_OUT(ValidarUsuarioAC_IN validarUsuarioAC_IN) throws SQLException;
}