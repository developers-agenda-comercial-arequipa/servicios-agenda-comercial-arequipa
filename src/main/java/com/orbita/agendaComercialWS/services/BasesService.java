package com.orbita.agendaComercialWS.services;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;

public interface BasesService {

	public ListarDepartamentos_OUT listarDepartamentos_OUT(ListarDepartamentos_IN listarDepartamentos_IN)
			throws SQLException;

	public ListarProvincias_OUT listarProvincias_OUT(ListarProvincias_IN listarProvincias_IN) throws SQLException;

	public ListarDistritos_OUT listarDistritos_OUT(ListarDistritos_IN listarDistritos_IN) throws SQLException;

	public ListarTiposDireccion_OUT listarTiposDireccion_OUT(ListarTiposDireccion_IN listarTiposDireccion_IN)
			throws SQLException;

	//public GuardarClienteGeo_OUT guardarClienteGeo_OUT(GuardarClienteGeo_IN guardarClienteGeo_IN) throws SQLException;

	public GuardarClienteGeoC_OUT guardarClienteGeoC_OUT(GuardarClienteGeoC_IN guardarClienteGeoC_IN)
			throws SQLException;
	
	public ListarCantidadBases_OUT listarCantidadBases_OUT(ListarCantidadCartera_IN listarCantidadCartera_IN)
			throws SQLException;
	
	public ListarBasesDetalle_OUT listarBasesDetalle_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException;

	public ListarBasesDetalle_OUT listarOportunidadXFiltro_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException;

	public ListarUltimosPagos_OUT listarUltimosPagos_OUT(ListarInfoCliente_IN listarInfoCliente_IN)
			throws SQLException;
}