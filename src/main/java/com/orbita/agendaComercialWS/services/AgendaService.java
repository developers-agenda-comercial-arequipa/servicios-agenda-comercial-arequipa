package com.orbita.agendaComercialWS.services;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;

public interface AgendaService {
	public ListarCantidadCitasCalendario_OUT listarCitasPorMes_OUT(
			ListarCantidadCitasCalendario_IN listarCitasPorMes_IN) throws SQLException;

	public ListarCitasPorDia_OUT listarCitasPorDia_OUT(ListarCitasPorDia_IN listarCitasPorDia_IN) throws SQLException;
	
	public ListarCitasPorDiaGeo_OUT listarCitasPorDiaGeo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException;

	public ObtenerDetalleCita_OUT obtenerDetalleCita_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException;

	public ListarMotivosNoContactado_OUT listarMotivosNoContactados_OUT(
			ListarMotivosNoContactado_IN listarMotivosNoContactados_IN) throws SQLException;

	public ListarResultadosCita_OUT listarResultadosCita_OUT(ListarResultadosCita_IN listarResultadosCita_IN)
			throws SQLException;

	public ListarResultadosCitaDetalle_OUT listarResultadosCitaDetalle_OUT(
			ListarResultadosCitaDetalle_IN listarResultadosCitaDetalle_IN) throws SQLException;

	public GuardarResultadoCitaNC_OUT guardarResultadoCitaNC_OUT(GuardarResultadoCitaNC_IN guardarResultadoCitaNC_IN)
			throws SQLException;

	public GuardarResultadoCitaSC_OUT guardarResultadoCitaSC_OUT(GuardarResultadoCitaSC_IN guardarResultadoCitaSC_IN)
			throws SQLException;
	
	public GuardarFotoCita_OUT guardarFotoCita_OUT(GuardarFotoCita_IN guardarFotoCita_IN)
			throws SQLException;

	public ListarCitasPorMes_OUT listarCitasPorMes_OUT(ListarCitasPorMes_IN listarCitasPorMes_IN) throws SQLException;

	public ListarCarteraMapa_OUT listarCarteraMapa_OUT(ListarMiRadar_IN listarMiRadar_IN) throws SQLException;

	public ListarDireccionesCliente_OUT listarDireccionesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException;
	
	public ListarBasesCliente_OUT listarBasesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException;
	
	public ObtenerInfoCliente_OUT obtenerInfoCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException;
	
	public GuardarInfoCliente_OUT guardarInfoCliente_OUT(GuardarInfoCliente_IN guardarInfoCliente_IN)
			throws SQLException;

	public GuardarInfoClienteDireccion_OUT guardarInfoClienteDireccion_OUT(
			GuardarInfoClienteDireccion_IN guardarInfoClienteDireccion_IN) throws SQLException;
	
	public GuardarCliente_OUT guardarCliente_OUT(GuardarCliente_IN guardarCliente_IN)
			throws SQLException;
	
	public ListarCitaResultado_OUT listarCitaResultado_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException;
	
	public ResponseAgenda_OUT bajaInfoCliente_OUT(BajaInfoCliente_IN bajaInfoCliente_IN)
			throws SQLException;
	
	public ListarClienteInfo_OUT listarClienteInfo_OUT(int tipo, ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException;
	
	public ListarClienteTelef_OUT listarClienteTelef_OUT(ListarTelefCliente_IN listarTelefCliente_IN)
			throws SQLException;
	
	public ListarClienteDireccion_OUT listarClienteDireccion_OUT(ListarDireccCliente_IN listarDireccCliente_IN)
			throws SQLException;
	
	public ListarCitaFotos_OUT listarCitaFotos_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException;

	public ListarClienteResultado_OUT listarClienteResultado_OUT(ListarInfoCliente_IN listarInfoCliente_IN)
			throws SQLException;
}