package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMiRadar_IN;
import com.orbita.agendaComercialWS.dto.response.ListarActividadOportunidad_OUT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.OportunidadesDAO;
import com.orbita.agendaComercialWS.dto.request.ListarCarteraInactivo_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerInfoCliente_IN;
import com.orbita.agendaComercialWS.dto.response.ListarBasesOportunidad_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarCarteraInactivo_OUT;
import com.orbita.agendaComercialWS.services.OportunidadesService;

@Service
public class OportunidadesServiceImpl implements OportunidadesService {
	@Autowired
	private OportunidadesDAO clientesInactivosDAO;

	@Override
	@Transactional
	public ListarCarteraInactivo_OUT listarCarteraInactivo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException {
		return clientesInactivosDAO.listarCarteraInactivo_OUT(listarMiRadar_IN);
	}

	@Override
	@Transactional
	public ListarBasesOportunidad_OUT listarOportunindad_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {
		return clientesInactivosDAO.listarBasesOportunidad_OUT(obtenerInfoCliente_IN);
	}

	@Override
	public ListarActividadOportunidad_OUT listarActividadOportunidad_OUT(General_IN general_IN) throws SQLException {
		return clientesInactivosDAO.listarActividadOportunidad_OUT(general_IN);
	}

}