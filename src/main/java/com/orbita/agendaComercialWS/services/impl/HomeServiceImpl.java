package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.HomeDAO;
import com.orbita.agendaComercialWS.dto.request.GuardarError_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMonitoreoCliente_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerContadorCitas_IN;
import com.orbita.agendaComercialWS.dto.request.Version_IN;
import com.orbita.agendaComercialWS.dto.response.ListarMonitoreoCliente_OUT;
import com.orbita.agendaComercialWS.dto.response.ObtenerContadorCitas_OUT;
import com.orbita.agendaComercialWS.dto.response.ResponseAgenda_OUT;
import com.orbita.agendaComercialWS.dto.response.Version_OUT;
import com.orbita.agendaComercialWS.services.HomeService;

@Service
public class HomeServiceImpl implements HomeService {
	@Autowired
	private HomeDAO homeDAO;

	@Override
	@Transactional
	public ObtenerContadorCitas_OUT obtenerContadorCitas_OUT(ObtenerContadorCitas_IN obtenerContadorCitas_IN)
			throws SQLException {
		return homeDAO.obtenerContadorCitas_OUT(obtenerContadorCitas_IN);
	}

	@Override
	@Transactional
	public ListarMonitoreoCliente_OUT listarMonitoreoCliente_OUT(ListarMonitoreoCliente_IN listarMonitoreoCliente_IN)
			throws SQLException {
		return homeDAO.listarMonitoreoCliente_OUT(listarMonitoreoCliente_IN);
	}
	
	@Override
	@Transactional
	public ResponseAgenda_OUT GuardarError(GuardarError_IN guardarError_IN) throws SQLException {
		return homeDAO.GuardarError(guardarError_IN);
		
	}
	
	@Override
	@Transactional
	public Version_OUT version_OUT(Version_IN version_IN) throws SQLException {
		return homeDAO.version_OUT(version_IN);
	}

}