package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.AgendarCitaMasivo_IN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.ConsultaCrediticiaDAO;
import com.orbita.agendaComercialWS.dto.request.AgendarCita_IN;
import com.orbita.agendaComercialWS.dto.request.ValidarCliente_IN;
import com.orbita.agendaComercialWS.dto.response.AgendarCita_OUT;
import com.orbita.agendaComercialWS.dto.response.ValidarCliente_OUT;
import com.orbita.agendaComercialWS.services.ConsultaCrediticiaService;

@Service
public class ConsultaCrediticiaServiceImpl implements ConsultaCrediticiaService {
	@Autowired
	private ConsultaCrediticiaDAO consultaCrediticiaDAO;

	@Override
	@Transactional
	public AgendarCita_OUT agendarCita_OUT(AgendarCita_IN agendarCita_IN) throws SQLException {
		return consultaCrediticiaDAO.agendarCita_OUT(agendarCita_IN);
	}

	@Override
	public AgendarCita_OUT agendarCitaMasivo_OUT(AgendarCitaMasivo_IN agendarCitaMasivo_IN) throws SQLException {
		return consultaCrediticiaDAO.agendarCitaMasivo_OUT(agendarCitaMasivo_IN);
	}

	@Override
	@Transactional
	public ValidarCliente_OUT validarCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException {
		return consultaCrediticiaDAO.validarCliente_OUT(validarCliente_IN);
	}

	@Override
	@Transactional
	public ValidarCliente_OUT validarEjecutivoCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException {
		return consultaCrediticiaDAO.validarEjecutivoCliente_OUT(validarCliente_IN);
	}
}