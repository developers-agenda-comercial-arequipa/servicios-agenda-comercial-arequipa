package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.BasesDAO;
import com.orbita.agendaComercialWS.services.BasesService;

@Service
public class BasesServiceImpl implements BasesService {
	@Autowired
	private BasesDAO hojaRutaDAO;

	@Override
	@Transactional
	public ListarDepartamentos_OUT listarDepartamentos_OUT(ListarDepartamentos_IN listarDepartamentos_IN)
			throws SQLException {
		return hojaRutaDAO.listarDepartamentos_OUT(listarDepartamentos_IN);
	}

	@Override
	@Transactional
	public ListarProvincias_OUT listarProvincias_OUT(ListarProvincias_IN listarProvincias_IN) throws SQLException {
		return hojaRutaDAO.listarProvincias_OUT(listarProvincias_IN);
	}

	@Override
	@Transactional
	public ListarDistritos_OUT listarDistritos_OUT(ListarDistritos_IN listarDistritos_IN) throws SQLException {
		return hojaRutaDAO.listarDistritos_OUT(listarDistritos_IN);
	}

	@Override
	@Transactional
	public ListarTiposDireccion_OUT listarTiposDireccion_OUT(ListarTiposDireccion_IN listarTiposDireccion_IN)
			throws SQLException {
		return hojaRutaDAO.listarTiposDireccion_OUT(listarTiposDireccion_IN);
	}

	/*@Override
	@Transactional
	public GuardarClienteGeo_OUT guardarClienteGeo_OUT(GuardarClienteGeo_IN guardarClienteGeo_IN) throws SQLException {
		return hojaRutaDAO.guardarClienteGeo_OUT(guardarClienteGeo_IN);
	}*/

	@Override
	@Transactional
	public GuardarClienteGeoC_OUT guardarClienteGeoC_OUT(GuardarClienteGeoC_IN guardarClienteGeoC_IN)
			throws SQLException {
		return hojaRutaDAO.guardarClienteGeoC_OUT(guardarClienteGeoC_IN);
	}

	@Override
	@Transactional
	public ListarCantidadBases_OUT listarCantidadBases_OUT(ListarCantidadCartera_IN listarCantidadCartera_IN)
			throws SQLException {
		return hojaRutaDAO.listarCantidadBases_OUT(listarCantidadCartera_IN);
	}
	
	@Override
	@Transactional
	public ListarBasesDetalle_OUT listarBasesDetalle_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException {
		return hojaRutaDAO.listarBasesDetalle_OUT(listarCarteraDetalle_IN);
	}

	@Override
	@Transactional
	public ListarBasesDetalle_OUT listarOportunidadXFiltro_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException {
		return hojaRutaDAO.listarOportunidadXFiltro_OUT(listarCarteraDetalle_IN);
	}

	@Override
	public ListarUltimosPagos_OUT listarUltimosPagos_OUT(ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException {
		return hojaRutaDAO.listarUltimosPagos_OUT(listarInfoCliente_IN);
	}

}