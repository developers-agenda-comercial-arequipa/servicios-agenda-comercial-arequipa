package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.LoginDAO;
import com.orbita.agendaComercialWS.dto.request.ValidarUsuarioAC_IN;
import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;
import com.orbita.agendaComercialWS.services.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginDAO loginDAO;

	@Override
	@Transactional
	public ValidarUsuarioAC_OUT validarUsuarioAC_OUT(ValidarUsuarioAC_IN validarUsuarioAC_IN) throws SQLException {
		return loginDAO.validarUsuarioAC_OUT(validarUsuarioAC_IN);
	}

}