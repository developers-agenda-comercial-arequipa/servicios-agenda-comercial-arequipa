package com.orbita.agendaComercialWS.services.impl;

import com.orbita.agendaComercialWS.dao.SimuladorDAO;
import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarTarifario_IN;
import com.orbita.agendaComercialWS.dto.response.ListarProductoSegmento_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarTarifario_OUT;
import com.orbita.agendaComercialWS.services.SimuladorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class SimuladorServiceImpl implements SimuladorService {

    @Autowired
    private SimuladorDAO simuladorDAO;

    @Override
    public ListarProductoSegmento_OUT listarProductoSegmento_OUT(General_IN general_IN) throws SQLException {
        return simuladorDAO.listarProductoSegmento_OUT(general_IN);
    }

    @Override
    public ListarTarifario_OUT listarTarifario_OUT(ListarTarifario_IN listarTarifario_IN) throws SQLException {
        return simuladorDAO.listarTarifario_OUT(listarTarifario_IN);
    }
}
