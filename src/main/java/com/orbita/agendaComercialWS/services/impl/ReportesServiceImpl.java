package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.Base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.ReportesDAO;
import com.orbita.agendaComercialWS.services.ReportesService;

@Service
public class ReportesServiceImpl implements ReportesService {

	@Autowired
	private ReportesDAO reportesDAO;
	
	@Override
	@Transactional
	public ReporteAsignados_OUT reporteAsignados_OUT(ReporteAsignados_IN reporteAsignados_IN) throws SQLException {
		return reportesDAO.reporteAsignados_OUT(reporteAsignados_IN);
	}

	@Override
	@Transactional
	public ReporteGestion_OUT reporteGestion_OUT(ReporteGestion_IN reporteGestion_IN) throws SQLException {
		return reportesDAO.reporteGestion_OUT(reporteGestion_IN);
	}

	
	@Override
	@Transactional
	public ListarActividad_OUT listarActividad_OUT(ListarBasesDetalle_IN listarBasesDetalle_IN) throws SQLException {
		return reportesDAO.listarActividad_OUT(listarBasesDetalle_IN);
	}

	@Override
	@Transactional
	public ListarBasesOUT listarBase_OUT(ListarBases_IN listarBases_in) throws SQLException {
		return reportesDAO.listarBase_OUT(listarBases_in);
	}

	@Override
	public ListarReportes_OUT listarReportes_OUT(General_IN general_IN) throws SQLException {
		return reportesDAO.listarReportes_OUT(general_IN);
	}

	@Override
	public ObtenerReporte_OUT obtenerReporte_OUT(ObtenerReporte_IN obtenerReporte_IN) throws SQLException {
		return reportesDAO.obtenerReporte_OUT(obtenerReporte_IN);
	}
}
