package com.orbita.agendaComercialWS.services.impl;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orbita.agendaComercialWS.dao.AgendaDAO;
import com.orbita.agendaComercialWS.services.AgendaService;

@Service
public class AgendaServiceImpl implements AgendaService {
	@Autowired
	private AgendaDAO agendaDAO;

	@Override
	@Transactional
	public ListarCantidadCitasCalendario_OUT listarCitasPorMes_OUT(
			ListarCantidadCitasCalendario_IN listarCitasPorMes_IN) throws SQLException {
		return agendaDAO.listarCitasPorMes_OUT(listarCitasPorMes_IN);
	}

	@Override
	@Transactional
	public ListarCitasPorDia_OUT listarCitasPorDia_OUT(ListarCitasPorDia_IN listarCitasPorDia_IN) throws SQLException {
		return agendaDAO.listarCitasPorDia_OUT(listarCitasPorDia_IN);
	}

	@Override
	@Transactional
	public ListarCitasPorDiaGeo_OUT listarCitasPorDiaGeo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException {
		return agendaDAO.listarCitasPorDiaGeo_OUT(listarMiRadar_IN);
	}

	@Override
	@Transactional
	public ObtenerDetalleCita_OUT obtenerDetalleCita_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException {
		return agendaDAO.obtenerDetalleCita_OUT(obtenerDetalleCita_IN);
	}

	@Override
	@Transactional
	public ListarMotivosNoContactado_OUT listarMotivosNoContactados_OUT(
			ListarMotivosNoContactado_IN listarMotivosNoContactados_IN) throws SQLException {
		return agendaDAO.listarMotivosNoContactados_OUT(listarMotivosNoContactados_IN);
	}

	@Override
	@Transactional
	public ListarResultadosCita_OUT listarResultadosCita_OUT(ListarResultadosCita_IN listarResultadosCita_IN)
			throws SQLException {
		return agendaDAO.listarResultadosCita_OUT(listarResultadosCita_IN);
	}

	@Override
	@Transactional
	public ListarResultadosCitaDetalle_OUT listarResultadosCitaDetalle_OUT(
			ListarResultadosCitaDetalle_IN listarResultadosCitaDetalle_IN) throws SQLException {
		return agendaDAO.listarResultadosCitaDetalle_OUT(listarResultadosCitaDetalle_IN);
	}

	@Override
	@Transactional
	public GuardarResultadoCitaNC_OUT guardarResultadoCitaNC_OUT(GuardarResultadoCitaNC_IN guardarResultadoCitaNC_IN)
			throws SQLException {
		return agendaDAO.guardarResultadoCitaNC_OUT(guardarResultadoCitaNC_IN);
	}

	@Override
	@Transactional
	public GuardarResultadoCitaSC_OUT guardarResultadoCitaSC_OUT(GuardarResultadoCitaSC_IN guardarResultadoCitaSC_IN)
			throws SQLException {
		return agendaDAO.guardarResultadoCitaSC_OUT(guardarResultadoCitaSC_IN);
	}

	@Override
	@Transactional
	public GuardarFotoCita_OUT guardarFotoCita_OUT(GuardarFotoCita_IN guardarFotoCita_IN) throws SQLException {
		return agendaDAO.guardarFotoCita_OUT(guardarFotoCita_IN);
	}

	@Override
	@Transactional
	public ListarCitasPorMes_OUT listarCitasPorMes_OUT(ListarCitasPorMes_IN listarCitasPorMes_IN) throws SQLException {
		return agendaDAO.listarCitasPorMes_OUT(listarCitasPorMes_IN);
	}

	@Override
	@Transactional
	public ListarCarteraMapa_OUT listarCarteraMapa_OUT(ListarMiRadar_IN listarMiRadar_IN) throws SQLException {
		return agendaDAO.listarCarteraMapa_OUT(listarMiRadar_IN);
	}

	@Override
	@Transactional
	public ObtenerInfoCliente_OUT obtenerInfoCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {
		return agendaDAO.obtenerInfoCliente_OUT(obtenerInfoCliente_IN);
	}

	@Override
	@Transactional
	public GuardarInfoCliente_OUT guardarInfoCliente_OUT(GuardarInfoCliente_IN guardarInfoCliente_IN)
			throws SQLException {
		return agendaDAO.guardarInfoCliente_OUT(guardarInfoCliente_IN);
	}

	@Override
	@Transactional
	public GuardarInfoClienteDireccion_OUT guardarInfoClienteDireccion_OUT(
			GuardarInfoClienteDireccion_IN guardarInfoClienteDireccion_IN) throws SQLException {
		return agendaDAO.guardarInfoClienteDireccion_OUT(guardarInfoClienteDireccion_IN);
	}

	@Override
	@Transactional
	public ListarDireccionesCliente_OUT listarDireccionesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		return agendaDAO.listarDireccionesCliente_OUT(obtenerInfoCliente_IN);
	}
	
	@Override
	@Transactional
	public ListarBasesCliente_OUT listarBasesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		return agendaDAO.listarBasesCliente_OUT(obtenerInfoCliente_IN);
	}

	@Override
	@Transactional
	public GuardarCliente_OUT guardarCliente_OUT(GuardarCliente_IN guardarCliente_IN) throws SQLException {
		return agendaDAO.guardarCliente_OUT(guardarCliente_IN);
	}

	@Override
	@Transactional
	public ListarCitaResultado_OUT listarCitaResultado_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException {
		return agendaDAO.listarCitaResultado_OUT(obtenerDetalleCita_IN);
	}

	@Override
	@Transactional
	public ResponseAgenda_OUT bajaInfoCliente_OUT(BajaInfoCliente_IN bajaInfoCliente_IN) throws SQLException {
		return agendaDAO.bajaInfoCliente_OUT(bajaInfoCliente_IN);
	}

	@Override
	@Transactional
	public ListarClienteInfo_OUT listarClienteInfo_OUT(int tipo, ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {
		return agendaDAO.listarClienteInfo_OUT(tipo, obtenerInfoCliente_IN);
	}

	@Override
	@Transactional
	public ListarClienteTelef_OUT listarClienteTelef_OUT(ListarTelefCliente_IN listarTelefCliente_IN)
			throws SQLException {
		return agendaDAO.listarClienteTelef_OUT(listarTelefCliente_IN);
	}

	@Override
	@Transactional
	public ListarClienteDireccion_OUT listarClienteDireccion_OUT(ListarDireccCliente_IN listarDireccCliente_IN)
			throws SQLException {
		return agendaDAO.listarClienteDireccion_OUT(listarDireccCliente_IN);
	}

	@Override
	@Transactional
	public ListarCitaFotos_OUT listarCitaFotos_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN) throws SQLException {
		return agendaDAO.listarCitaFotos_OUT(obtenerDetalleCita_IN);
	}

	@Override
	public ListarClienteResultado_OUT listarClienteResultado_OUT(ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException {
		return agendaDAO.listarClienteResultado_OUT(listarInfoCliente_IN);
	}

}