package com.orbita.agendaComercialWS.util;

public class WebServicesConstants {
	
	public static final String PARAM_RUTA_PROPERTIES = "/WEB-INF/config.properties";
	
	// RestURIs
	
	/**
	 * WS General
	 */
	public static final String WS_LISTAR_BASES = "/WS_ListarBases";
	public static final String WS_LISTAR_ACTIVIDAD_BASE = "/WS_ListarActividadBase";
	public static final String WS_LISTAR_MONITOREO_CLIENTE = "/WS_ListarMonitoreoCliente";
	public static final String WS_GUARDAR_ERRORES = "/WS_GuardarErrores";
	public static final String WS_VERSION = "WS_Version";
	
	/**
	 * WS M�dulo Agenda
	 */
	public static final String WS_VALIDAR_USUARIO_AC = "/WS_ValidarUsuarioAC";
	public static final String WS_OBTENER_CONTADOR_CITAS = "/WS_ObtenerContadorCitas";
	public static final String WS_LISTAR_CANTIDAD_CITAS_CALENDARIO = "/WS_ListarCantidadCitasCalendario";
	public static final String WS_LISTAR_CITAS_POR_DIA = "/WS_ListarCitasPorDia";
	public static final String WS_LISTAR_CITAS_POR_DIA_GEO = "/WS_ListarCitasPorDiaGeo";
	public static final String WS_LISTAR_CITA_RESULTADOS = "/WS_ListarCitaResultados";
	public static final String WS_LISTAR_CITA_FOTOS = "WS_ListarCitaFotos";
	public static final String WS_OBTENER_DETALLE_CITA = "/WS_ObtenerDetalleCita";
	public static final String WS_LISTAR_MOTIVOS_NO_CONTACTADOS = "/WS_ListarMotivosNoContactados";
	public static final String WS_LISTAR_REACCIONES = "/WS_ListarReacciones";
	public static final String WS_LISTAR_REACCIONES_DETALLE = "/WS_ListarReaccionesDetalle";
	public static final String WS_GUARDAR_RESULTADO_CITA_NC = "/WS_GuardarResultadoCitaNC";
	public static final String WS_GUARDAR_RESULTADO_CITA_SC = "/WS_GuardarResultadoCitaSC";
	public static final String WS_GUARDAR_FOTO_CITA = "/WS_GuardarFotoCita";
	public static final String WS_LISTAR_CITAS_POR_MES = "/WS_ListarCitasPorMes";
	public static final String WS_LISTAR_CARTERA_MAPA = "/WS_ListarCarteraMapa";
	public static final String WS_OBTENER_INFO_CLIENTE = "/WS_ObtenerInfoCliente";
	public static final String WS_GUARDAR_INFO_CLIENTE = "/WS_GuardarInfoCliente";
	public static final String WS_LISTAR_DIRECCIONES_CLIENTE = "/WS_ListarDireccionesCLiente";
	public static final String WS_GUARDAR_CLIENTE_DIRECCION = "/WS_GuardarClienteDireccion";
	public static final String WS_BAJA_INFO_CLIENTE = "/WS_BajaInfoCliente";
	public static final String WS_LISTAR_TELEF_CLIENTE = "/WS_ListarTelefCliente";
	public static final String WS_LISTAR_DIRECC_CLIENTE = "/WS_ListarDireccionCliente";
	public static final String WS_LISTAR_CLIENTE_RESULTADOS = "/WS_ListarClienteResultado";
	
	/**
	 * WS M�dulo Bases
	 */
	public static final String WS_LISTAR_BASES_CLIENTE = "/WS_ListarBasesCliente";
	public static final String WS_LISTAR_BASES_OPORTUNIDAD = "/WS_ListarBasesOportunidad";
	public static final String WS_LISTAR_CANTIDAD_BASES = "/WS_ListarCantidadBases";
	public static final String WS_LISTAR_BASES_DETALLE = "/WS_ListarBasesDetalle";
	public static final String WS_LISTAR_OPORTUNIDAD_XFILTRO = "/WS_ListarOportunidadXFiltro";
	public static final String WS_LISTAR_DEPARTAMENTOS = "/WS_ListarDepartamentos";
	public static final String WS_LISTAR_PROVINCIAS = "/WS_ListarProvincias";
	public static final String WS_LISTAR_DISTRITOS = "/WS_ListarDistritos";
	public static final String WS_LISTAR_TIPOS_DIRECCION = "/WS_ListarTiposDireccion";
	public static final String WS_GUARDAR_CLIENTE_GEO = "/WS_GuardarClienteGeo";
	public static final String WS_GUARDAR_CLIENTE_GEO_C = "/WS_GuardarClienteGeoC";
	public static final String WS_LISTAR_INFO_CLIENTE = "/WS_ListarInfoCliente";
	public static final String WS_LISTAR_ULT_PAGO = "/WS_ListarUltimoPago";
	
	/**
	 * WS M�dulo Consulta Crediticia
	 */
	public static final String WS_VALIDAR_AGENDAR_CITA = "/WS_ValidarAgendarCita";
	public static final String WS_AGENDAR_CITA = "/WS_AgendarCita";
	public static final String WS_AGENDAR_CITA_MASIVO = "/WS_AgendarCitaMasivo";
	public static final String WS_AGENDAR_CITA_NUEVA = "/WS_AgendarCitaNueva";
	public static final String WS_VALIDAR_CLIENTE = "/WS_ValidarCliente";
	public static final String WS_GUARDAR_CLIENTE = "/WS_GuardarCliente";
	public static final String WS_VALIDAR_EJECUTIVO_CLIENTE = "/WS_ValidarEjecutivoCliente";
	
	/**
	 * WS M�dulo Oportunidades
	 */
	public static final String WS_LISTAR_CARTERA_INACTIVA = "/WS_ListarCarteraInactivo";
	public static final String WS_OBTENER_CLIENTE_INACTIVO = "/WS_ObtenerInfoClienteInactivo";
	public static final String WS_ACTIVAR_CLIENTE_INACTIVO = "/WS_ActivarClienteInactivo";
	public static final String WS_ACTIVIDAD_OPORTUNIDAD = "/WS_ListarActividadesOportunidad";

	/**
	 * WS Módulo Simulador
	 */
	public static final String WS_LISTAR_PRODUCTO_SEGMENTO = "/WS_ListarProductoSegmento";
	public static final String WS_LISTAR_TARIFARIO = "/WS_ListarTarifario";

	/**
	 * WS M�dulo Reportes
	 */
	public static final String WS_REPORTE_ASIGNADOS = "/WS_ReporteAsignados";
	public static final String WS_REPORTE_GESTION = "/WS_ReporteGestion";
	public static final String WS_LISTAR_REPORTES = "/WS_ListarReportes";
	public static final String WS_OBTENER_REPORTE = "/WS_ObtenerReporte";
	
	// StoreProcedures Names
	public static final String SP_VALIDAR_USUARIO_AC = "call PK_MOVIL.SP_VALIDAR_USUARIO_AC(?,?,?,?,?,?,?,?,?)";
	public static final String SP_OBTENER_CONTADOR_CITAS = "call PK_MOVIL.SP_OBTENER_CONTADOR_CITAS(?,?,?,?,?)";
	public static final String SP_LISTAR_CANTIDAD_CITAS_CALENDARIO = "call PK_MOVIL.SP_LISTAR_CANT_CITAS_CALEND(?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_INDICADOR_ACTIVIDAD = "call PK_MOVIL.SP_LISTAR_INDICADOR_ACTIVIDAD(?,?,?,?,?)";
	public static final String SP_LISTAR_CITAS_POR_DIA = "call PK_MOVIL.SP_LISTAR_CITAS_POR_DIA(?,?,?,?,?,?)";
	public static final String SP_LISTAR_CITAS_POR_DIA_GEO = "call PK_MOVIL.SP_LISTAR_CITAS_POR_DIA_GEO(?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_CITA_RESULTADOS = "call PK_MOVIL.SP_LISTAR_CITA_RESULTADOS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_CITA_FOTOS = "call PK_MOVIL.SP_LISTAR_CITA_FOTOS(?,?,?,?)";
	public static final String SP_OBTENER_DETALLE_CITA = "call PK_MOVIL.SP_OBTENER_DETALLE_CITA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_MOTIVOS_NO_CONTACTADOS = "call PK_MOVIL.SP_LISTAR_MOTIVOS_NO_CONTAC(?,?,?,?,?)";
	public static final String SP_LISTAR_REACCIONES = "call PK_MOVIL.SP_LISTAR_REACCIONES(?,?,?,?)";
	public static final String SP_LISTAR_REACCIONES_DETALLE = "call PK_MOVIL.SP_LISTAR_REACCIONES_DETA(?,?,?,?,?,?)";
	public static final String SP_GUARDAR_RESULT_CITA_NC = "call PK_MOVIL.SP_GUARDAR_RESULT_CITA_NC(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_GUARDAR_RESULT_CITA_SC = "call PK_MOVIL.SP_GUARDAR_RESULT_CITA_SC(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_GUARDAR_FOTO_CITA = "call PK_MOVIL.SP_GUARDAR_FOTO_CITA(?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_CITAS_POR_MES = "call PK_MOVIL.SP_LISTAR_CITAS_POR_MES(?,?,?,?,?,?)";
	public static final String SP_LISTAR_CARTERA_MAPA = "call PK_MOVIL.SP_LISTAR_CARTERA_MAPA(?,?,?,?,?)";
	public static final String SP_OBTENER_INFO_CLIENTE = "call PK_MOVIL.SP_OBTENER_INFO_CLIENTE(?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_GUARDAR_INFO_CLIENTE = "call PK_MOVIL.SP_GUARDAR_INFO_CLIENT(?,?,?,?,?,?)";
	public static final String SP_GUARDAR_CLIENTE = "call PK_MOVIL.SP_GUARDAR_CLIENTE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_GUARDAR_CLIENTE_DIRECCION = "call PK_MOVIL.SP_GUARDAR_CLIENTE_DIRECCION(?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_BAJA_INFO_CLIENTE = "call PK_MOVIL.SP_BAJA_INFO_CLIENTE(?,?,?,?,?)";
	public static final String SP_LISTAR_TELEF_CLIENTE = "call PK_MOVIL.SP_LISTAR_TELEF_CLIENTE(?,?,?,?,?)";
	public static final String SP_LISTAR_DIRECC_CLIENTE = "call PK_MOVIL.SP_LISTAR_DIRECC_CLIENTE(?,?,?,?,?)";
	public static final String SP_LISTAR_MI_RADAR = "call PK_MOVIL.SP_LISTAR_MI_RADAR(?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_CLIENTE_RESULTADO = "call PK_MOVIL.SP_LISTAR_CLIENTE_RESULTADO(?,?,?,?,?)";
	public static final String SP_LISTAR_ACT_PAGOS = "call PK_MOVIL.SP_LISTAR_ACT_PAGOS(?,?,?,?,?)";

	
	public static final String SP_LISTAR_BASES_CLIENTE = "call PK_MOVIL.SP_LISTAR_BASES_CLIENTE(?,?,?,?,?)";
	public static final String SP_LISTAR_BASES_OPORTUNIDAD = "call PK_MOVIL.SP_LISTAR_BASES_OPORTUNIDAD(?,?,?,?,?)";
	public static final String SP_LISTAR_CANTIDAD_BASES = "call PK_MOVIL.SP_LISTAR_CANTIDAD_BASES(?,?,?,?)";
	public static final String SP_LISTAR_BASES_DETALLE = "call PK_MOVIL.SP_LISTAR_BASES_DETALLE(?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_OPORTUNIDAD_XFILTRO = "call PK_MOVIL.SP_LISTAR_OPORTUNIDAD_XFILTRO(?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_DEPARTAMENTOS = "call PK_MOVIL.SP_LISTAR_DEPARTAMENTOS(?,?,?)";
	public static final String SP_LISTAR_PROVINCIAS = "call PK_MOVIL.SP_LISTAR_PROVINCIAS(?,?,?,?,?,?)";
	public static final String SP_LISTAR_DISTRITOS = "call PK_MOVIL.SP_LISTAR_DISTRITOS(?,?,?,?,?,?)";
	public static final String SP_LISTAR_TIPOS_DIRECCION = "call PK_MOVIL.SP_LISTAR_TIPOS_DIRECCION(?,?,?,?,?)";
	public static final String SP_GUARDAR_CLIENTE_GEO = "call PK_MOVIL.SP_GUARDAR_CLIENTE_GEO(?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SP_GUARDAR_CLIENTE_GEO_C = "call PK_MOVIL.SP_GUARDAR_CLIENTE_GEO_C(?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_DIRECCION_CLIENTE = "call PK_MOVIL.SP_LISTAR_DIRECCION_CLIENTE(?,?,?,?)";
	public static final String SP_LISTAR_INFO_CLIENTE = "call PK_MOVIL.SP_LISTAR_INFO_CLIENTE(?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_INFO_CLIENTE_DETALLE = "call PK_MOVIL.SP_LISTAR_INFO_CLIENTE_DETALLE(?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_ACTIVIDAD_FILTRO = "call PK_MOVIL.SP_LISTAR_ACTIVIDAD_FILTRO(?,?,?,?,?)";
	
	public static final String SP_VALIDAR_AGENDAR_CITA = "call PK_MOVIL.SP_VALIDAR_AGENDAR_CITA(?,?,?,?,?,?)";
	public static final String SP_AGENDAR_CITA = "call PK_MOVIL.SP_AGENDAR_CITA(?,?,?,?,?,?,?,?,?)";
	public static final String SP_AGENDAR_CITA_MASIVO = "call PK_MOVIL.SP_AGENDAR_CITA_MASIVO(?,?,?,?)";
	public static final String SP_AGENDAR_CITA_NUEVA = "call PK_MOVIL.SP_AGENDAR_CITA_NUEVA(?,?,?,?,?,?,?)";
	public static final String SP_VALIDAR_CLIENTE = "call PK_MOVIL.SP_VALIDAR_CLIENTE(?,?,?,?,?,?)";
	public static final String SP_VALIDAR_EJECUTIVO_CLIENTE = "call PK_MOVIL.SP_VALIDAR_EJECUTIVO_CLIENTE(?,?,?,?,?,?,?)";
	
	public static final String SP_LISTAR_CARTERA_INACTIVA = "call PK_MOVIL.SP_LISTAR_CARTERA_INACTIVO(?,?,?,?,?,?,?)";
	public static final String SP_OBTENER_CLIENTE_INACTIVO = "call PK_MOVIL.SP_OBTENER_INFO_CLIENTE_INACT(?,?,?,?,?,?,?,?,?)";
	public static final String SP_ACTIVAR_CLIENTE_INACTIVO = "call PK_MOVIL.SP_ACTIVAR_CLIENT_INACT(?,?,?,?,?)";
	
	public static final String SP_LISTAR_ACTIVIDAD_CLIENTE = "call PK_MOVIL.SP_LISTAR_ACTIVIDAD_CLIENTE(?,?,?,?,?)";
	public static final String SP_LISTAR_MONITOREO_CLIENTE = "call PK_MOVIL.SP_LISTAR_MONITOREO_CLIENTE(?,?,?,?,?,?)";

	//Simulador
	public static final String SP_LISTAR_PRODUCTO_SEGMENTO = "call PK_MOVIL.SP_LISTAR_PRODUCTO_SEGMENTO(?,?,?,?)";
	public static final String SP_LISTAR_TARIFARIO = "call PK_MOVIL.SP_LISTAR_TARIFARIO(?,?,?,?,?,?,?,?,?)";

	//Reportes
	public static final String SP_REPORTE_ASIGNADOS = "call PK_MOVIL.SP_REPORTE_ASIGNADOS(?,?,?,?,?,?,?,?,?)";
	public static final String SP_REPORTE_GESTION = "call PK_MOVIL.SP_REPORTE_GESTION(?,?,?,?,?,?,?,?,?)";
	public static final String SP_LISTAR_ACTIVIDAD = "call PK_MOVIL.SP_LISTAR_ACTIVIDAD(?,?,?,?)";
	public static final String SP_LISTAR_BASE = "call PK_MOVIL.SP_LISTAR_BASE(?,?,?,?)";
	public static final String SP_LISTAR_REPORTES = "call PK_MOVIL.SP_LISTAR_REPORTE(?,?,?,?,?)";
	public static final String SP_OBTENER_REPORTE_CAB = "call PK_MOVIL.SP_LISTAR_REPORTE(?,?,?,?,?,?)";
	public static final String SP_OBTENER_REPORTE_DETA = "call PK_MOVIL.SP_LISTAR_REPORTE(?,?,?,?,?,?,?)";
	
	//Auditoria
	public static final String SP_G_AUDT_ERRORES = "call PK_MOVIL.SP_G_AUDT_ERRORES(?)";
	
	//Seguridad
	public static final String BEAN_SESSION = "beanSession";
	public static final String SEGURIDAD_NO_INGRESO = "/NOSESSION";
	public static final String SEGURIDAD_VULNERABILIDAD = "/VULNERABILIDAD";
	

}
