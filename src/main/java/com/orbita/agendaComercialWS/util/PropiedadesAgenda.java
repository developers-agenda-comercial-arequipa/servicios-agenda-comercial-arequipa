package com.orbita.agendaComercialWS.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.ServletContext;

public class PropiedadesAgenda {
	private static java.util.Properties prop ;
	public static java.util.Properties getProp(ServletContext context){
		if (prop == null) {
			 prop = new java.util.Properties();
			try {
				String path = context.getRealPath("/") + "WEB-INF/classes/config.properties";
				File file = new File(path);
			    InputStream inputStream = new FileInputStream(file);
				prop.load(inputStream);
			} catch (Exception e) {
				//e.printStackTrace();
				// TODO: handle exception
			}
			
		}
		return prop;
	}
	public static String getPropiedad (String key) {
		return prop.getProperty(key);
	}
	
}
