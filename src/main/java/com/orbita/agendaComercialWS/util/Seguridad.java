package com.orbita.agendaComercialWS.util;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orbita.agendaComercialProperties.Properties;
import com.orbita.agendaComercialWS.model.ErrorSeguridad;


public class Seguridad {

	private static final Logger logger = LoggerFactory.getLogger(Seguridad.class);

	String url = Properties.LDAP_URL;

	public ErrorSeguridad validarUsuarioLDAP(String usuario, String contrasena) {

		ErrorSeguridad errorSeguridad = new ErrorSeguridad();
		
		// Desencriptación Base 64
		contrasena.replace("\n", "");
		
        byte[] decoded = Base64.decodeBase64(contrasena);
        String contrasena_decoded = new String(decoded);
		
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, Properties.LDAP_URL);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, usuario);
		env.put(Context.SECURITY_CREDENTIALS, contrasena_decoded);

		
		try {
			DirContext ctx = new InitialDirContext(env);
			
			logger.info("Conectado");
		    logger.info(ctx.getEnvironment().toString());

			if (ctx != null) ctx.close();
			
			errorSeguridad.setCodigo("1");
			errorSeguridad.setMensaje("Conectado");
			
			return errorSeguridad;
			
		} catch (AuthenticationNotSupportedException ex) {
			logger.info("La autentificación no es soportada por el servidor.");
			logger.error(ex.getMessage(), ex);
			
			errorSeguridad.setCodigo("0");
			errorSeguridad.setMensaje("La autentificación no es soportada por el servidor.");
			
			return errorSeguridad;
		} catch (AuthenticationException ex) {
			logger.info("Usuario o contraseña incorrecta");
			logger.error(ex.getMessage(), ex);
			
			errorSeguridad.setCodigo("0");
			errorSeguridad.setMensaje("Usuario o contraseña incorrecta");
			
			return errorSeguridad;
		} catch (NamingException ex) {
			logger.info("Error cuando se trata de crear el contexto");
			logger.error(ex.getMessage(), ex);
			
			errorSeguridad.setCodigo("0");
			errorSeguridad.setMensaje("Error cuando se trata de crear el contexto");
			
			return errorSeguridad;
		}
	}
}
