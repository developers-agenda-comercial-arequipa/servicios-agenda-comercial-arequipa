package com.orbita.agendaComercialWS.model;

public class ClienteTelef {

	private int idInfo;
	private int idTipoInfo;
	private String descInfo;
	private int estado;
	
	public int getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(int idInfo) {
		this.idInfo = idInfo;
	}
	public int getIdTipoInfo() {
		return idTipoInfo;
	}
	public void setIdTipoInfo(int idTipoInfo) {
		this.idTipoInfo = idTipoInfo;
	}
	public String getDescInfo() {
		return descInfo;
	}
	public void setDescInfo(String descInfo) {
		this.descInfo = descInfo;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
}
