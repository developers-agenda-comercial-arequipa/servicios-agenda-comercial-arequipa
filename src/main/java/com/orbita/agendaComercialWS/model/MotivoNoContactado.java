package com.orbita.agendaComercialWS.model;

public class MotivoNoContactado {
	int id_motivo;
	String descripcion_motivo;

	public int getId_motivo() {
		return id_motivo;
	}

	public void setId_motivo(int id_motivo) {
		this.id_motivo = id_motivo;
	}

	public String getDescripcion_motivo() {
		return descripcion_motivo;
	}

	public void setDescripcion_motivo(String descripcion_motivo) {
		this.descripcion_motivo = descripcion_motivo;
	}

}
