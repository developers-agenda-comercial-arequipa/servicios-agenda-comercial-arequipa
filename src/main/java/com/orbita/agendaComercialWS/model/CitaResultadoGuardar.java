package com.orbita.agendaComercialWS.model;

public class CitaResultadoGuardar {

	private int idGestion;
	private int idReaccion;
	private int idReaccionDetalle;
	private String comentario;
	private int reprogramar;
	private String fecNuevaCita;
	
	public int getIdGestion() {
		return idGestion;
	}
	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}
	public int getIdReaccion() {
		return idReaccion;
	}
	public void setIdReaccion(int idReaccion) {
		this.idReaccion = idReaccion;
	}
	public int getIdReaccionDetalle() {
		return idReaccionDetalle;
	}
	public void setIdReaccionDetalle(int idReaccionDetalle) {
		this.idReaccionDetalle = idReaccionDetalle;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public int getReprogramar() {
		return reprogramar;
	}
	public void setReprogramar(int reprogramar) {
		this.reprogramar = reprogramar;
	}
	public String getFecNuevaCita() {
		return fecNuevaCita;
	}
	public void setFecNuevaCita(String fecNuevaCita) {
		this.fecNuevaCita = fecNuevaCita;
	}
	
}
