package com.orbita.agendaComercialWS.model;

public class CarteraDetalle {
	int id_hoja_ruta_detalle;
	int id_cliente;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	int id_tipo_cliente;
	String tipo_cliente;
	int id_color_tipo_cliente;
	int id_direccion;
	String direccion;
	String referencia_direccion;
	String distrito;
	String latitud;
	String longitud;
	int id_tipo_documento;
	String tipo_documento;
	String numero_documento;
	String email;
	String telefono_1;
	String estado_hoja_ruta;
	String resultado_cita;
	String resultado_cita_detalle;
	String motivo_no_contactado;
	String comentario;

	public int getId_hoja_ruta_detalle() {
		return id_hoja_ruta_detalle;
	}

	public void setId_hoja_ruta_detalle(int id_hoja_ruta_detalle) {
		this.id_hoja_ruta_detalle = id_hoja_ruta_detalle;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public int getId_tipo_cliente() {
		return id_tipo_cliente;
	}

	public void setId_tipo_cliente(int id_tipo_cliente) {
		this.id_tipo_cliente = id_tipo_cliente;
	}

	public String getTipo_cliente() {
		return tipo_cliente;
	}

	public void setTipo_cliente(String tipo_cliente) {
		this.tipo_cliente = tipo_cliente;
	}

	public int getId_color_tipo_cliente() {
		return id_color_tipo_cliente;
	}

	public void setId_color_tipo_cliente(int id_color_tipo_cliente) {
		this.id_color_tipo_cliente = id_color_tipo_cliente;
	}

	public int getId_direccion() {
		return id_direccion;
	}

	public void setId_direccion(int id_direccion) {
		this.id_direccion = id_direccion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getReferencia_direccion() {
		return referencia_direccion;
	}

	public void setReferencia_direccion(String referencia_direccion) {
		this.referencia_direccion = referencia_direccion;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public int getId_tipo_documento() {
		return id_tipo_documento;
	}

	public void setId_tipo_documento(int id_tipo_documento) {
		this.id_tipo_documento = id_tipo_documento;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono_1() {
		return telefono_1;
	}

	public void setTelefono_1(String telefono_1) {
		this.telefono_1 = telefono_1;
	}

	public String getEstado_hoja_ruta() {
		return estado_hoja_ruta;
	}

	public void setEstado_hoja_ruta(String estado_hoja_ruta) {
		this.estado_hoja_ruta = estado_hoja_ruta;
	}

	public String getResultado_cita() {
		return resultado_cita;
	}

	public void setResultado_cita(String resultado_cita) {
		this.resultado_cita = resultado_cita;
	}

	public String getResultado_cita_detalle() {
		return resultado_cita_detalle;
	}

	public void setResultado_cita_detalle(String resultado_cita_detalle) {
		this.resultado_cita_detalle = resultado_cita_detalle;
	}

	public String getMotivo_no_contactado() {
		return motivo_no_contactado;
	}

	public void setMotivo_no_contactado(String motivo_no_contactado) {
		this.motivo_no_contactado = motivo_no_contactado;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
