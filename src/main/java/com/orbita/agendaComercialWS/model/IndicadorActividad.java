package com.orbita.agendaComercialWS.model;

public class IndicadorActividad {

	private int idColor;
	private int gestionado;
	
	public int getIdColor() {
		return idColor;
	}
	public void setIdColor(int idColor) {
		this.idColor = idColor;
	}
	public int getGestionado() {
		return gestionado;
	}
	public void setGestionado(int gestionado) {
		this.gestionado = gestionado;
	}
	
	
}
