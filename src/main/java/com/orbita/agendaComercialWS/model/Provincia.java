package com.orbita.agendaComercialWS.model;

public class Provincia {
	int id_provincia;
	String descripcion_provincia;

	public int getId_provincia() {
		return id_provincia;
	}

	public void setId_provincia(int id_provincia) {
		this.id_provincia = id_provincia;
	}

	public String getDescripcion_provincia() {
		return descripcion_provincia;
	}

	public void setDescripcion_provincia(String descripcion_provincia) {
		this.descripcion_provincia = descripcion_provincia;
	}

}
