package com.orbita.agendaComercialWS.model;

/**
 * @author Jorge
 *
 */
public class ActividadInfo {

	private int idInfo;
	private String descInfo;
	
	public int getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(int idInfo) {
		this.idInfo = idInfo;
	}
	public String getDescInfo() {
		return descInfo;
	}
	public void setDescInfo(String descInfo) {
		this.descInfo = descInfo;
	}
	
}
