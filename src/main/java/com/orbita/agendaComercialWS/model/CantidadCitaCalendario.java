package com.orbita.agendaComercialWS.model;

import java.util.ArrayList;

public class CantidadCitaCalendario {
	String fecha;
	int cantidad;
	ArrayList<IndicadorActividad> gestionActividad;
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public ArrayList<IndicadorActividad> getGestionActividad() {
		return gestionActividad;
	}
	public void setGestionActividad(ArrayList<IndicadorActividad> gestionActividad) {
		this.gestionActividad = gestionActividad;
	}

	
}
