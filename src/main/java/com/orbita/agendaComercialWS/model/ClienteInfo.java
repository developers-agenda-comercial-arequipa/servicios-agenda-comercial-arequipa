package com.orbita.agendaComercialWS.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

public class ClienteInfo {

	private int idInfo;
	private int idTipoInfo;
	private String tipoInfo;
	private String descInfo;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String idGroupInfo;
	private int idActividad;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private ArrayList<ClienteInfo> infoDetalle;
	
	public int getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(int idInfo) {
		this.idInfo = idInfo;
	}
	public int getIdTipoInfo() {
		return idTipoInfo;
	}
	public void setIdTipoInfo(int idTipoInfo) {
		this.idTipoInfo = idTipoInfo;
	}
	public String getTipoInfo() {
		return tipoInfo;
	}
	public void setTipoInfo(String tipoInfo) {
		this.tipoInfo = tipoInfo;
	}
	public String getDescInfo() {
		return descInfo;
	}
	public void setDescInfo(String descInfo) {
		this.descInfo = descInfo;
	}
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

	public String getIdGroupInfo() {
		return idGroupInfo;
	}

	public void setIdGroupInfo(String idGroupInfo) {
		this.idGroupInfo = idGroupInfo;
	}

	public ArrayList<ClienteInfo> getInfoDetalle() {
		return infoDetalle;
	}

	public void setInfoDetalle(ArrayList<ClienteInfo> infoDetalle) {
		this.infoDetalle = infoDetalle;
	}
}
