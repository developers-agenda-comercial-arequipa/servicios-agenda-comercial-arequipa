package com.orbita.agendaComercialWS.model;

public class Distrito {
	int id_distrito;
	String descripcion_distrito;

	public int getId_distrito() {
		return id_distrito;
	}

	public void setId_distrito(int id_distrito) {
		this.id_distrito = id_distrito;
	}

	public String getDescripcion_distrito() {
		return descripcion_distrito;
	}

	public void setDescripcion_distrito(String descripcion_distrito) {
		this.descripcion_distrito = descripcion_distrito;
	}

}
