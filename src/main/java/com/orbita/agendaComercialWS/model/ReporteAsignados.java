package com.orbita.agendaComercialWS.model;

public class ReporteAsignados {

	private int idGestionEstado;
	private int cantXEstado;
	private int pctXEstado;
	
	public int getIdGestionEstado() {
		return idGestionEstado;
	}
	public void setIdGestionEstado(int idGestionEstado) {
		this.idGestionEstado = idGestionEstado;
	}
	public int getCantXEstado() {
		return cantXEstado;
	}
	public void setCantXEstado(int cantXEstado) {
		this.cantXEstado = cantXEstado;
	}
	public int getPctXEstado() {
		return pctXEstado;
	}
	public void setPctXEstado(int pctXEstado) {
		this.pctXEstado = pctXEstado;
	}
	
	
}
