package com.orbita.agendaComercialWS.model;

public class DistritoCartera {
	String idUbigeo;
	String distrito;
	int cantidad;
	
	public String getIdUbigeo() {
		return idUbigeo;
	}
	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
