package com.orbita.agendaComercialWS.model;

public class Cita {

    int id_cliente;
    int idGestion;
    String fecha;
    int origen;
    int nuevaCita;

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getIdGestion() {
        return idGestion;
    }

    public void setIdGestion(int idGestion) {
        this.idGestion = idGestion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getOrigen() {
        return origen;
    }

    public void setOrigen(int origen) {
        this.origen = origen;
    }

    public int getNuevaCita() {
        return nuevaCita;
    }

    public void setNuevaCita(int nuevaCita) {
        this.nuevaCita = nuevaCita;
    }
}
