package com.orbita.agendaComercialWS.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class BasesDetalle {
	int id_cliente;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	int idGestion;
	int idBase;
	int idActividad;
	String actividad;
	int idColorActividad;
	int id_direccion;
	String direccion;
	String referencia_direccion;
	String distrito;
	String latitud;
	String longitud;
	int id_tipo_documento;
	String tipo_documento;
	String numero_documento;
	int pago;
	//ArrayList<ClienteInfo> informacion;
	
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public int getIdGestion() {
		return idGestion;
	}
	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}
	public int getIdBase() {
		return idBase;
	}
	public void setIdBase(int idBase) {
		this.idBase = idBase;
	}
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getIdColorActividad() {
		return idColorActividad;
	}
	public void setIdColorActividad(int idColorActividad) {
		this.idColorActividad = idColorActividad;
	}
	public int getId_direccion() {
		return id_direccion;
	}
	public void setId_direccion(int id_direccion) {
		this.id_direccion = id_direccion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia_direccion() {
		return referencia_direccion;
	}
	public void setReferencia_direccion(String referencia_direccion) {
		this.referencia_direccion = referencia_direccion;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public int getId_tipo_documento() {
		return id_tipo_documento;
	}
	public void setId_tipo_documento(int id_tipo_documento) {
		this.id_tipo_documento = id_tipo_documento;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getNumero_documento() {
		return numero_documento;
	}
	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public int getPago() {
		return pago;
	}

	public void setPago(int pago) {
		this.pago = pago;
	}
	/*public ArrayList<ClienteInfo> getInformacion() {
		return informacion;
	}
	public void setInformacion(ArrayList<ClienteInfo> informacion) {
		this.informacion = informacion;
	}*/
	
}
