package com.orbita.agendaComercialWS.model;

public class ResultadoCitaDetalle {
	int id_reaccion_detalle;
	String descripcion_reaccion_detalle;
	int nueva_visita;
	int misti;
	
	public int getId_reaccion_detalle() {
		return id_reaccion_detalle;
	}

	public void setId_reaccion_detalle(int id_reaccion_detalle) {
		this.id_reaccion_detalle = id_reaccion_detalle;
	}

	public String getDescripcion_reaccion_detalle() {
		return descripcion_reaccion_detalle;
	}

	public void setDescripcion_reaccion_detalle(String descripcion_reaccion_detalle) {
		this.descripcion_reaccion_detalle = descripcion_reaccion_detalle;
	}

	public int getNueva_visita() {
		return nueva_visita;
	}

	public void setNueva_visita(int nueva_visita) {
		this.nueva_visita = nueva_visita;
	}

	public int getMisti() {
		return misti;
	}

	public void setMisti(int misti) {
		this.misti = misti;
	}

}