package com.orbita.agendaComercialWS.model;

public class CitaFoto {

	int idFoto;
	String archivoFoto;
	
	public int getIdFoto() {
		return idFoto;
	}
	public void setIdFoto(int idFoto) {
		this.idFoto = idFoto;
	}
	public String getArchivoFoto() {
		return archivoFoto;
	}
	public void setArchivoFoto(String archivoFotos) {
		this.archivoFoto = archivoFotos;
	}
	
}
