package com.orbita.agendaComercialWS.model;

public class AudtError {

	int estadoCnn;
	String fecHoraError;
	String idError;
	String descripcionError;
	String ultimaAccion;
	String userApp;
	
	public int getEstadoCnn() {
		return estadoCnn;
	}
	public void setEstadoCnn(int estadoCnn) {
		this.estadoCnn = estadoCnn;
	}
	public String getFecHoraError() {
		return fecHoraError;
	}
	public void setFecHoraError(String fecHoraError) {
		this.fecHoraError = fecHoraError;
	}
	public String getIdError() {
		return idError;
	}
	public void setIdError(String idError) {
		this.idError = idError;
	}
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	public String getUltimaAccion() {
		return ultimaAccion;
	}
	public void setUltimaAccion(String ultimaAccion) {
		this.ultimaAccion = ultimaAccion;
	}
	public String getUserApp() {
		return userApp;
	}
	public void setUserApp(String userApp) {
		this.userApp = userApp;
	}
	
}
