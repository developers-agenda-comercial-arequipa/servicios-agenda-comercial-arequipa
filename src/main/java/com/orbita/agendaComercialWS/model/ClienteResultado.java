package com.orbita.agendaComercialWS.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

public class ClienteResultado {

    private Date fecha;
    private String origen;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reaccion;
    private String reaccionDetalle;
    private String comentario;
    private String usuario;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public String getReaccionDetalle() {
        return reaccionDetalle;
    }

    public void setReaccionDetalle(String reaccionDetalle) {
        this.reaccionDetalle = reaccionDetalle;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
