package com.orbita.agendaComercialWS.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class UltimoPago {

    private String fecha;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Double monto;
    private String estado;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
