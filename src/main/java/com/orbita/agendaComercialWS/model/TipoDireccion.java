package com.orbita.agendaComercialWS.model;

public class TipoDireccion {
	int id_tipo_direccion;
	String descripcion_tipo_direccion;

	public int getId_tipo_direccion() {
		return id_tipo_direccion;
	}

	public void setId_tipo_direccion(int id_tipo_direccion) {
		this.id_tipo_direccion = id_tipo_direccion;
	}

	public String getDescripcion_tipo_direccion() {
		return descripcion_tipo_direccion;
	}

	public void setDescripcion_tipo_direccion(String descripcion_tipo_direccion) {
		this.descripcion_tipo_direccion = descripcion_tipo_direccion;
	}

}
