package com.orbita.agendaComercialWS.model;

public class Info {

	private int idTipoInfo;
	private String descInfo;
	
	public int getIdTipoInfo() {
		return idTipoInfo;
	}
	public void setIdTipoInfo(int idTipoInfo) {
		this.idTipoInfo = idTipoInfo;
	}
	public String getDescInfo() {
		return descInfo;
	}
	public void setDescInfo(String descInfo) {
		this.descInfo = descInfo;
	}
	
	
}
