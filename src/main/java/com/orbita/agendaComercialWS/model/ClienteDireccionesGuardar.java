package com.orbita.agendaComercialWS.model;

public class ClienteDireccionesGuardar {
	String nro_documento;
	String id_usuario_session;
	int id_cliente;
	int id_direccion;
	String direccion;
	String referencia;
	int id_tipo_direccion;
	String id_departamento;
	String id_provincia;
	String id_distrito;
	int principal;
	String latitud;
	String longitud;
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public String getId_usuario_session() {
		return id_usuario_session;
	}
	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public int getId_direccion() {
		return id_direccion;
	}
	public void setId_direccion(int id_direccion) {
		this.id_direccion = id_direccion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public int getId_tipo_direccion() {
		return id_tipo_direccion;
	}
	public void setId_tipo_direccion(int id_tipo_direccion) {
		this.id_tipo_direccion = id_tipo_direccion;
	}
	public String getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(String id_departamento) {
		this.id_departamento = id_departamento;
	}
	public String getId_provincia() {
		return id_provincia;
	}
	public void setId_provincia(String id_provincia) {
		this.id_provincia = id_provincia;
	}
	public String getId_distrito() {
		return id_distrito;
	}
	public void setId_distrito(String id_distrito) {
		this.id_distrito = id_distrito;
	}
	public int getPrincipal() {
		return principal;
	}
	public void setPrincipal(int principal) {
		this.principal = principal;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	
	
	
}
