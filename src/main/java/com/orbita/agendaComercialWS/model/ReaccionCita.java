package com.orbita.agendaComercialWS.model;

import java.util.ArrayList;

public class ReaccionCita {
	int id_actividad;
	String actividad;
	int idColorActividad;
	int id_reaccion;
	String descripcion_reaccion;
	int nueva_visita;
	ArrayList<ResultadoCitaDetalle> resultadoDetalle;
	
	public int getId_actividad() {
		return id_actividad;
	}
	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getIdColorActividad() {
		return idColorActividad;
	}
	public void setIdColorActividad(int idColorActividad) {
		this.idColorActividad = idColorActividad;
	}
	public int getId_reaccion() {
		return id_reaccion;
	}
	public void setId_reaccion(int id_reaccion) {
		this.id_reaccion = id_reaccion;
	}
	public String getDescripcion_reaccion() {
		return descripcion_reaccion;
	}
	public void setDescripcion_reaccion(String descripcion_reaccion) {
		this.descripcion_reaccion = descripcion_reaccion;
	}
	public int getNueva_visita() {
		return nueva_visita;
	}
	public void setNueva_visita(int nueva_visita) {
		this.nueva_visita = nueva_visita;
	}
	public ArrayList<ResultadoCitaDetalle> getResultadoDetalle() {
		return resultadoDetalle;
	}
	public void setResultadoDetalle(ArrayList<ResultadoCitaDetalle> resultadoDetalle) {
		this.resultadoDetalle = resultadoDetalle;
	}
	
}
