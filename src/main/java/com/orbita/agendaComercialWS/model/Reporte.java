package com.orbita.agendaComercialWS.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

public class Reporte {

    private String tipo;
    private String descripcion;
    private String grupo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ArrayList<Reporte> detalle;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public ArrayList<Reporte> getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList<Reporte> detalle) {
        this.detalle = detalle;
    }
}
