package com.orbita.agendaComercialWS.model;

public class ClienteDirecciones {
	int id_direccion;
	String direccion;
	String referencia;
	int id_tipo_direccion;
	String tipo_direccion;
	String id_departamento;
	String departamento;
	String id_provincia;
	String provincia;
	String id_distrito;
	String distrito;
	String latitud;
	String longitud;
	int principal;
	public int getId_direccion() {
		return id_direccion;
	}
	public void setId_direccion(int id_direccion) {
		this.id_direccion = id_direccion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public int getId_tipo_direccion() {
		return id_tipo_direccion;
	}
	public void setId_tipo_direccion(int id_tipo_direccion) {
		this.id_tipo_direccion = id_tipo_direccion;
	}
	public String getTipo_direccion() {
		return tipo_direccion;
	}
	public void setTipo_direccion(String tipo_direccion) {
		this.tipo_direccion = tipo_direccion;
	}
	public String getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(String id_departamento) {
		this.id_departamento = id_departamento;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getId_provincia() {
		return id_provincia;
	}
	public void setId_provincia(String id_provincia) {
		this.id_provincia = id_provincia;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getId_distrito() {
		return id_distrito;
	}
	public void setId_distrito(String id_distrito) {
		this.id_distrito = id_distrito;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public int getPrincipal() {
		return principal;
	}
	public void setPrincipal(int principal) {
		this.principal = principal;
	}

	
}
