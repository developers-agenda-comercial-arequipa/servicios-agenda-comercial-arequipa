package com.orbita.agendaComercialWS.model;

public class ReporteGestion {

	private String cliente;
	private String actividad;
	private int idColor;
	private int efectivo;
	
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getIdColor() {
		return idColor;
	}
	public void setIdColor(int idColor) {
		this.idColor = idColor;
	}
	public int getEfectivo() {
		return efectivo;
	}
	public void setEfectivo(int efectivo) {
		this.efectivo = efectivo;
	}
	
}
