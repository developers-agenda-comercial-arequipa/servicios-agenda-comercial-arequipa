package com.orbita.agendaComercialWS.model;

import java.util.ArrayList;

public class ActividadBase {

	private int idActividad;
	private String actividad;
	private ArrayList<Base> bases;
	
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public ArrayList<Base> getBases() {
		return bases;
	}
	public void setBases(ArrayList<Base> bases) {
		this.bases = bases;
	}
	
}
