package com.orbita.agendaComercialWS.model;

public class Departamento {
	int id_departamento;
	String descripcion_departamento;

	public int getId_departamento() {
		return id_departamento;
	}

	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}

	public String getDescripcion_departamento() {
		return descripcion_departamento;
	}

	public void setDescripcion_departamento(String descripcion_departamento) {
		this.descripcion_departamento = descripcion_departamento;
	}

}
