package com.orbita.agendaComercialWS.model;

import java.util.ArrayList;

public class CarteraMapa {
	int id_cliente;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	int id_tipo_documento;
	String tipo_documento;
	String nro_documento;
	int idGestion;
	int gestionEstado;
	int id_base;
	int idActividad;
	String actividad;
	int idColorActividad;
	int idClienteDireccion;
	String direccion;
	String referencia;
	int id_distrito;
	String distrito;
	String latitud;
	String longitud;
	int tipoDireccion;
	int pago;
	ArrayList<ClienteInfo> informacion;
	
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public int getId_tipo_documento() {
		return id_tipo_documento;
	}
	public void setId_tipo_documento(int id_tipo_documento) {
		this.id_tipo_documento = id_tipo_documento;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}

	public int getGestionEstado() {
		return gestionEstado;
	}

	public void setGestionEstado(int gestionEstado) {
		this.gestionEstado = gestionEstado;
	}

	public int getIdClienteDireccion() {
		return idClienteDireccion;
	}
	public int getId_base() {
		return id_base;
	}
	public void setId_base(int id_base) {
		this.id_base = id_base;
	}

	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getIdColorActividad() {
		return idColorActividad;
	}

	public void setIdColorActividad(int idColorActividad) {
		this.idColorActividad = idColorActividad;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public ArrayList<ClienteInfo> getInformacion() {
		return informacion;
	}
	public void setInformacion(ArrayList<ClienteInfo> informacion) {
		this.informacion = informacion;
	}
	public int getIdGestion() {
		return idGestion;
	}
	public void setIdClienteDireccion(int idClienteDireccion) {
		this.idClienteDireccion = idClienteDireccion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getId_distrito() {
		return id_distrito;
	}
	public void setId_distrito(int id_distrito) {
		this.id_distrito = id_distrito;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public int getTipoDireccion() {
		return tipoDireccion;
	}
	public void setTipoDireccion(int tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}

	public int getPago() {
		return pago;
	}

	public void setPago(int pago) {
		this.pago = pago;
	}
}
