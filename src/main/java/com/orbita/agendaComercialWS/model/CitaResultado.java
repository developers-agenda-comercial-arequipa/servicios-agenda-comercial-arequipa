package com.orbita.agendaComercialWS.model;


public class CitaResultado {
	
	private int idCitaResultado;
	private int idBase;
	private String base;
	private int idColor;
	private String resultCita;
	private String resultCitaDetalle;
	private String fecCita;
	private String comentario;
	
	public int getIdCitaResultado() {
		return idCitaResultado;
	}
	public void setIdCitaResultado(int idCitaResultado) {
		this.idCitaResultado = idCitaResultado;
	}
	public int getIdBase() {
		return idBase;
	}
	public void setIdBase(int idBase) {
		this.idBase = idBase;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public int getIdColor() {
		return idColor;
	}
	public void setIdColor(int idColor) {
		this.idColor = idColor;
	}
	public String getResultCita() {
		return resultCita;
	}
	public void setResultCita(String resultCita) {
		this.resultCita = resultCita;
	}
	public String getResultCitaDetalle() {
		return resultCitaDetalle;
	}
	public void setResultCitaDetalle(String resultCitaDetalle) {
		this.resultCitaDetalle = resultCitaDetalle;
	}
	public String getFecCita() {
		return fecCita;
	}
	public void setFecCita(String fecCita) {
		this.fecCita = fecCita;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	
}
