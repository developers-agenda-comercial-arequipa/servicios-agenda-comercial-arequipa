package com.orbita.agendaComercialWS.model;

import java.util.ArrayList;

public class CitaProgramadaDia {
	int id_cita;
	int id_cliente;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	int idGestion;
	String fecha;
	String estado_cita;
	String cantGeo;
	int estBloqueo;
	String msjeBloqueo;
	int cantFoto;
	ArrayList<ClienteActividad> actividades;
	
	public int getId_cita() {
		return id_cita;
	}

	public void setId_cita(int id_cita) {
		this.id_cita = id_cita;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	
	public int getIdGestion() {
		return idGestion;
	}

	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getEstado_cita() {
		return estado_cita;
	}

	public void setEstado_cita(String estado_cita) {
		this.estado_cita = estado_cita;
	}

	public String getCantGeo() {
		return cantGeo;
	}

	public void setCantGeo(String cantGeo) {
		this.cantGeo = cantGeo;
	}

	public int getEstBloqueo() {
		return estBloqueo;
	}

	public void setEstBloqueo(int estBloqueo) {
		this.estBloqueo = estBloqueo;
	}

	public String getMsjeBloqueo() {
		return msjeBloqueo;
	}

	public void setMsjeBloqueo(String msjeBloqueo) {
		this.msjeBloqueo = msjeBloqueo;
	}
	
	public int getCantFoto() {
		return cantFoto;
	}

	public void setCantFoto(int cantFoto) {
		this.cantFoto = cantFoto;
	}

	public ArrayList<ClienteActividad> getActividades() {
		return actividades;
	}

	public void setActividades(ArrayList<ClienteActividad> actividades) {
		this.actividades = actividades;
	}
	
}