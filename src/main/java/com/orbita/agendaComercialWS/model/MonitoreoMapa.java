package com.orbita.agendaComercialWS.model;

public class MonitoreoMapa {

	int idCliente;
	String cliente = " ";
	int idActividad;
	String Actividad = " ";
	int idColorActividad;
	int origen;
	
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	public String getActividad() {
		return Actividad;
	}
	public void setActividad(String actividad) {
		Actividad = actividad;
	}
	public int getIdColorActividad() {
		return idColorActividad;
	}
	public void setIdColorActividad(int idColorActividad) {
		this.idColorActividad = idColorActividad;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	
}
