package com.orbita.agendaComercialWS.model;

public class Ubigeo {

	private String idUbigeo;

	public String getIdUbigeo() {
		return idUbigeo;
	}
	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}
	
}
