package com.orbita.agendaComercialWS.seguridad;

import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;

public class SessionBean {
	private ValidarUsuarioAC_OUT user;

	public SessionBean (ValidarUsuarioAC_OUT usuario)
	{
		this.user = usuario;
	}
	
	public ValidarUsuarioAC_OUT getUser() {
		return user;
	}

	public void setUser(ValidarUsuarioAC_OUT user) {
		this.user = user;
	}
	
	
}
