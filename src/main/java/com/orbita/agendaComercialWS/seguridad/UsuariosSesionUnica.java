package com.orbita.agendaComercialWS.seguridad;

import java.util.Hashtable;
import java.util.Map;

public class UsuariosSesionUnica {
	private static Map<String, String> listaUsuariosSesion;

	public static Map<?, ?> getListaUsuariosSesion() {
		return listaUsuariosSesion;
	}

	public static void setListaUsuariosSesion(String usuario,String id) {
		if (listaUsuariosSesion == null)
			listaUsuariosSesion = new Hashtable<>();
		listaUsuariosSesion.put(usuario, id);
		
	}
	
	public static void delListaUsuariosSesion(String usuario) {
		if (listaUsuariosSesion!=null)
			listaUsuariosSesion.remove(usuario);
		
	}
	
}
