package com.orbita.agendaComercialWS.seguridad;


import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.core.env.Environment;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.orbita.agendaComercialWS.util.PropiedadesAgenda;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	private static Environment environment;

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		//logger.debug("sessionCreated: inicio");
		//int tiempoMaximoSesionEnSegundos = Integer.valueOf(10);
		//PropiedadesAgenda.getProp(event.getSession().getServletContext()).getProperty("tiempo_sesion");
		int minutos = Integer.valueOf(PropiedadesAgenda.getProp(event.getSession().getServletContext()).getProperty("tiempo_sesion")).intValue();
		//int minutos  = 5;
		int tiempoMaximoSesionEnSegundos = minutos * 60;
		event.getSession().setMaxInactiveInterval(tiempoMaximoSesionEnSegundos);
		//logger.debug("session id nueva:"+event.getSession().getId());
		//logger.debug("sessionCreated: fin");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		SessionBean sessionBean = (SessionBean) event.getSession().getAttribute(WebServicesConstants.BEAN_SESSION);
		if (sessionBean != null)
			UsuariosSesionUnica.delListaUsuariosSesion(sessionBean.getUser().getNro_documento());
		//logger.debug("sessionDestroyed: inicio");
		//logger.debug("session id caducada:"+event.getSession().getId());
	}

	/**
	 * todo que permite obtener entorno del contexto.
	 * 
	 * @param event
	 * @return
	 */
	public Environment getEnvironment(HttpSessionEvent event) {
		if (environment == null) {
			environment = WebApplicationContextUtils.getWebApplicationContext(event.getSession().getServletContext()).getEnvironment();
		}
		return environment;
	}


}
