package com.orbita.agendaComercialWS.seguridad;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.orbita.agendaComercialWS.util.WebServicesConstants;

@WebFilter(urlPatterns = {"/*" })
public class SessionFilter implements Filter {

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		httpServletResponse.setHeader("X-Powered-By", "");
		httpServletResponse.setHeader("Strict-Transport-Security", "max-age=63072000; includeSubdomains");
		httpServletResponse.setHeader("X-Frame-Options", "SAMEORIGIN");
		httpServletResponse.setHeader("X-Content-Type-Options", "nosniff");
		httpServletResponse.setHeader("X-XSS-Protection", "1; mode=block");
		
		HttpSession httpSession = httpServletRequest.getSession(false);
		
		String contextPath = httpServletRequest.getContextPath();
		// obtener uri
		String requestUri = httpServletRequest.getRequestURI();
		if (requestUri.equalsIgnoreCase(contextPath + WebServicesConstants.WS_VALIDAR_USUARIO_AC)) {
			httpServletResponse.addHeader("estadoSesion", "ok");
			filterChain.doFilter(servletRequest, servletResponse);
		}else if (requestUri.equalsIgnoreCase(contextPath + WebServicesConstants.WS_GUARDAR_ERRORES)){	
			httpServletResponse.addHeader("estadoSesion", "ok");
			filterChain.doFilter(servletRequest, servletResponse);
		}else if (requestUri.equalsIgnoreCase(contextPath + WebServicesConstants.WS_LISTAR_MONITOREO_CLIENTE)){	
			httpServletResponse.addHeader("estadoSesion", "ok");
			filterChain.doFilter(servletRequest, servletResponse);	
		}else {
			if (verificarSesion(httpSession,httpServletRequest) == 0) {
				httpServletResponse.addHeader("estadoSesion", "SESION_CADUCADA");
				servletRequest.getRequestDispatcher(WebServicesConstants.SEGURIDAD_NO_INGRESO).forward(httpServletRequest,
				        httpServletResponse);
			}else if (verificarSesion(httpSession,httpServletRequest) == 99) {
				httpServletResponse.addHeader("estadoSesion", "SESION_CADUCADA");
				servletRequest.getRequestDispatcher(WebServicesConstants.SEGURIDAD_VULNERABILIDAD).forward(httpServletRequest,
				        httpServletResponse);
			}else {
				httpServletResponse.addHeader("estadoSesion", "ok");
				filterChain.doFilter(servletRequest, servletResponse);
			}
		}
	}

	private int verificarSesion(HttpSession session,HttpServletRequest httpServletRequest) {
		int ind = 0;
		
		if (session != null) {
			if (session.getAttribute(WebServicesConstants.BEAN_SESSION) != null) {
				SessionBean sessionBean = (SessionBean) session.getAttribute(WebServicesConstants.BEAN_SESSION);
				//session.setMaxInactiveInterval(Integer.valueOf(PropiedadesAgenda.getPropiedad("tiempo_sesion")).intValue() * 60);
				String doc = httpServletRequest.getHeader("nro_doc") == null ? "" : httpServletRequest.getHeader("nro_doc").toString();
				if (doc.equals(sessionBean.getUser().getNro_documento()) && 
						UsuariosSesionUnica.getListaUsuariosSesion().get(sessionBean.getUser().getNro_documento()).equals(session.getId()))
					ind = 1; // todo ok
				else
					ind = 99; // error por vulnerabilidad
			}
		}

		return ind;

	}
	
}
