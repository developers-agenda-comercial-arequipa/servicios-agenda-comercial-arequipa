package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.services.BasesService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class BasesController {

	// Properties properties = new Properties();

	@Autowired
	private BasesService hojaRutaService;
	private static final Logger logger = LoggerFactory.getLogger(BasesController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws SQLException 
	 */

	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_DEPARTAMENTOS, method = RequestMethod.POST)
	public ResponseEntity<ListarDepartamentos_OUT> listarDepartamentos_OUT(
			@RequestBody ListarDepartamentos_IN listarDepartamentos_IN) throws SQLException {
		ListarDepartamentos_OUT listarDepartamentos_OUT = new ListarDepartamentos_OUT();


			listarDepartamentos_OUT = hojaRutaService.listarDepartamentos_OUT(listarDepartamentos_IN);

		return new ResponseEntity<ListarDepartamentos_OUT>(listarDepartamentos_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_PROVINCIAS, method = RequestMethod.POST)
	public ResponseEntity<ListarProvincias_OUT> listarProvincias_OUT(
			@RequestBody ListarProvincias_IN listarProvincias_IN) throws SQLException {
		ListarProvincias_OUT listarProvincias_OUT = new ListarProvincias_OUT();

			listarProvincias_OUT = hojaRutaService.listarProvincias_OUT(listarProvincias_IN);


		return new ResponseEntity<ListarProvincias_OUT>(listarProvincias_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_DISTRITOS, method = RequestMethod.POST)
	public ResponseEntity<ListarDistritos_OUT> listarDistritos_OUT(@RequestBody ListarDistritos_IN listarDistritos_IN) throws SQLException {
		ListarDistritos_OUT listarDistritos_OUT = new ListarDistritos_OUT();


			listarDistritos_OUT = hojaRutaService.listarDistritos_OUT(listarDistritos_IN);


		return new ResponseEntity<ListarDistritos_OUT>(listarDistritos_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_TIPOS_DIRECCION, method = RequestMethod.POST)
	public ResponseEntity<ListarTiposDireccion_OUT> listarTiposDireccion_OUT(
			@RequestBody ListarTiposDireccion_IN listarTiposDireccion_IN) throws SQLException {
		ListarTiposDireccion_OUT listarTiposDireccion_OUT = new ListarTiposDireccion_OUT();

			listarTiposDireccion_OUT = hojaRutaService.listarTiposDireccion_OUT(listarTiposDireccion_IN);

		return new ResponseEntity<ListarTiposDireccion_OUT>(listarTiposDireccion_OUT, HttpStatus.OK);
	}

	/*@RequestMapping(value = WebServicesConstants.WS_GUARDAR_CLIENTE_GEO, method = RequestMethod.POST)
	public ResponseEntity<GuardarClienteGeo_OUT> guardarClienteGeo_OUT(
			@RequestBody GuardarClienteGeo_IN guardarClienteGeo_IN) {
		GuardarClienteGeo_OUT guardarClienteGeo_OUT = new GuardarClienteGeo_OUT();

		try {
			guardarClienteGeo_OUT = hojaRutaService.guardarClienteGeo_OUT(guardarClienteGeo_IN);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return new ResponseEntity<GuardarClienteGeo_OUT>(guardarClienteGeo_OUT, HttpStatus.OK);
	}*/

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_CLIENTE_GEO_C, method = RequestMethod.POST)
	public ResponseEntity<GuardarClienteGeoC_OUT> guardarClienteGeoC_OUT(
			@RequestBody GuardarClienteGeoC_IN guardarClienteGeoC_IN) {
		GuardarClienteGeoC_OUT guardarClienteGeoC_OUT = new GuardarClienteGeoC_OUT();

		try {
			guardarClienteGeoC_OUT = hojaRutaService.guardarClienteGeoC_OUT(guardarClienteGeoC_IN);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return new ResponseEntity<GuardarClienteGeoC_OUT>(guardarClienteGeoC_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CANTIDAD_BASES, method = RequestMethod.POST)
	public ResponseEntity<ListarCantidadBases_OUT> listarCantidadBases_OUT(
			@RequestBody ListarCantidadCartera_IN listarCantidadCartera_IN) throws SQLException {
		ListarCantidadBases_OUT listarCantidadBases_OUT = new ListarCantidadBases_OUT();

			listarCantidadBases_OUT = hojaRutaService.listarCantidadBases_OUT(listarCantidadCartera_IN);


		return new ResponseEntity<ListarCantidadBases_OUT>(listarCantidadBases_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_BASES_DETALLE, method = RequestMethod.POST)
	public ResponseEntity<ListarBasesDetalle_OUT> listarBasesDetalle_OUT(
			@RequestBody ListarBasesDetalle_IN listarCarteraDetalle_IN) throws SQLException {
		ListarBasesDetalle_OUT listarBasesDetalle_OUT = new ListarBasesDetalle_OUT();

			listarBasesDetalle_OUT = hojaRutaService.listarBasesDetalle_OUT(listarCarteraDetalle_IN);

		return new ResponseEntity<ListarBasesDetalle_OUT>(listarBasesDetalle_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_OPORTUNIDAD_XFILTRO, method = RequestMethod.POST)
	public ResponseEntity<ListarBasesDetalle_OUT> listarBasesOportunidad_OUT(
			@RequestBody ListarBasesDetalle_IN listarCarteraDetalle_IN) throws SQLException {
		ListarBasesDetalle_OUT listarBasesDetalle_OUT = new ListarBasesDetalle_OUT();

		listarBasesDetalle_OUT = hojaRutaService.listarOportunidadXFiltro_OUT(listarCarteraDetalle_IN);

		return new ResponseEntity<ListarBasesDetalle_OUT>(listarBasesDetalle_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_ULT_PAGO, method = RequestMethod.POST)
	public ResponseEntity<ListarUltimosPagos_OUT> listarUltimosPagos_OUT(
			@RequestBody ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException {
		ListarUltimosPagos_OUT listarUltimosPagos_OUT;

		listarUltimosPagos_OUT = hojaRutaService.listarUltimosPagos_OUT(listarInfoCliente_IN);

		return new ResponseEntity<>(listarUltimosPagos_OUT, HttpStatus.OK);
	}

}
