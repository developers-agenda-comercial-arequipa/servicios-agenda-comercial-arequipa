package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.services.ReportesService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ReportesController {

	// Properties properties = new Properties();
	
	@Autowired
	private ReportesService reportesService;
	private static final Logger logger = LoggerFactory.getLogger(ReportesController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws SQLException 
	 */
	
	@RequestMapping(value = WebServicesConstants.WS_REPORTE_ASIGNADOS, method = RequestMethod.POST)
	public ResponseEntity<ReporteAsignados_OUT> reporteAsignados_OUT(
			@RequestBody ReporteAsignados_IN reporteAsignados_IN) throws SQLException {
		ReporteAsignados_OUT reporteAsignados_OUT = new ReporteAsignados_OUT();

			reporteAsignados_OUT = reportesService.reporteAsignados_OUT(reporteAsignados_IN);

		return new ResponseEntity<ReporteAsignados_OUT>(reporteAsignados_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_REPORTE_GESTION, method = RequestMethod.POST)
	public ResponseEntity<ReporteGestion_OUT> reporteGestion_OUT(@RequestBody ReporteGestion_IN reporteGestion_IN) throws SQLException{
		
		ReporteGestion_OUT reporteGestion_OUT = new ReporteGestion_OUT();
		
			reporteGestion_OUT = reportesService.reporteGestion_OUT(reporteGestion_IN);
		
		return new ResponseEntity<ReporteGestion_OUT>(reporteGestion_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_ACTIVIDAD_BASE, method = RequestMethod.POST)
	public ResponseEntity<ListarActividad_OUT> listarActividad_OUT(
			@RequestBody ListarBasesDetalle_IN listarBasesDetalle_IN) throws SQLException {
		ListarActividad_OUT listarActividad_OUT = new ListarActividad_OUT();

			listarActividad_OUT = reportesService.listarActividad_OUT(listarBasesDetalle_IN);

		return new ResponseEntity<ListarActividad_OUT>(listarActividad_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_REPORTES, method = RequestMethod.POST)
	public ResponseEntity<ListarReportes_OUT> listarReportes_OUT(@RequestBody General_IN general_IN) throws SQLException{

		ListarReportes_OUT listarReportes_OUT;

		listarReportes_OUT = reportesService.listarReportes_OUT(general_IN);

		return new ResponseEntity<>(listarReportes_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_OBTENER_REPORTE, method = RequestMethod.POST)
	public ResponseEntity<ObtenerReporte_OUT> ObtenerReporte_OUT(@RequestBody ObtenerReporte_IN obtenerReporte_IN) throws SQLException{

		ObtenerReporte_OUT obtenerReporte_OUT;

		obtenerReporte_OUT = reportesService.obtenerReporte_OUT(obtenerReporte_IN);

		return new ResponseEntity<>(obtenerReporte_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_BASES, method = RequestMethod.POST)
	public ResponseEntity<ListarBasesOUT> ListarBase_OUT(@RequestBody ListarBases_IN listarBases_in) throws SQLException{

		ListarBasesOUT listarBasesOUT;

		listarBasesOUT = reportesService.listarBase_OUT(listarBases_in);

		return new ResponseEntity<>(listarBasesOUT, HttpStatus.OK);
	}
}
