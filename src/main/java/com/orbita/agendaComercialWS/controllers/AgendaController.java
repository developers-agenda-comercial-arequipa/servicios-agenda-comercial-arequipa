package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.services.AgendaService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AgendaController {

	// Properties properties = new Properties();

	@Autowired
	private AgendaService agendaService;
	private static final Logger logger = LoggerFactory.getLogger(AgendaController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws SQLException 
	 */

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CANTIDAD_CITAS_CALENDARIO, method = RequestMethod.POST)
	public ResponseEntity<ListarCantidadCitasCalendario_OUT> listarCitasPorMes_OUT(
			@RequestBody ListarCantidadCitasCalendario_IN listarCitasPorMes_IN) throws SQLException {
		ListarCantidadCitasCalendario_OUT listarCitasPorMes_OUT = new ListarCantidadCitasCalendario_OUT();

			listarCitasPorMes_OUT = agendaService.listarCitasPorMes_OUT(listarCitasPorMes_IN);

		return new ResponseEntity<ListarCantidadCitasCalendario_OUT>(listarCitasPorMes_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CITAS_POR_DIA, method = RequestMethod.POST)
	public ResponseEntity<ListarCitasPorDia_OUT> listarCitasPorDia_OUT(
			@RequestBody ListarCitasPorDia_IN listarCitasPorDia_IN) throws SQLException {
		ListarCitasPorDia_OUT listarCitasPorDia_OUT = new ListarCitasPorDia_OUT();

			listarCitasPorDia_OUT = agendaService.listarCitasPorDia_OUT(listarCitasPorDia_IN);

		return new ResponseEntity<ListarCitasPorDia_OUT>(listarCitasPorDia_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CITAS_POR_DIA_GEO, method = RequestMethod.POST)
	public ResponseEntity<ListarCitasPorDiaGeo_OUT> listarCitasPorDiaNoGeo_OUT(
			@RequestBody ListarMiRadar_IN listarMiRadar_IN) throws SQLException {
		ListarCitasPorDiaGeo_OUT listarCitasPorDiaGeo_OUT = new ListarCitasPorDiaGeo_OUT();

			listarCitasPorDiaGeo_OUT = agendaService.listarCitasPorDiaGeo_OUT(listarMiRadar_IN);

		return new ResponseEntity<ListarCitasPorDiaGeo_OUT>(listarCitasPorDiaGeo_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_OBTENER_DETALLE_CITA, method = RequestMethod.POST)
	public ResponseEntity<ObtenerDetalleCita_OUT> obtenerDetalleCita_OUT(
			@RequestBody ObtenerDetalleCita_IN obtenerDetalleCita_IN) throws SQLException {
		ObtenerDetalleCita_OUT obtenerDetalleCita_OUT = new ObtenerDetalleCita_OUT();

			obtenerDetalleCita_OUT = agendaService.obtenerDetalleCita_OUT(obtenerDetalleCita_IN);
			
		return new ResponseEntity<ObtenerDetalleCita_OUT>(obtenerDetalleCita_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_MOTIVOS_NO_CONTACTADOS, method = RequestMethod.POST)
	public ResponseEntity<ListarMotivosNoContactado_OUT> listarMotivosNoContactados_OUT(
			@RequestBody ListarMotivosNoContactado_IN listarMotivosNoContactados_IN) throws SQLException {
		ListarMotivosNoContactado_OUT listarMotivosNoContactados_OUT = new ListarMotivosNoContactado_OUT();

			listarMotivosNoContactados_OUT = agendaService
					.listarMotivosNoContactados_OUT(listarMotivosNoContactados_IN);

		return new ResponseEntity<ListarMotivosNoContactado_OUT>(listarMotivosNoContactados_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_REACCIONES, method = RequestMethod.POST)
	public ResponseEntity<ListarResultadosCita_OUT> listarResultadosCita_OUT(
			@RequestBody ListarResultadosCita_IN listarResultadosCita_IN) throws SQLException {
		ListarResultadosCita_OUT listarResultadosCita_OUT = new ListarResultadosCita_OUT();

			listarResultadosCita_OUT = agendaService.listarResultadosCita_OUT(listarResultadosCita_IN);

		return new ResponseEntity<ListarResultadosCita_OUT>(listarResultadosCita_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_REACCIONES_DETALLE, method = RequestMethod.POST)
	public ResponseEntity<ListarResultadosCitaDetalle_OUT> listarResultadosCitaDetalle_OUT(
			@RequestBody ListarResultadosCitaDetalle_IN listarResultadosCitaDetalle_IN) throws SQLException {
		ListarResultadosCitaDetalle_OUT listarResultadosCitaDetalle_OUT = new ListarResultadosCitaDetalle_OUT();

			listarResultadosCitaDetalle_OUT = agendaService
					.listarResultadosCitaDetalle_OUT(listarResultadosCitaDetalle_IN);

		return new ResponseEntity<ListarResultadosCitaDetalle_OUT>(listarResultadosCitaDetalle_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_RESULTADO_CITA_NC, method = RequestMethod.POST)
	public ResponseEntity<GuardarResultadoCitaNC_OUT> guardarResultadoCitaNC_OUT(
			@RequestBody GuardarResultadoCitaNC_IN guardarResultadoCitaNC_IN) throws SQLException {
		GuardarResultadoCitaNC_OUT guardarResultadoCitaNC_OUT = new GuardarResultadoCitaNC_OUT();

			guardarResultadoCitaNC_OUT = agendaService.guardarResultadoCitaNC_OUT(guardarResultadoCitaNC_IN);

		return new ResponseEntity<GuardarResultadoCitaNC_OUT>(guardarResultadoCitaNC_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_RESULTADO_CITA_SC, method = RequestMethod.POST)
	public ResponseEntity<GuardarResultadoCitaSC_OUT> guardarResultadoCitaSC_OUT(
			@RequestBody GuardarResultadoCitaSC_IN guardarResultadoCitaSC_IN) throws SQLException {
		GuardarResultadoCitaSC_OUT guardarResultadoCitaSC_OUT = new GuardarResultadoCitaSC_OUT();

			guardarResultadoCitaSC_OUT = agendaService.guardarResultadoCitaSC_OUT(guardarResultadoCitaSC_IN);

		return new ResponseEntity<GuardarResultadoCitaSC_OUT>(guardarResultadoCitaSC_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_FOTO_CITA, method = RequestMethod.POST)
	public ResponseEntity<GuardarFotoCita_OUT> guardarFotoCita_OUT(@RequestBody GuardarFotoCita_IN guardarFotoCita_IN) throws SQLException {
		GuardarFotoCita_OUT guardarFotoCita_OUT = new GuardarFotoCita_OUT();

			guardarFotoCita_OUT = agendaService.guardarFotoCita_OUT(guardarFotoCita_IN);

		return new ResponseEntity<GuardarFotoCita_OUT>(guardarFotoCita_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CITAS_POR_MES, method = RequestMethod.POST)
	public ResponseEntity<ListarCitasPorMes_OUT> listarCitasPorMes_OUT(
			@RequestBody ListarCitasPorMes_IN listarCitasPorMes_IN) throws SQLException {
		ListarCitasPorMes_OUT listarCitasPorMes_OUT = new ListarCitasPorMes_OUT();

			listarCitasPorMes_OUT = agendaService.listarCitasPorMes_OUT(listarCitasPorMes_IN);

		return new ResponseEntity<ListarCitasPorMes_OUT>(listarCitasPorMes_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CARTERA_MAPA, method = RequestMethod.POST)
	public ResponseEntity<ListarCarteraMapa_OUT> listarCarteraMapa_OUT(
			@RequestBody ListarMiRadar_IN listarMiRadar_IN) throws SQLException {
		ListarCarteraMapa_OUT listarCarteraMapa_OUT = new ListarCarteraMapa_OUT();

			listarCarteraMapa_OUT = agendaService.listarCarteraMapa_OUT(listarMiRadar_IN);

		return new ResponseEntity<ListarCarteraMapa_OUT>(listarCarteraMapa_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_OBTENER_INFO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ObtenerInfoCliente_OUT> obtenerInfoCliente_OUT(
			@RequestBody ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		ObtenerInfoCliente_OUT obtenerInfoCliente_OUT = new ObtenerInfoCliente_OUT();

			obtenerInfoCliente_OUT = agendaService.obtenerInfoCliente_OUT(obtenerInfoCliente_IN);

		return new ResponseEntity<ObtenerInfoCliente_OUT>(obtenerInfoCliente_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_INFO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<GuardarInfoCliente_OUT> guardarInfoCliente_OUT(
			@RequestBody GuardarInfoCliente_IN guardarInfoCliente_IN) throws SQLException {
		GuardarInfoCliente_OUT guardarInfoCliente_OUT = new GuardarInfoCliente_OUT();

			guardarInfoCliente_OUT = agendaService.guardarInfoCliente_OUT(guardarInfoCliente_IN);

		return new ResponseEntity<GuardarInfoCliente_OUT>(guardarInfoCliente_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_CLIENTE_DIRECCION, method = RequestMethod.POST)
	public ResponseEntity<GuardarInfoClienteDireccion_OUT> guardarInfoClienteDireccion_OUT(
			@RequestBody GuardarInfoClienteDireccion_IN guardarInfoClienteDireccion_IN) throws SQLException {
		GuardarInfoClienteDireccion_OUT guardarInfoClienteDireccion_OUT = new GuardarInfoClienteDireccion_OUT();

			guardarInfoClienteDireccion_OUT = agendaService
					.guardarInfoClienteDireccion_OUT(guardarInfoClienteDireccion_IN);

		return new ResponseEntity<GuardarInfoClienteDireccion_OUT>(guardarInfoClienteDireccion_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_DIRECCIONES_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarDireccionesCliente_OUT> listarDireccionesCliente_OUT(
			@RequestBody ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		ListarDireccionesCliente_OUT listarDireccionesCliente_OUT = new ListarDireccionesCliente_OUT();

			listarDireccionesCliente_OUT = agendaService.listarDireccionesCliente_OUT(obtenerInfoCliente_IN);

		return new ResponseEntity<ListarDireccionesCliente_OUT>(listarDireccionesCliente_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_BASES_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarBasesCliente_OUT> listarBasesCliente_OUT(
			@RequestBody ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		ListarBasesCliente_OUT listarBasesCliente_OUT = new ListarBasesCliente_OUT();

			listarBasesCliente_OUT = agendaService.listarBasesCliente_OUT(obtenerInfoCliente_IN);

		return new ResponseEntity<ListarBasesCliente_OUT>(listarBasesCliente_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<GuardarCliente_OUT> guardarCliente_OUT(
			@RequestBody GuardarCliente_IN guardarCliente_IN) throws SQLException {
		GuardarCliente_OUT guardarCliente_OUT = new GuardarCliente_OUT();

			guardarCliente_OUT = agendaService.guardarCliente_OUT(guardarCliente_IN);
			
		return new ResponseEntity<GuardarCliente_OUT>(guardarCliente_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CITA_RESULTADOS, method = RequestMethod.POST)
	public ResponseEntity<ListarCitaResultado_OUT> listarCitaResultado_OUT(
			@RequestBody ObtenerDetalleCita_IN obtenerDetalleCita_IN) throws SQLException{
		ListarCitaResultado_OUT listarCitaResultado_OUT = new ListarCitaResultado_OUT();

			listarCitaResultado_OUT = agendaService.listarCitaResultado_OUT(obtenerDetalleCita_IN);

		return new ResponseEntity<ListarCitaResultado_OUT>(listarCitaResultado_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_BAJA_INFO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ResponseAgenda_OUT> bajaInfoCliente_OUT(
			@RequestBody BajaInfoCliente_IN bajaInfoCliente_IN) throws SQLException{
		ResponseAgenda_OUT bajaInfoCliente_OUT = new ResponseAgenda_OUT();
		
			bajaInfoCliente_OUT = agendaService.bajaInfoCliente_OUT(bajaInfoCliente_IN);

		return new ResponseEntity<ResponseAgenda_OUT>(bajaInfoCliente_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_INFO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarClienteInfo_OUT> listarClienteInfo_OUT(
			@RequestBody ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException{
		ListarClienteInfo_OUT listarClienteInfo_OUT = new ListarClienteInfo_OUT();
		
			listarClienteInfo_OUT = agendaService.listarClienteInfo_OUT(2, obtenerInfoCliente_IN);
		
		return new ResponseEntity<ListarClienteInfo_OUT>(listarClienteInfo_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_TELEF_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarClienteTelef_OUT> listarClienteTelef_OUT(
			@RequestBody ListarTelefCliente_IN listarTelefCliente_IN) throws SQLException{
		ListarClienteTelef_OUT listarClienteTelef_OUT = new ListarClienteTelef_OUT();
		
			listarClienteTelef_OUT = agendaService.listarClienteTelef_OUT(listarTelefCliente_IN);
		
		return new ResponseEntity<ListarClienteTelef_OUT>(listarClienteTelef_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_DIRECC_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarClienteDireccion_OUT> listarClienteDireccion_OUT(
			@RequestBody ListarDireccCliente_IN listarDireccCliente_IN) throws SQLException{
		ListarClienteDireccion_OUT listarClienteDireccion_OUT = new ListarClienteDireccion_OUT();
		
			listarClienteDireccion_OUT = agendaService.listarClienteDireccion_OUT(listarDireccCliente_IN);

		return new ResponseEntity<ListarClienteDireccion_OUT>(listarClienteDireccion_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CITA_FOTOS, method = RequestMethod.POST)
	public ResponseEntity<ListarCitaFotos_OUT> listarCitaFotos_OUT(
			@RequestBody ObtenerDetalleCita_IN obtenerDetalleCita_IN) throws SQLException{
		ListarCitaFotos_OUT listarCitaFotos_OUT = new ListarCitaFotos_OUT();

			listarCitaFotos_OUT = agendaService.listarCitaFotos_OUT(obtenerDetalleCita_IN);
		
		return new ResponseEntity<ListarCitaFotos_OUT>(listarCitaFotos_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CLIENTE_RESULTADOS, method = RequestMethod.POST)
	public ResponseEntity<ListarClienteResultado_OUT> listarClienteResultado_OUT(
			@RequestBody ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException{
		ListarClienteResultado_OUT listarClienteResultado_OUT;

		listarClienteResultado_OUT = agendaService.listarClienteResultado_OUT(listarInfoCliente_IN);

		return new ResponseEntity<>(listarClienteResultado_OUT, HttpStatus.OK);
	}
}
