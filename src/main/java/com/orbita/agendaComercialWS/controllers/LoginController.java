package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialProperties.Properties;
import com.orbita.agendaComercialWS.dto.request.ValidarUsuarioAC_IN;
import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;
import com.orbita.agendaComercialWS.model.ErrorSeguridad;
import com.orbita.agendaComercialWS.seguridad.SessionBean;
import com.orbita.agendaComercialWS.seguridad.UsuariosSesionUnica;
import com.orbita.agendaComercialWS.services.LoginService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;
import com.orbita.agendaComercialWS.util.Seguridad;

/**
 * Handles requests for the application home page.
 */
@Controller
public class LoginController {
	
	Seguridad seg = new Seguridad();
	String usuario;
	
	@Autowired
	private LoginService loginService;
	//private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@RequestMapping(value = WebServicesConstants.WS_VALIDAR_USUARIO_AC, method = RequestMethod.POST)
	public ResponseEntity<ValidarUsuarioAC_OUT> validarUsuarioAC_OUT(@Valid @RequestBody ValidarUsuarioAC_IN validarUsuarioAC_IN, HttpSession session) throws SQLException {
		ValidarUsuarioAC_OUT validarUsuarioAC_OUT = new ValidarUsuarioAC_OUT();
		
		// Usuario
		usuario = validarUsuarioAC_IN.getUsuario().replace(Properties.LDAP_DC, " ");
		usuario = usuario.trim() + Properties.LDAP_DC;
		validarUsuarioAC_IN.setUsuario(usuario);
		
		//ErrorSeguridad error = seg.validarUsuarioLDAP(validarUsuarioAC_IN.getUsuario(), validarUsuarioAC_IN.getContrasena());
		ErrorSeguridad error = new ErrorSeguridad();
		error.setCodigo("1");

		if (error.getCodigo().equals("0")) {
			validarUsuarioAC_OUT.setCodigo_validacion(error.getCodigo());
			validarUsuarioAC_OUT.setMensaje(error.getMensaje());
			return new ResponseEntity<>(validarUsuarioAC_OUT, HttpStatus.OK);
		}
		
			usuario = validarUsuarioAC_IN.getUsuario().replace(Properties.LDAP_DC, "");
			validarUsuarioAC_IN.setUsuario(usuario);
			validarUsuarioAC_OUT = loginService.validarUsuarioAC_OUT(validarUsuarioAC_IN);
			SessionBean sessionBean = new SessionBean(validarUsuarioAC_OUT);
			
			session.setAttribute(WebServicesConstants.BEAN_SESSION, sessionBean);
			UsuariosSesionUnica.setListaUsuariosSesion(sessionBean.getUser().getNro_documento(), session.getId());
		
		return new ResponseEntity<>(validarUsuarioAC_OUT, HttpStatus.OK);
	}
	
}
