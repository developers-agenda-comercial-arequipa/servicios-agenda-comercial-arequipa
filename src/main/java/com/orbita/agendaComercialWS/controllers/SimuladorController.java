package com.orbita.agendaComercialWS.controllers;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarTarifario_IN;
import com.orbita.agendaComercialWS.dto.response.ListarProductoSegmento_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarTarifario_OUT;
import com.orbita.agendaComercialWS.services.SimuladorService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.SQLException;

@Controller
public class SimuladorController {


    @Autowired
    private SimuladorService simuladorService;

    @RequestMapping(value = WebServicesConstants.WS_LISTAR_PRODUCTO_SEGMENTO, method = RequestMethod.POST)
    public ResponseEntity<ListarProductoSegmento_OUT> listarProductoSegmento_OUT(
            @RequestBody General_IN general_IN) throws SQLException {
        ListarProductoSegmento_OUT listarProductoSegmento_OUT;

        listarProductoSegmento_OUT = simuladorService.listarProductoSegmento_OUT(general_IN);

        return new ResponseEntity<>(listarProductoSegmento_OUT, HttpStatus.OK);
    }

    @RequestMapping(value = WebServicesConstants.WS_LISTAR_TARIFARIO, method = RequestMethod.POST)
    public ResponseEntity<ListarTarifario_OUT> listarTarifario_OUT(
            @RequestBody ListarTarifario_IN listarTarifario_IN) throws SQLException {
        ListarTarifario_OUT listarTarifario_OUT;

        listarTarifario_OUT = simuladorService.listarTarifario_OUT(listarTarifario_IN);

        return new ResponseEntity<>(listarTarifario_OUT, HttpStatus.OK);
    }
}
