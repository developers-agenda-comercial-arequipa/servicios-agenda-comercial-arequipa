package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.dto.request.GuardarError_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMonitoreoCliente_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerContadorCitas_IN;
import com.orbita.agendaComercialWS.dto.request.Version_IN;
import com.orbita.agendaComercialWS.dto.response.ListarMonitoreoCliente_OUT;
import com.orbita.agendaComercialWS.dto.response.ObtenerContadorCitas_OUT;
import com.orbita.agendaComercialWS.dto.response.ResponseAgenda_OUT;
import com.orbita.agendaComercialWS.dto.response.Version_OUT;
import com.orbita.agendaComercialWS.services.HomeService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	// Properties properties = new Properties();

	@Autowired
	private HomeService homeService;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = WebServicesConstants.WS_OBTENER_CONTADOR_CITAS, method = RequestMethod.POST)
	public ResponseEntity<ObtenerContadorCitas_OUT> obtenerContadorCitas_OUT(
			@RequestBody ObtenerContadorCitas_IN obtenerContadorCitas_IN) throws SQLException {
		ObtenerContadorCitas_OUT obtenerContadorCitas_OUT = new ObtenerContadorCitas_OUT();

			obtenerContadorCitas_OUT = homeService.obtenerContadorCitas_OUT(obtenerContadorCitas_IN);

		return new ResponseEntity<ObtenerContadorCitas_OUT>(obtenerContadorCitas_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_MONITOREO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ListarMonitoreoCliente_OUT> listarMonitoreoCliente_OUT(
			@RequestBody ListarMonitoreoCliente_IN listarMonitoreoCliente_IN) throws SQLException{
		ListarMonitoreoCliente_OUT listarMonitoreoCliente_OUT = new ListarMonitoreoCliente_OUT();

		try {
			listarMonitoreoCliente_OUT = homeService.listarMonitoreoCliente_OUT(listarMonitoreoCliente_IN);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return new ResponseEntity<ListarMonitoreoCliente_OUT>(listarMonitoreoCliente_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_GUARDAR_ERRORES, method = RequestMethod.POST)
	public ResponseEntity<ResponseAgenda_OUT> error_OUT(@RequestBody GuardarError_IN guardarError_IN) throws SQLException {
		ResponseAgenda_OUT reponse_OUT = new ResponseAgenda_OUT();
		
		try {
			reponse_OUT =  homeService.GuardarError(guardarError_IN);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return new ResponseEntity<ResponseAgenda_OUT>(reponse_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_VERSION, method = RequestMethod.POST)
	public ResponseEntity<Version_OUT> version_OUT(
			@RequestBody Version_IN version_IN) throws SQLException {
		Version_OUT version_OUT = new Version_OUT();

			version_OUT = homeService.version_OUT(version_IN);

		return new ResponseEntity<Version_OUT>(version_OUT, HttpStatus.OK);
	}
}
