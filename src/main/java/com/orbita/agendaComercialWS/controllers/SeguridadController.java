package com.orbita.agendaComercialWS.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orbita.agendaComercialWS.dto.response.ErrorControlado_OUT;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

@Controller
public class SeguridadController {
	
	@RequestMapping(value = WebServicesConstants.SEGURIDAD_NO_INGRESO)
	public ResponseEntity<Object> validarUsuarioAC_OUT(HttpSession session) {
		ErrorControlado_OUT validarUsuarioAC_OUT = new ErrorControlado_OUT();
		session.removeAttribute(WebServicesConstants.BEAN_SESSION);
		validarUsuarioAC_OUT.setCodigo_validacion("99");
		validarUsuarioAC_OUT.setMensaje("ERROR: " + "Estimado Cliente por favor vuelva a ingresar.");
		//99 es cuando pierde la sesion por tiempo
		//98 es cuando pierde la sesion por intrusos
		//97 es cuando sucedio un error con la base de datos
		//96 cuando sucede un error de programador
		return new ResponseEntity<Object>(validarUsuarioAC_OUT, HttpStatus.FORBIDDEN);
	}
	
	@RequestMapping(value = WebServicesConstants.SEGURIDAD_VULNERABILIDAD)
	public ResponseEntity<Object> validarUsuarioVulnerabilidad_OUT(HttpSession session) {
		ErrorControlado_OUT validarUsuarioAC_OUT = new ErrorControlado_OUT();
		session.removeAttribute(WebServicesConstants.BEAN_SESSION);
		validarUsuarioAC_OUT.setCodigo_validacion("98");
		validarUsuarioAC_OUT.setMensaje("ERROR: " + "Estimado Cliente por favor vuelva a ingresar, ocurri� un error de vulnerabilidad");
		//99 es cuando pierde la sesion por tiempo
		//98 es cuando pierde la sesion por intrusos
		//97 es cuando sucedio un error con la base de datos
		//96 cuando sucede un error de programador
		return new ResponseEntity<Object>(validarUsuarioAC_OUT, HttpStatus.FORBIDDEN);
	}
	
}
