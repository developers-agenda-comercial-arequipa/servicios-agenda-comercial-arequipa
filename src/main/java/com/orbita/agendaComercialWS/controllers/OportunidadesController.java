package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMiRadar_IN;
import com.orbita.agendaComercialWS.dto.response.ListarActividadOportunidad_OUT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.dto.request.ListarCarteraInactivo_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerInfoCliente_IN;
import com.orbita.agendaComercialWS.dto.response.ListarBasesOportunidad_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarCarteraInactivo_OUT;
import com.orbita.agendaComercialWS.services.OportunidadesService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class OportunidadesController {

	// Properties properties = new Properties();

	@Autowired
	private OportunidadesService clientesInactivosService;
	private static final Logger logger = LoggerFactory.getLogger(OportunidadesController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */

	@RequestMapping(value = WebServicesConstants.WS_LISTAR_CARTERA_INACTIVA, method = RequestMethod.POST)
	public ResponseEntity<ListarCarteraInactivo_OUT> listarCarteraInactivo_OUT(
			@RequestBody ListarMiRadar_IN listarMiRadar_IN) {
		ListarCarteraInactivo_OUT listarCarteraInactivo_OUT = new ListarCarteraInactivo_OUT();

		try {
			listarCarteraInactivo_OUT = clientesInactivosService.listarCarteraInactivo_OUT(listarMiRadar_IN);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return new ResponseEntity<ListarCarteraInactivo_OUT>(listarCarteraInactivo_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_LISTAR_BASES_OPORTUNIDAD, method = RequestMethod.POST)
	public ResponseEntity<ListarBasesOportunidad_OUT> listarBasesOportunidad_OUT(
			@RequestBody ObtenerInfoCliente_IN obtenerInfoCliente_IN) throws SQLException {
		ListarBasesOportunidad_OUT listarBasesOportunidad_OUT = new ListarBasesOportunidad_OUT();

			listarBasesOportunidad_OUT = clientesInactivosService.listarOportunindad_OUT(obtenerInfoCliente_IN);

		return new ResponseEntity<ListarBasesOportunidad_OUT>(listarBasesOportunidad_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_ACTIVIDAD_OPORTUNIDAD, method = RequestMethod.POST)
	public ResponseEntity<ListarActividadOportunidad_OUT> listarActividadOportunidad_OUT(
			@RequestBody General_IN general_IN) throws SQLException {
		ListarActividadOportunidad_OUT listarActividadOportunidad_OUT;

		listarActividadOportunidad_OUT = clientesInactivosService.listarActividadOportunidad_OUT(general_IN);

		return new ResponseEntity<>(listarActividadOportunidad_OUT, HttpStatus.OK);
	}
}
