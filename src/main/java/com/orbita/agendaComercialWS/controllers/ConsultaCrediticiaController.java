package com.orbita.agendaComercialWS.controllers;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.AgendarCitaMasivo_IN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orbita.agendaComercialWS.dto.request.AgendarCita_IN;
import com.orbita.agendaComercialWS.dto.request.ValidarCliente_IN;
import com.orbita.agendaComercialWS.dto.response.AgendarCita_OUT;
import com.orbita.agendaComercialWS.dto.response.ValidarCliente_OUT;
import com.orbita.agendaComercialWS.services.ConsultaCrediticiaService;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ConsultaCrediticiaController {

	// Properties properties = new Properties();

	@Autowired
	private ConsultaCrediticiaService consultaCrediticiaService;
	private static final Logger logger = LoggerFactory.getLogger(ConsultaCrediticiaController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws SQLException 
	 */

	@RequestMapping(value = WebServicesConstants.WS_AGENDAR_CITA, method = RequestMethod.POST)
	public ResponseEntity<AgendarCita_OUT> agendarCita_OUT(@RequestBody AgendarCita_IN agendarCita_IN) throws SQLException {
		AgendarCita_OUT agendarCita_OUT = new AgendarCita_OUT();

			agendarCita_OUT = consultaCrediticiaService.agendarCita_OUT(agendarCita_IN);

		return new ResponseEntity<AgendarCita_OUT>(agendarCita_OUT, HttpStatus.OK);
	}

	@RequestMapping(value = WebServicesConstants.WS_AGENDAR_CITA_MASIVO, method = RequestMethod.POST)
	public ResponseEntity<AgendarCita_OUT> agendarCitaMasivo_OUT(@RequestBody AgendarCitaMasivo_IN agendarCitaMasivo_IN) throws SQLException {
		AgendarCita_OUT agendarCita_OUT;

		agendarCita_OUT = consultaCrediticiaService.agendarCitaMasivo_OUT(agendarCitaMasivo_IN);

		return new ResponseEntity<>(agendarCita_OUT, HttpStatus.OK);
	}
	
	@RequestMapping(value = WebServicesConstants.WS_VALIDAR_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ValidarCliente_OUT> validarCliente_OUT(@RequestBody ValidarCliente_IN validarCliente_IN) throws SQLException {
		ValidarCliente_OUT validarCliente_OUT = new ValidarCliente_OUT();

			validarCliente_OUT = consultaCrediticiaService.validarCliente_OUT(validarCliente_IN);

		return new ResponseEntity<ValidarCliente_OUT>(validarCliente_OUT, HttpStatus.OK);
	}
	

	@RequestMapping(value = WebServicesConstants.WS_VALIDAR_EJECUTIVO_CLIENTE, method = RequestMethod.POST)
	public ResponseEntity<ValidarCliente_OUT> validarEjecutivoCliente_OUT(@RequestBody ValidarCliente_IN validarCliente_IN) throws SQLException {
		ValidarCliente_OUT validarCliente_OUT = new ValidarCliente_OUT();

			validarCliente_OUT = consultaCrediticiaService.validarEjecutivoCliente_OUT(validarCliente_IN);

		return new ResponseEntity<ValidarCliente_OUT>(validarCliente_OUT, HttpStatus.OK);
	}

}
