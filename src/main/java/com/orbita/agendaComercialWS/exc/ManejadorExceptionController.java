package com.orbita.agendaComercialWS.exc;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.orbita.agendaComercialWS.dto.response.ErrorControlado_OUT;
import com.orbita.agendaComercialWS.model.ErrorSeguridad;

@ControllerAdvice
public class ManejadorExceptionController {
	private static final Logger logger = LoggerFactory.getLogger(ManejadorExceptionController.class);
	
	@ExceptionHandler({ SQLException.class, CannotCreateTransactionException.class })
	public ResponseEntity<Object> resultadoErrorBD(Exception excCA, HttpServletResponse response) {
		logger.error("Error controlado BD::", excCA);
		
		ErrorControlado_OUT errorOut = new ErrorControlado_OUT();
		errorOut.setCodigo_validacion("97");
		errorOut.setMensaje("ERROR: " + "Ocurrio un error en la base de datos: ");
		//99 es cuando pierde la sesion por tiempo
		//98 es cuando pierde la sesion por intrusos
		//97 es cuando sucedio un error con la base de datos
		//96 cuando sucede un error de programador
		
		return new ResponseEntity<Object>(errorOut, HttpStatus.OK);
		
	}
	
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> resultadoPagina(Exception excCA, HttpServletResponse response) {
		logger.error("Error controlado Programador::", excCA);
		ErrorControlado_OUT errorOut = new ErrorControlado_OUT();
		errorOut.setCodigo_validacion("97");
		errorOut.setMensaje("ERROR: " + "Ocurrio un error de programación : ");
		//99 es cuando pierde la sesion por tiempo
		//98 es cuando pierde la sesion por intrusos
		//97 es cuando sucedio un error con la base de datos
		//96 cuando sucede un error de programador
		
		return new ResponseEntity<Object>(errorOut, HttpStatus.OK);
		
	}
	
	@ExceptionHandler({ MethodArgumentNotValidException.class })
	public ResponseEntity<Object> resultadoValidacion(MethodArgumentNotValidException excCA, HttpServletResponse response) {
		logger.error("Error controlado Login::", excCA);
		ErrorSeguridad errorSeguridad = new ErrorSeguridad();
		errorSeguridad.setCodigo("0");
		errorSeguridad.setMensaje(excCA.getBindingResult().getAllErrors().iterator().next().getDefaultMessage());
		//99 es cuando pierde la sesion por tiempo
		//98 es cuando pierde la sesion por intrusos
		//97 es cuando sucedio un error con la base de datos
		//96 cuando sucede un error de programador
		return new ResponseEntity<Object>(errorSeguridad, HttpStatus.OK);
				
	}
}
