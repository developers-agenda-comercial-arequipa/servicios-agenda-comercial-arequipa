package com.orbita.agendaComercialWS;

import java.sql.SQLException;
import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.ErrorPageFilter;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
//import org.springframework.session.web.http.DefaultCookieSerializer;

import oracle.jdbc.pool.OracleDataSource;

import com.orbita.agendaComercialProperties.Properties;

//import com.orbita.agendaComercialWS.seguridad.LoggingFilter;
//import com.orbita.agendaComercialWS.seguridad.SessionFilter;

@ServletComponentScan
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class AgendaComercialWsApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(AgendaComercialWsApplication.class, args);
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		setRegisterErrorPageFilter(false);
		return application.sources(AgendaComercialWsApplication.class);
    }
	
//	private void registerFilter(ServletContext servletContext) {
//	// session-filter
//	Dynamic filterRegistrationSesion = servletContext.addFilter("session-filter", SessionFilter.class);
//	filterRegistrationSesion.addMappingForUrlPatterns(null, false, "/*");
//
//	// logging-filter
//	Dynamic filterRegistration = servletContext.addFilter("logging-filter", LoggingFilter.class);
//	filterRegistration.addMappingForUrlPatterns(null, false, "/*");
//
//}
	
	@Bean
    DataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser(Properties.BD_USER);
        dataSource.setPassword(Properties.BD_PASSWORD);
        dataSource.setURL(Properties.BD_URL);
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }
	
	@Bean
	public ErrorPageFilter errorPageFilter() {
		return new ErrorPageFilter();
	}
	
	@Bean
	public FilterRegistrationBean<ErrorPageFilter> disableErrorFilter (ErrorPageFilter filter) {
		FilterRegistrationBean<ErrorPageFilter> filterRegistrationBean = new FilterRegistrationBean<ErrorPageFilter>();
		filterRegistrationBean.setFilter(filter);
		filterRegistrationBean.setEnabled(false);
		return filterRegistrationBean;
	}
	
}
