package com.orbita.agendaComercialWS.dto.request;

public class ListarResultadosCitaDetalle_IN {
	String nro_documento;
	String id_usuario_session;
	int id_resultado_cita;
	
	public ListarResultadosCitaDetalle_IN(String nro_documento, String id_usuario_session, int id_resultado_cita) {
		super();
		this.nro_documento = nro_documento;
		this.id_usuario_session = id_usuario_session;
		this.id_resultado_cita = id_resultado_cita;
	}

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public int getId_resultado_cita() {
		return id_resultado_cita;
	}

	public void setId_resultado_cita(int id_resultado_cita) {
		this.id_resultado_cita = id_resultado_cita;
	}

}
