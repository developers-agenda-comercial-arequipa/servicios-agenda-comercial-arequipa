package com.orbita.agendaComercialWS.dto.request;

public class GuardarFotoCita_IN {
	String nro_documento;
	String id_usuario_session;
	int id_cita;
	String nombre_foto;
	String archivo_foto;

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public int getId_cita() {
		return id_cita;
	}

	public void setId_cita(int id_cita) {
		this.id_cita = id_cita;
	}

	public String getNombre_foto() {
		return nombre_foto;
	}

	public void setNombre_foto(String nombre_foto) {
		this.nombre_foto = nombre_foto;
	}

	public String getArchivo_foto() {
		return archivo_foto;
	}

	public void setArchivo_foto(String archivo_foto) {
		this.archivo_foto = archivo_foto;
	}

}
