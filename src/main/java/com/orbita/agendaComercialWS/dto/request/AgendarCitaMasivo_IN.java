package com.orbita.agendaComercialWS.dto.request;

import com.orbita.agendaComercialWS.model.Cita;

import java.util.ArrayList;

public class AgendarCitaMasivo_IN {
	String nro_documento;
	ArrayList<Cita> citas;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public ArrayList<Cita> getCitas() {
		return citas;
	}

	public void setCitas(ArrayList<Cita> citas) {
		this.citas = citas;
	}
}
