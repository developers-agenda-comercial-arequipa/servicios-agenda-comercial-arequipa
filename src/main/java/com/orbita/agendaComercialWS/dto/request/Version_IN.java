package com.orbita.agendaComercialWS.dto.request;

public class Version_IN {

	private String nroDocumento;
	private String idVal;
	
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getIdVal() {
		return idVal;
	}
	public void setIdVal(String idVal) {
		this.idVal = idVal;
	}
	
	
}
