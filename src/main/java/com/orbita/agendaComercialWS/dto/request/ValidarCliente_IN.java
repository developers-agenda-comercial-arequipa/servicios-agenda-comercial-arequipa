package com.orbita.agendaComercialWS.dto.request;

public class ValidarCliente_IN {
	String nro_documento;
	String id_usuario_session;
	String nro_documento_cliente;

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public String getNro_documento_cliente() {
		return nro_documento_cliente;
	}

	public void setNro_documento_cliente(String nro_documento_cliente) {
		this.nro_documento_cliente = nro_documento_cliente;
	}

}
