package com.orbita.agendaComercialWS.dto.request;

public class ListarIndicadorActividad_IN {
	String nro_documento;
	String fecha;
	
	public ListarIndicadorActividad_IN(String nro_documento, String fecha) {
		super();
		this.nro_documento = nro_documento;
		this.fecha = fecha;
	}

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
