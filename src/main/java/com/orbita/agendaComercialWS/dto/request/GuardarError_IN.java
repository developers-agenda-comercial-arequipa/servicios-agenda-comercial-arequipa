package com.orbita.agendaComercialWS.dto.request;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.AudtError;

public class GuardarError_IN {

	ArrayList<AudtError> errores;

	public ArrayList<AudtError> getErrores() {
		return errores;
	}

	public void setErrores(ArrayList<AudtError> errores) {
		this.errores = errores;
	}
	
}
