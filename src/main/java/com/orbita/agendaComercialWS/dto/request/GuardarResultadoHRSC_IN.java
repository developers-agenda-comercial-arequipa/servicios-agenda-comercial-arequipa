package com.orbita.agendaComercialWS.dto.request;

public class GuardarResultadoHRSC_IN {
	String nro_documento;
	String id_usuario_session;
	int id_hoja_ruta_detalle;
	int id_result_cita;
	int id_result_cita_detalle;
	String correo;
	String telefono;
	String comentario;

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public int getId_hoja_ruta_detalle() {
		return id_hoja_ruta_detalle;
	}

	public void setId_hoja_ruta_detalle(int id_hoja_ruta_detalle) {
		this.id_hoja_ruta_detalle = id_hoja_ruta_detalle;
	}

	public int getId_result_cita() {
		return id_result_cita;
	}

	public void setId_result_cita(int id_result_cita) {
		this.id_result_cita = id_result_cita;
	}

	public int getId_result_cita_detalle() {
		return id_result_cita_detalle;
	}

	public void setId_result_cita_detalle(int id_result_cita_detalle) {
		this.id_result_cita_detalle = id_result_cita_detalle;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
