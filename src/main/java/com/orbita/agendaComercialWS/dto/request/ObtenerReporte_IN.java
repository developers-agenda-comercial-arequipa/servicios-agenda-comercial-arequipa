package com.orbita.agendaComercialWS.dto.request;

public class ObtenerReporte_IN extends General_IN{

    private String nomReporte;
    private String grupo;

    public String getNomReporte() {
        return nomReporte;
    }

    public void setNomReporte(String nomReporte) {
        this.nomReporte = nomReporte;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
}
