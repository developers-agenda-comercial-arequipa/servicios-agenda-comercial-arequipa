package com.orbita.agendaComercialWS.dto.request;

public class BajaInfoCliente_IN {

	private int tipo;
	private String nroDocumento;
	private int idInfo;
	
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public int getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(int idInfo) {
		this.idInfo = idInfo;
	}
	
	
	
}
