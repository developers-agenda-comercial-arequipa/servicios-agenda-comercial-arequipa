package com.orbita.agendaComercialWS.dto.request;

public class ReporteAsignados_IN {

	private int nroDocumento;
	private int idActividad;
	private int idBase;
	private int anio;
	private int mes;
	
	public int getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(int nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}
	public int getIdBase() {
		return idBase;
	}
	public void setIdBase(int idBase) {
		this.idBase = idBase;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	
}
