package com.orbita.agendaComercialWS.dto.request;

public class AgendarCita_IN {
	String nro_documento;
	String id_usuario_session;
	int id_cliente;
	int idGestion;
	String fecha;
	int origen;
	int nuevaCita;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public String getId_usuario_session() {
		return id_usuario_session;
	}
	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public int getIdGestion() {
		return idGestion;
	}
	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public int getNuevaCita() {
		return nuevaCita;
	}
	public void setNuevaCita(int nuevaCita) {
		this.nuevaCita = nuevaCita;
	}

}
