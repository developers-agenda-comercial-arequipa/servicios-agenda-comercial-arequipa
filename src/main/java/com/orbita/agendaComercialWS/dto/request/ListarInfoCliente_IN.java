package com.orbita.agendaComercialWS.dto.request;

public class ListarInfoCliente_IN {
	String nro_documento;
	int id_cliente;
	
	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
}
