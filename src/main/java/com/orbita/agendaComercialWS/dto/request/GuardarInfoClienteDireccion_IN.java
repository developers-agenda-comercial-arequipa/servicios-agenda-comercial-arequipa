package com.orbita.agendaComercialWS.dto.request;

public class GuardarInfoClienteDireccion_IN {
	String nro_documento;
	int id_cliente;
	String direccion;
	String referencia;
	int id_tipo_direccion;
	String idUbigeo;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public int getId_tipo_direccion() {
		return id_tipo_direccion;
	}
	public void setId_tipo_direccion(int id_tipo_direccion) {
		this.id_tipo_direccion = id_tipo_direccion;
	}
	public String getIdUbigeo() {
		return idUbigeo;
	}
	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}

}
