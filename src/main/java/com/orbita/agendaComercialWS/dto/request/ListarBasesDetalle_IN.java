package com.orbita.agendaComercialWS.dto.request;

import java.util.ArrayList;
import com.orbita.agendaComercialWS.model.Info;
import com.orbita.agendaComercialWS.model.Ubigeo;

public class ListarBasesDetalle_IN {
	String nro_documento;
	int origen;
	double latActual;
	double lonActual;
	int idActividad;
	int idBase;
	String nroDocCliente;
	ArrayList<Info> info;
	ArrayList<Ubigeo> ubigeos;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}

	public double getLatActual() {
		return latActual;
	}

	public void setLatActual(double latActual) {
		this.latActual = latActual;
	}

	public double getLonActual() {
		return lonActual;
	}

	public void setLonActual(double lonActual) {
		this.lonActual = lonActual;
	}

	public int getIdActividad() {
		return idActividad;
	}
	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

	public int getIdBase() {
		return idBase;
	}

	public void setIdBase(int idBase) {
		this.idBase = idBase;
	}

	public String getNroDocCliente() {
		return nroDocCliente;
	}

	public void setNroDocCliente(String nroDocCliente) {
		this.nroDocCliente = nroDocCliente;
	}
	public ArrayList<Info> getInfo() {
		return info;
	}
	public void setInfo(ArrayList<Info> info) {
		this.info = info;
	}
	public ArrayList<Ubigeo> getUbigeos() {
		return ubigeos;
	}
	public void setUbigeos(ArrayList<Ubigeo> ubigeos) {
		this.ubigeos = ubigeos;
	}

}
