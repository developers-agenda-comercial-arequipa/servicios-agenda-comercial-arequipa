package com.orbita.agendaComercialWS.dto.request;

public class GuardarResultadoHRNC_IN {
	String nro_documento;
	String id_usuario_session;
	int id_hoja_ruta_detalle;
	int id_motivo_nc;
	String comentario;

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public int getId_hoja_ruta_detalle() {
		return id_hoja_ruta_detalle;
	}

	public void setId_hoja_ruta_detalle(int id_hoja_ruta_detalle) {
		this.id_hoja_ruta_detalle = id_hoja_ruta_detalle;
	}

	public int getId_motivo_nc() {
		return id_motivo_nc;
	}

	public void setId_motivo_nc(int id_motivo_nc) {
		this.id_motivo_nc = id_motivo_nc;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
