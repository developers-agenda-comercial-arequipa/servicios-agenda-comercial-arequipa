package com.orbita.agendaComercialWS.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size; 

public class ValidarUsuarioAC_IN {
	
	@NotNull
	@Size(min=1, message="Estimado cliente ingrese el usuario")
	String usuario;
	
	@NotNull
	@Size(min=1, message="Estimado cliente ingrese la contraseña")
	String contrasena;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario.trim();
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena.trim();
	}

}
