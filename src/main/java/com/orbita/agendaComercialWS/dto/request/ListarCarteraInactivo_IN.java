package com.orbita.agendaComercialWS.dto.request;


public class ListarCarteraInactivo_IN {
	String nro_documento;
	String id_usuario_session;
	double latActual;
	double lonActual;
	
	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public double getLatActual() {
		return latActual;
	}

	public void setLatActual(double latActual) {
		this.latActual = latActual;
	}

	public double getLonActual() {
		return lonActual;
	}

	public void setLonActual(double lonActual) {
		this.lonActual = lonActual;
	}

}
