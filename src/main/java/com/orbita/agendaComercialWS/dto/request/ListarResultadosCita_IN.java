package com.orbita.agendaComercialWS.dto.request;

public class ListarResultadosCita_IN {
	String nro_documento;
	String id_usuario_session;
	int idResultPadre;
	int id_actividad;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public String getId_usuario_session() {
		return id_usuario_session;
	}
	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}
	public int getIdResultPadre() {
		return idResultPadre;
	}
	public void setIdResultPadre(int idResultPadre) {
		this.idResultPadre = idResultPadre;
	}
	public int getId_actividad() {
		return id_actividad;
	}
	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}
	
	
}
