package com.orbita.agendaComercialWS.dto.request;


public class GuardarInfoCliente_IN {
	String nro_documento;
	int id_cliente;
	int tipoInfo;
	String descInfo;
	//ArrayList<ClienteDireccionesGuardar> cliente_direcciones_guardar;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public int getTipoInfo() {
		return tipoInfo;
	}
	public void setTipoInfo(int tipoInfo) {
		this.tipoInfo = tipoInfo;
	}
	public String getDescInfo() {
		return descInfo;
	}
	public void setDescInfo(String descInfo) {
		this.descInfo = descInfo;
	}
	/*public ArrayList<ClienteDireccionesGuardar> getCliente_direcciones_guardar() {
		return cliente_direcciones_guardar;
	}
	public void setCliente_direcciones_guardar(ArrayList<ClienteDireccionesGuardar> cliente_direcciones_guardar) {
		this.cliente_direcciones_guardar = cliente_direcciones_guardar;
	}*/

}
