package com.orbita.agendaComercialWS.dto.request;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CitaResultadoGuardar;

public class GuardarResultadoCitaSC_IN {
	String nro_documento;
	String id_usuario_session;
	int idGestion;
	int id_cita;
	int origen;
	int id_cliente;
	int id_direccion_visita;
	Double latitud;
	Double longitud;
	ArrayList<CitaResultadoGuardar> resultados;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public String getId_usuario_session() {
		return id_usuario_session;
	}
	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}
	public int getIdGestion() {
		return idGestion;
	}
	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}
	public int getId_cita() {
		return id_cita;
	}
	public void setId_cita(int id_cita) {
		this.id_cita = id_cita;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public int getId_direccion_visita() {
		return id_direccion_visita;
	}
	public void setId_direccion_visita(int id_direccion_visita) {
		this.id_direccion_visita = id_direccion_visita;
	}
	public Double getLatitud() {
		return latitud;
	}
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	public Double getLongitud() {
		return longitud;
	}
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	public ArrayList<CitaResultadoGuardar> getResultados() {
		return resultados;
	}
	public void setResultados(ArrayList<CitaResultadoGuardar> resultados) {
		this.resultados = resultados;
	}
	
	
	
}
