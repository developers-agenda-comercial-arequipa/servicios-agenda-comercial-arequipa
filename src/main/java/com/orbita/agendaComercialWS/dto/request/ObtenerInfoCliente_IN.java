package com.orbita.agendaComercialWS.dto.request;

public class ObtenerInfoCliente_IN {
	String nro_documento;
	String id_usuario_session;
	int id_cliente;
	int idGestion;
	String idGrupoInfo;
	

	public ObtenerInfoCliente_IN() {
		super();
	}

	public ObtenerInfoCliente_IN(String nro_documento, int id_cliente) {
		super();
		this.nro_documento = nro_documento;
		this.id_cliente = id_cliente;
	}

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getId_usuario_session() {
		return id_usuario_session;
	}

	public void setId_usuario_session(String id_usuario_session) {
		this.id_usuario_session = id_usuario_session;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public int getIdGestion() {
		return idGestion;
	}

	public void setIdGestion(int idGestion) {
		this.idGestion = idGestion;
	}

	public String getIdGrupoInfo() {
		return idGrupoInfo;
	}

	public void setIdGrupoInfo(String idGrupoInfo) {
		this.idGrupoInfo = idGrupoInfo;
	}

}
