package com.orbita.agendaComercialWS.dto.request;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteDireccionesGuardar;
import com.orbita.agendaComercialWS.model.ClienteInfo;

public class GuardarCliente_IN {
	int origen;
	int origen_cita;
	String nro_documento;
	int idCliente;
	String nombres;
	String apePaterno;
	String apeMaterno;
	int codTipoDoc;
	String nroDocumentoCliente;
	String fechaCita;
	ArrayList<ClienteDireccionesGuardar> cliente_direcciones;
	ArrayList<ClienteInfo> informacion;
	
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public int getOrigen_cita() {
		return origen_cita;
	}
	public void setOrigen_cita(int origen_cita) {
		this.origen_cita = origen_cita;
	}
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApePaterno() {
		return apePaterno;
	}
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}
	public String getApeMaterno() {
		return apeMaterno;
	}
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}
	public int getCodTipoDoc() {
		return codTipoDoc;
	}
	public void setCodTipoDoc(int codTipoDoc) {
		this.codTipoDoc = codTipoDoc;
	}
	public String getNroDocumentoCliente() {
		return nroDocumentoCliente;
	}
	public void setNroDocumentoCliente(String nroDocumentoCliente) {
		this.nroDocumentoCliente = nroDocumentoCliente;
	}
	public String getFechaCita() {
		return fechaCita;
	}
	public void setFechaCita(String fechaCita) {
		this.fechaCita = fechaCita;
	}
	public ArrayList<ClienteDireccionesGuardar> getCliente_direcciones() {
		return cliente_direcciones;
	}
	public void setCliente_direcciones(ArrayList<ClienteDireccionesGuardar> cliente_direcciones) {
		this.cliente_direcciones = cliente_direcciones;
	}
	public ArrayList<ClienteInfo> getInformacion() {
		return informacion;
	}
	public void setInformacion(ArrayList<ClienteInfo> informacion) {
		this.informacion = informacion;
	}
	
}
