package com.orbita.agendaComercialWS.dto.request;

public class ListarMiRadar_IN {
	String nro_documento;
	String fecha;
	double latActual;
	double lonActual;
	
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public double getLatActual() {
		return latActual;
	}
	public void setLatActual(double latActual) {
		this.latActual = latActual;
	}
	public double getLonActual() {
		return lonActual;
	}
	public void setLonActual(double lonActual) {
		this.lonActual = lonActual;
	}

}
