package com.orbita.agendaComercialWS.dto.request;

public class ListarBases_IN {

    int codActividad;

    public ListarBases_IN() {
    }

    public ListarBases_IN(int codActividad) {
        this.codActividad = codActividad;
    }

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }
}
