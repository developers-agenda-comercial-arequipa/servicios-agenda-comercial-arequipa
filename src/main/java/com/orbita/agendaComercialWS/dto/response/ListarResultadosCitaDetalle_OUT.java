package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ResultadoCitaDetalle;

public class ListarResultadosCitaDetalle_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<ResultadoCitaDetalle> resultado_cita_detalle;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<ResultadoCitaDetalle> getResultado_cita_detalle() {
		return resultado_cita_detalle;
	}

	public void setResultado_cita_detalle(ArrayList<ResultadoCitaDetalle> resultado_cita_detalle) {
		this.resultado_cita_detalle = resultado_cita_detalle;
	}

}
