package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CantidadCitaCalendario;

public class ListarCantidadCitasCalendario_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CantidadCitaCalendario> cantidad_citas_calendario;
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public ArrayList<CantidadCitaCalendario> getCantidad_citas_calendario() {
		return cantidad_citas_calendario;
	}
	public void setCantidad_citas_calendario(ArrayList<CantidadCitaCalendario> cantidad_citas_calendario) {
		this.cantidad_citas_calendario = cantidad_citas_calendario;
	}

}
