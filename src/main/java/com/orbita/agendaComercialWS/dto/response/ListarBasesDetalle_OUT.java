package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.BasesDetalle;

public class ListarBasesDetalle_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<BasesDetalle> bases_detalle;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<BasesDetalle> getBases_detalle() {
		return bases_detalle;
	}

	public void setBases_detalle(ArrayList<BasesDetalle> bases_detalle) {
		this.bases_detalle = bases_detalle;
	}

	
}
