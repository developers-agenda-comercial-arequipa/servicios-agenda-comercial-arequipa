package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CarteraDetalle;

public class ListarCarteraDetalle_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CarteraDetalle> cartera_detalle;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<CarteraDetalle> getCartera_detalle() {
		return cartera_detalle;
	}

	public void setCartera_detalle(ArrayList<CarteraDetalle> cartera_detalle) {
		this.cartera_detalle = cartera_detalle;
	}

}
