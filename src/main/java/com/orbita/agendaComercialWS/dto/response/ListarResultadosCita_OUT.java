package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ReaccionCita;

public class ListarResultadosCita_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<ReaccionCita> resultado_cita;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<ReaccionCita> getResultado_cita() {
		return resultado_cita;
	}

	public void setResultado_cita(ArrayList<ReaccionCita> resultado_cita) {
		this.resultado_cita = resultado_cita;
	}

}
