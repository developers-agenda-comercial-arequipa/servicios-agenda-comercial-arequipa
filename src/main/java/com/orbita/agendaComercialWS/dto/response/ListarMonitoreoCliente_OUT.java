package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.MonitoreoMapa;

public class ListarMonitoreoCliente_OUT extends ResponseAgenda_OUT{

	ArrayList<MonitoreoMapa> monitoreoCliente;

	public ArrayList<MonitoreoMapa> getMonitoreoCliente() {
		return monitoreoCliente;
	}

	public void setMonitoreoCliente(ArrayList<MonitoreoMapa> monitoreoCliente) {
		this.monitoreoCliente = monitoreoCliente;
	}
	
}
