package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CitaProgramadaDia;

public class ListarCitasPorDia_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CitaProgramadaDia> citas_programadas_dia;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<CitaProgramadaDia> getCitas_programadas_dia() {
		return citas_programadas_dia;
	}

	public void setCitas_programadas_dia(ArrayList<CitaProgramadaDia> citas_programadas_dia) {
		this.citas_programadas_dia = citas_programadas_dia;
	}

}
