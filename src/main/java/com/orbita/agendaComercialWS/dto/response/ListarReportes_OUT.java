package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.ListaReportes;

import java.util.ArrayList;

public class ListarReportes_OUT extends ResponseAgenda_OUT {

    private ArrayList<ListaReportes> reportes;

    public ArrayList<ListaReportes> getReportes() {
        return reportes;
    }

    public void setReportes(ArrayList<ListaReportes> reportes) {
        this.reportes = reportes;
    }

}
