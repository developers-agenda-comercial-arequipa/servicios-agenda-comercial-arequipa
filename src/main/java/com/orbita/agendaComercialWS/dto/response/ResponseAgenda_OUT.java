package com.orbita.agendaComercialWS.dto.response;

public class ResponseAgenda_OUT {

	private String codigo_validacion;
	private String mensaje;
	
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
