package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteInfo;

public class ListarClienteInfo_OUT extends ResponseAgenda_OUT{

	private ArrayList<ClienteInfo> informacion;

	public ArrayList<ClienteInfo> getInformacion() {
		return informacion;
	}

	public void setInformacion(ArrayList<ClienteInfo> informacion) {
		this.informacion = informacion;
	}
	
}
