package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteDirecciones;

public class ListarDireccionesCliente_OUT {
	
	String codigo_validacion;
	String mensaje;
	ArrayList<ClienteDirecciones> cliente_direcciones;

	
	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<ClienteDirecciones> getCliente_direcciones() {
		return cliente_direcciones;
	}

	public void setCliente_direcciones(ArrayList<ClienteDirecciones> cliente_direcciones) {
		this.cliente_direcciones = cliente_direcciones;
	}

}
