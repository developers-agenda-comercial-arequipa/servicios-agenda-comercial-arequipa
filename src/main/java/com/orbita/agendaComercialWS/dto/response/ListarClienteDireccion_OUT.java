package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteDireccion;

/**
 * @author Jorge
 *
 */
public class ListarClienteDireccion_OUT extends ResponseAgenda_OUT{

	private ArrayList<ClienteDireccion> direcciones;

	public ArrayList<ClienteDireccion> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(ArrayList<ClienteDireccion> direcciones) {
		this.direcciones = direcciones;
	}

}
