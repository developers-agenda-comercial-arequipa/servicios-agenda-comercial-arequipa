package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ReporteAsignados;

public class ReporteAsignados_OUT extends ResponseAgenda_OUT {

	private int totalAsignados;
	private ArrayList<ReporteAsignados> reporteAsignados;
	
	public ReporteAsignados_OUT() {
		super();
	}

	public int getTotalAsignados() {
		return totalAsignados;
	}

	public void setTotalAsignados(int totalAsignados) {
		this.totalAsignados = totalAsignados;
	}

	public ArrayList<ReporteAsignados> getReporteAsignados() {
		return reporteAsignados;
	}

	public void setReporteAsignados(ArrayList<ReporteAsignados> reporteAsignados) {
		this.reporteAsignados = reporteAsignados;
	}
	
}
