package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CitaProgramadaMes;

public class ListarCitasPorMes_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CitaProgramadaMes> citas_programadas_mes;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<CitaProgramadaMes> getCitas_programadas_mes() {
		return citas_programadas_mes;
	}

	public void setCitas_programadas_mes(ArrayList<CitaProgramadaMes> citas_programadas_mes) {
		this.citas_programadas_mes = citas_programadas_mes;
	}

}
