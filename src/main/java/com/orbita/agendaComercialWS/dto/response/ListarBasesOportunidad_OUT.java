package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.BasesOportunidad;

public class ListarBasesOportunidad_OUT {
	
	String codigo_validacion;
	String mensaje;
	ArrayList<BasesOportunidad> basesOportunidad;
	
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public ArrayList<BasesOportunidad> getBasesOportunidad() {
		return basesOportunidad;
	}
	public void setBasesOportunidad(ArrayList<BasesOportunidad> basesOportunidad) {
		this.basesOportunidad = basesOportunidad;
	}
	
}
