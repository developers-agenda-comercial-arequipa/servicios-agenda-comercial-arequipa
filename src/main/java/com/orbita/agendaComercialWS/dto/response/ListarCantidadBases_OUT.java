package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.Actividad;
import com.orbita.agendaComercialWS.model.DistritoCartera;

public class ListarCantidadBases_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<DistritoCartera> distritos_cartera;
	ArrayList<Actividad> actividades;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public ArrayList<DistritoCartera> getDistritos_cartera() {
		return distritos_cartera;
	}

	public void setDistritos_cartera(ArrayList<DistritoCartera> distritos_cartera) {
		this.distritos_cartera = distritos_cartera;
	}

	public ArrayList<Actividad> getActividades() {
		return actividades;
	}

	public void setActividades(ArrayList<Actividad> actividades) {
		this.actividades = actividades;
	}


}
