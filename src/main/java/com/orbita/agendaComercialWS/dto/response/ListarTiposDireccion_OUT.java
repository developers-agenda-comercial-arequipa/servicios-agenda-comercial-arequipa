package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.TipoDireccion;

public class ListarTiposDireccion_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<TipoDireccion> tipos_direccion;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<TipoDireccion> getTipos_direccion() {
		return tipos_direccion;
	}

	public void setTipos_direccion(ArrayList<TipoDireccion> tipos_direccion) {
		this.tipos_direccion = tipos_direccion;
	}

}
