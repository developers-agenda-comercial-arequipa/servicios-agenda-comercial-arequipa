package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.Reporte;

import java.util.ArrayList;

public class ObtenerReporte_OUT extends ResponseAgenda_OUT{

    private ArrayList<Reporte> reporte;

    public ArrayList<Reporte> getReporte() {
        return reporte;
    }

    public void setReporte(ArrayList<Reporte> reporte) {
        this.reporte = reporte;
    }
}
