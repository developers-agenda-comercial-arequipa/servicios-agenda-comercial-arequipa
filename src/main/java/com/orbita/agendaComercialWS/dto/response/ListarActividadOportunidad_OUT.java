package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.Actividad;

import java.util.ArrayList;

public class ListarActividadOportunidad_OUT extends ResponseAgenda_OUT {

    private ArrayList<Actividad> actividades;

    public ArrayList<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(ArrayList<Actividad> actividades) {
        this.actividades = actividades;
    }
}
