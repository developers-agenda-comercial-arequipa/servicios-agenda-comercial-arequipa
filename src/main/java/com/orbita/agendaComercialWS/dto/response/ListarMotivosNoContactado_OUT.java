package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.MotivoNoContactado;

public class ListarMotivosNoContactado_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<MotivoNoContactado> motivos_no_contactados;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<MotivoNoContactado> getMotivos_no_contactados() {
		return motivos_no_contactados;
	}

	public void setMotivos_no_contactados(ArrayList<MotivoNoContactado> motivos_no_contactados) {
		this.motivos_no_contactados = motivos_no_contactados;
	}

}
