package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteActividad;
import com.orbita.agendaComercialWS.model.ClienteDirecciones;
import com.orbita.agendaComercialWS.model.ClienteInfo;

public class ObtenerInfoCliente_OUT {
	String codigo_validacion;
	String mensaje;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	int id_tipo_documento;
	String tipo_documento;
	String nro_documento;
	ArrayList<ClienteInfo> informacion;
	ArrayList<ClienteDirecciones> cliente_direcciones;
	ArrayList<ClienteActividad> actividades;
	
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public int getId_tipo_documento() {
		return id_tipo_documento;
	}
	public void setId_tipo_documento(int id_tipo_documento) {
		this.id_tipo_documento = id_tipo_documento;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getNro_documento() {
		return nro_documento;
	}
	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}
	public ArrayList<ClienteInfo> getInformacion() {
		return informacion;
	}
	public void setInformacion(ArrayList<ClienteInfo> informacion) {
		this.informacion = informacion;
	}
	public ArrayList<ClienteDirecciones> getCliente_direcciones() {
		return cliente_direcciones;
	}
	public void setCliente_direcciones(ArrayList<ClienteDirecciones> cliente_direcciones) {
		this.cliente_direcciones = cliente_direcciones;
	}
	public ArrayList<ClienteActividad> getActividades() {
		return actividades;
	}
	public void setActividades(ArrayList<ClienteActividad> actividades) {
		this.actividades = actividades;
	}

}
