package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.Base;

import java.util.ArrayList;

public class ListarBasesOUT {

    String codigo_validacion;
    String mensaje;
    ArrayList<Base> bases;

    public String getCodigo_validacion() {
        return codigo_validacion;
    }

    public void setCodigo_validacion(String codigo_validacion) {
        this.codigo_validacion = codigo_validacion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public ArrayList<Base> getBases() {
        return bases;
    }

    public void setBases(ArrayList<Base> bases) {
        this.bases = bases;
    }
}
