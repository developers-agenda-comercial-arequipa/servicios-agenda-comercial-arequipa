package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CitaFoto;

public class ListarCitaFotos_OUT extends ResponseAgenda_OUT{

	private ArrayList<CitaFoto> citaFotos;

	public ArrayList<CitaFoto> getCitaFotos() {
		return citaFotos;
	}

	public void setCitaFotos(ArrayList<CitaFoto> citaFotos) {
		this.citaFotos = citaFotos;
	}
	
	
}
