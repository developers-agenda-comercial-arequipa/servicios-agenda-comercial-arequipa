package com.orbita.agendaComercialWS.dto.response;

public class ObtenerDetalleCita_OUT {
	String codigo_validacion;
	String mensaje;
	int id_cita;
	int id_cliente;
	String nombres;
	String apellido_paterno;
	String apellido_materno;
	String base;
	int id_color_base;
	String fecha;
	String direccion;
	String referencia_direccion;
	String distrito;
	String latitud;
	String longitud;
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getId_cita() {
		return id_cita;
	}
	public void setId_cita(int id_cita) {
		this.id_cita = id_cita;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public int getId_color_base() {
		return id_color_base;
	}
	public void setId_color_base(int id_color_base) {
		this.id_color_base = id_color_base;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia_direccion() {
		return referencia_direccion;
	}
	public void setReferencia_direccion(String referencia_direccion) {
		this.referencia_direccion = referencia_direccion;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	
}