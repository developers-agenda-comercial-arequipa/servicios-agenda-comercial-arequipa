package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.DistritoCartera;

public class ListarCantidadCartera_OUT {
	String codigo_validacion;
	String mensaje;
	int id_hoja_ruta;
	ArrayList<DistritoCartera> distritos_cartera;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getId_hoja_ruta() {
		return id_hoja_ruta;
	}

	public void setId_hoja_ruta(int id_hoja_ruta) {
		this.id_hoja_ruta = id_hoja_ruta;
	}

	public ArrayList<DistritoCartera> getDistritos_cartera() {
		return distritos_cartera;
	}

	public void setDistritos_cartera(ArrayList<DistritoCartera> distritos_cartera) {
		this.distritos_cartera = distritos_cartera;
	}

}
