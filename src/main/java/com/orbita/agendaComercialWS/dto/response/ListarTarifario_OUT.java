package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.Tarifario;

import java.util.ArrayList;
import java.util.Date;

public class ListarTarifario_OUT extends ResponseAgenda_OUT {

    Date fecha;
    ArrayList<Tarifario> tarifario;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ArrayList<Tarifario> getTarifario() {
        return tarifario;
    }

    public void setTarifario(ArrayList<Tarifario> tarifario) {
        this.tarifario = tarifario;
    }
}
