package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.BasesCliente;

public class ListarBasesCliente_OUT {
	
	String codigo_validacion;
	String mensaje;
	ArrayList<BasesCliente> basesCliente;
	
	public String getCodigo_validacion() {
		return codigo_validacion;
	}
	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public ArrayList<BasesCliente> getBasesCliente() {
		return basesCliente;
	}
	public void setBasesCliente(ArrayList<BasesCliente> basesCliente) {
		this.basesCliente = basesCliente;
	}
	
}
