package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ClienteTelef;

/**
 * @author Jorge
 *
 */
public class ListarClienteTelef_OUT extends ResponseAgenda_OUT{

	private ArrayList<ClienteTelef> telefonos;

	public ArrayList<ClienteTelef> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(ArrayList<ClienteTelef> telefonos) {
		this.telefonos = telefonos;
	}

}
