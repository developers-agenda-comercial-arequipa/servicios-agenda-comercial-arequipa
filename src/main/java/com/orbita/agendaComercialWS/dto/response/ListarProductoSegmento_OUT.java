package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.ComboString;

import java.util.ArrayList;

public class ListarProductoSegmento_OUT extends ResponseAgenda_OUT {

    ArrayList<ComboString> productos;
    ArrayList<ComboString> segmentos;

    public ArrayList<ComboString> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<ComboString> productos) {
        this.productos = productos;
    }

    public ArrayList<ComboString> getSegmentos() {
        return segmentos;
    }

    public void setSegmentos(ArrayList<ComboString> segmentos) {
        this.segmentos = segmentos;
    }
}
