package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ActividadBase;

public class ListarActividad_OUT extends ResponseAgenda_OUT {

	private ArrayList<ActividadBase> actividades;
	
	public ListarActividad_OUT() {
		super();
	}

	public ArrayList<ActividadBase> getActividades() {
		return actividades;
	}

	public void setActividades(ArrayList<ActividadBase> actividades) {
		this.actividades = actividades;
	}
	
}
