package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.ClienteResultado;

import java.util.ArrayList;

public class ListarClienteResultado_OUT extends ResponseAgenda_OUT {

    private ArrayList<ClienteResultado> resultados;

    public ArrayList<ClienteResultado> getResultados() {
        return resultados;
    }

    public void setResultados(ArrayList<ClienteResultado> resultados) {
        this.resultados = resultados;
    }
}
