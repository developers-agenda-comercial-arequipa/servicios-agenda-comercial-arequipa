package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CarteraMapa;

public class ListarCarteraMapa_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CarteraMapa> cartera_mapa;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<CarteraMapa> getCartera_mapa() {
		return cartera_mapa;
	}

	public void setCartera_mapa(ArrayList<CarteraMapa> cartera_mapa) {
		this.cartera_mapa = cartera_mapa;
	}

}
