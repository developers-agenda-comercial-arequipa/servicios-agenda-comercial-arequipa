package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.ReporteGestion;

public class ReporteGestion_OUT extends ResponseAgenda_OUT {

	private int cantPendientes;
	private int cantEfectivos;
	private int cantNoEfectivos;
	private int pctEfectividad;
	private ArrayList<ReporteGestion> gestionClientes;
	
	public ReporteGestion_OUT() {
		super();
	}
	
	public int getCantPendientes() {
		return cantPendientes;
	}
	public void setCantPendientes(int cantPendientes) {
		this.cantPendientes = cantPendientes;
	}
	public int getCantEfectivos() {
		return cantEfectivos;
	}
	public void setCantEfectivos(int cantEfectivos) {
		this.cantEfectivos = cantEfectivos;
	}
	public int getCantNoEfectivos() {
		return cantNoEfectivos;
	}
	public void setCantNoEfectivos(int cantNoEfectivos) {
		this.cantNoEfectivos = cantNoEfectivos;
	}
	public int getPctEfectividad() {
		return pctEfectividad;
	}
	public void setPctEfectividad(int pctEfectividad) {
		this.pctEfectividad = pctEfectividad;
	}
	public ArrayList<ReporteGestion> getGestionClientes() {
		return gestionClientes;
	}
	public void setGestionClientes(ArrayList<ReporteGestion> gestionClientes) {
		this.gestionClientes = gestionClientes;
	}
	
}
