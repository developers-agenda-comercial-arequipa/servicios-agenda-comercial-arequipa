package com.orbita.agendaComercialWS.dto.response;

import java.util.ArrayList;

import com.orbita.agendaComercialWS.model.CarteraNoGeo;

public class ListarCarteraNoGeo_OUT {
	String codigo_validacion;
	String mensaje;
	ArrayList<CarteraNoGeo> cartera_no_geo;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ArrayList<CarteraNoGeo> getCartera_no_geo() {
		return cartera_no_geo;
	}

	public void setCartera_no_geo(ArrayList<CarteraNoGeo> cartera_no_geo) {
		this.cartera_no_geo = cartera_no_geo;
	}

}
