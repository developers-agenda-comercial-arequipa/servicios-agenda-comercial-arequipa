package com.orbita.agendaComercialWS.dto.response;

import com.orbita.agendaComercialWS.model.UltimoPago;

import java.util.ArrayList;

public class ListarUltimosPagos_OUT extends ResponseAgenda_OUT {

    private ArrayList<UltimoPago> pagos;

    public ArrayList<UltimoPago> getPagos() {
        return pagos;
    }

    public void setPagos(ArrayList<UltimoPago> pagos) {
        this.pagos = pagos;
    }
}
