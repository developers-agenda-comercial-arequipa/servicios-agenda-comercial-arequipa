package com.orbita.agendaComercialWS.dto.response;

public class ObtenerContadorCitas_OUT {
	String codigo_validacion;
	String mensaje;
	int cantidad_citas;

	public String getCodigo_validacion() {
		return codigo_validacion;
	}

	public void setCodigo_validacion(String codigo_validacion) {
		this.codigo_validacion = codigo_validacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getCantidad_citas() {
		return cantidad_citas;
	}

	public void setCantidad_citas(int cantidad_citas) {
		this.cantidad_citas = cantidad_citas;
	}

}
