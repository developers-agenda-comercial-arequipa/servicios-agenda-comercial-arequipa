package com.orbita.agendaComercialWS.dto.response;

public class Version_OUT {

	private String version;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
}
