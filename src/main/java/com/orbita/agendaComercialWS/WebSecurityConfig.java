package com.orbita.agendaComercialWS;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@Configuration
//@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Override	
	protected void configure(HttpSecurity http) throws Exception{
			http.authorizeRequests() 
		    .antMatchers("/WS*")
		    .anonymous();
			http.authorizeRequests()
			.antMatchers("/login*")
			.permitAll().and()
			.headers()
			.httpStrictTransportSecurity()
			.includeSubDomains(false)
			.maxAgeInSeconds(31536000);
	}


}