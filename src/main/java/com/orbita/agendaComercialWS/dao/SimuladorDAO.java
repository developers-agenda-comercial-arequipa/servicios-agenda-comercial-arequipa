package com.orbita.agendaComercialWS.dao;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarTarifario_IN;
import com.orbita.agendaComercialWS.dto.response.ListarProductoSegmento_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarTarifario_OUT;

import java.sql.SQLException;

public interface SimuladorDAO {

    ListarProductoSegmento_OUT listarProductoSegmento_OUT(General_IN general_IN) throws SQLException;

    ListarTarifario_OUT listarTarifario_OUT(ListarTarifario_IN listarTarifario_IN) throws SQLException;
}
