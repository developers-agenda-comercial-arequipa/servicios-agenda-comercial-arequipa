package com.orbita.agendaComercialWS.dao;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.ValidarUsuarioAC_IN;
import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;

public interface LoginDAO {
	public ValidarUsuarioAC_OUT validarUsuarioAC_OUT(ValidarUsuarioAC_IN validarUsuarioAC_IN) throws SQLException;
}