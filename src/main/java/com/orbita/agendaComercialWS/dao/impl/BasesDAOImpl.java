package com.orbita.agendaComercialWS.dao.impl;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.BasesDAO;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

@Repository
public class BasesDAOImpl implements BasesDAO {

	private static final Logger logger = LoggerFactory.getLogger(BasesDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public ListarDepartamentos_OUT listarDepartamentos_OUT(ListarDepartamentos_IN listarDepartamentos_IN)
			throws SQLException {

		ListarDepartamentos_OUT listarDepartamentos_OUT = new ListarDepartamentos_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_DEPARTAMENTOS);){
			
			ArrayList<Departamento> listaDepartamento = new ArrayList<Departamento>();

			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(3);){
				while (rs.next()) {
					Departamento d = new Departamento();
					d.setId_departamento(rs.getInt("COD_PARAMETRO"));
					d.setDescripcion_departamento(rs.getString("TXT_DESCRIPCION"));

					listaDepartamento.add(d);
				}
			}

			listarDepartamentos_OUT.setCodigo_validacion(cs.getString(1));
			listarDepartamentos_OUT.setMensaje(cs.getString(2));
			listarDepartamentos_OUT.setDepartamentos(listaDepartamento);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarDepartamentos_OUT;
	}

	@Override
	public ListarProvincias_OUT listarProvincias_OUT(ListarProvincias_IN listarProvincias_IN) throws SQLException {

		ListarProvincias_OUT listarProvincias_OUT = new ListarProvincias_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_PROVINCIAS);){

			ArrayList<Provincia> listaProvincia = new ArrayList<Provincia>();

			cs.setString(1, listarProvincias_IN.getNro_documento());
			cs.setString(2, listarProvincias_IN.getId_usuario_session());
			cs.setInt(3, listarProvincias_IN.getId_departamento());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6);){
				while (rs.next()) {
					Provincia p = new Provincia();
					p.setId_provincia(rs.getInt("COD_PARAMETRO"));
					p.setDescripcion_provincia(rs.getString("TXT_DESCRIPCION"));

					listaProvincia.add(p);
				}
			}

			listarProvincias_OUT.setCodigo_validacion(cs.getString(4));
			listarProvincias_OUT.setMensaje(cs.getString(5));
			listarProvincias_OUT.setProvincias(listaProvincia);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarProvincias_OUT;
	}

	@Override
	public ListarDistritos_OUT listarDistritos_OUT(ListarDistritos_IN listarDistritos_IN) throws SQLException {

		ListarDistritos_OUT listarDistritos_OUT = new ListarDistritos_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_DISTRITOS);){
			
			ArrayList<Distrito> listaDistrito = new ArrayList<Distrito>();
			
			cs.setString(1, listarDistritos_IN.getNro_documento());
			cs.setString(2, listarDistritos_IN.getId_usuario_session());
			cs.setInt(3, listarDistritos_IN.getId_provincia());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6);){
				while (rs.next()) {
					Distrito d = new Distrito();
					d.setId_distrito(rs.getInt("COD_PARAMETRO"));
					d.setDescripcion_distrito(rs.getString("TXT_DESCRIPCION"));

					listaDistrito.add(d);
				}
			}
			
			listarDistritos_OUT.setCodigo_validacion(cs.getString(4));
			listarDistritos_OUT.setMensaje(cs.getString(5));
			listarDistritos_OUT.setDistritos(listaDistrito);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarDistritos_OUT;
	}

	@Override
	public ListarTiposDireccion_OUT listarTiposDireccion_OUT(ListarTiposDireccion_IN listarTiposDireccion_IN)
			throws SQLException {
		
		ListarTiposDireccion_OUT listarTiposDireccion_OUT = new ListarTiposDireccion_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_TIPOS_DIRECCION);){
		
			ArrayList<TipoDireccion> listaTipoDireccion = new ArrayList<TipoDireccion>();

			cs.setString(1, listarTiposDireccion_IN.getNro_documento());
			cs.setString(2, listarTiposDireccion_IN.getId_usuario_session());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5);){
				while (rs.next()) {
					TipoDireccion td = new TipoDireccion();
					td.setId_tipo_direccion(rs.getInt("COD_PARAMETRO"));
					td.setDescripcion_tipo_direccion(rs.getString("TXT_DESCRIPCION"));

					listaTipoDireccion.add(td);
				}
			}

			listarTiposDireccion_OUT.setCodigo_validacion(cs.getString(3));
			listarTiposDireccion_OUT.setMensaje(cs.getString(4));
			listarTiposDireccion_OUT.setTipos_direccion(listaTipoDireccion);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarTiposDireccion_OUT;
	}
	
	@Override
	public GuardarClienteGeoC_OUT guardarClienteGeoC_OUT(GuardarClienteGeoC_IN guardarClienteGeoC_IN) throws SQLException {

		GuardarClienteGeoC_OUT guardarClienteGeoC_OUT = new GuardarClienteGeoC_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_CLIENTE_GEO_C);){	

			cs.setString(1, guardarClienteGeoC_IN.getNro_documento());
			cs.setString(2, guardarClienteGeoC_IN.getId_usuario_session());
			cs.setInt(3, guardarClienteGeoC_IN.getId_direccion());
			cs.setInt(4, guardarClienteGeoC_IN.getId_cliente());
			cs.setString(5, guardarClienteGeoC_IN.getLatitud());
			cs.setString(6, guardarClienteGeoC_IN.getLongitud());
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.execute();

			guardarClienteGeoC_OUT.setCodigo_validacion(cs.getString(7));
			guardarClienteGeoC_OUT.setMensaje(cs.getString(8));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return guardarClienteGeoC_OUT;
	}

	/**
	 * Lista la cantidad de clientes por distrito
	 */
	@Override
	public ListarCantidadBases_OUT listarCantidadBases_OUT(ListarCantidadCartera_IN listarCantidadCartera_IN)
			throws SQLException {

		ListarCantidadBases_OUT listarCantidadBases_OUT = new ListarCantidadBases_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CANTIDAD_BASES);){

			ArrayList<DistritoCartera> listaDistritoCartera = new ArrayList<DistritoCartera>();
			
			cs.setString(1, listarCantidadCartera_IN.getNro_documento());
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(4);){
				while (rs.next()) {
					DistritoCartera dc = new DistritoCartera();
					dc.setIdUbigeo(rs.getString("UBIGEO"));
					dc.setDistrito(rs.getString("DISTRITO"));
					dc.setCantidad(rs.getInt("CANTIDAD"));

					listaDistritoCartera.add(dc);
				}

			}

			listarCantidadBases_OUT.setCodigo_validacion(cs.getString(2));
			listarCantidadBases_OUT.setMensaje(cs.getString(3));
			listarCantidadBases_OUT.setDistritos_cartera(listaDistritoCartera);
			listarCantidadBases_OUT.setActividades(ListarActividadCartera_OUT(listarCantidadCartera_IN.getNro_documento()));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCantidadBases_OUT;
	}

	/**
	 * Lista los clientes de la cartera
	 */
	@Override
	public ListarBasesDetalle_OUT listarBasesDetalle_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException {

		ListarBasesDetalle_OUT listarBasesDetalle_OUT = new ListarBasesDetalle_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_BASES_DETALLE);){

			ArrayList<BasesDetalle> listaBasesDetalle = new ArrayList<>();
			Array listaInfo;
			Array listaUbig;
			
			// Filtro Info
			StructDescriptor structDescAct = StructDescriptor.createDescriptor("TYPE_INFO", connObj);
			ArrayDescriptor arrDescAct = ArrayDescriptor.createDescriptor("TYPE_INFO_TAB", connObj); 
			
			if (listarCarteraDetalle_IN.getInfo() != null) {
				Object[] array = new Object[listarCarteraDetalle_IN.getInfo().size()];
	            Object[] objeto = new Object[2];

	            for (int i = 0; i< listarCarteraDetalle_IN.getInfo().size(); i++) {
	                objeto[0] = listarCarteraDetalle_IN.getInfo().get(i).getIdTipoInfo();
	                objeto[1] = listarCarteraDetalle_IN.getInfo().get(i).getDescInfo();
	                
	                STRUCT oracleObjeto = new STRUCT(structDescAct, connObj, objeto);
	                array[i] = oracleObjeto;
	            }
				listaInfo = new ARRAY(arrDescAct, connObj, array);
			} else {
				listaInfo = new ARRAY(arrDescAct, connObj, new Object[0]);
			}
			
			// Filtro Ubigeo
			StructDescriptor structDescUbig = StructDescriptor.createDescriptor("TYPE_UBIGEO", connObj);
			ArrayDescriptor arrDescUbig = ArrayDescriptor.createDescriptor("TYPE_UBIGEO_TAB", connObj);
			
			if (listarCarteraDetalle_IN.getUbigeos() != null) {
				Object[] array2 = new Object[listarCarteraDetalle_IN.getUbigeos().size()];
	            Object[] objeto2 = new Object[1];

	            for (int i = 0; i< listarCarteraDetalle_IN.getUbigeos().size(); i++) {
	            	objeto2[0] = listarCarteraDetalle_IN.getUbigeos().get(i).getIdUbigeo();
	                
	                STRUCT oracleObjeto2 = new STRUCT(structDescUbig, connObj, objeto2);
	                array2[i] = oracleObjeto2;
	            }
				listaUbig = new ARRAY(arrDescUbig, connObj, array2);
			} else {
				
				listaUbig = new ARRAY(arrDescUbig, connObj, new Object[0]);
			}
			
			cs.setString(1, listarCarteraDetalle_IN.getNro_documento());
			cs.setInt(2, listarCarteraDetalle_IN.getOrigen());
			cs.setInt(3, listarCarteraDetalle_IN.getIdActividad());
			cs.setInt(4, listarCarteraDetalle_IN.getIdBase());
			cs.setString(5, listarCarteraDetalle_IN.getNroDocCliente());
			cs.setArray(6, listaInfo);
			cs.setArray(7, listaUbig);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.registerOutParameter(10, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(10)){
				while (rs.next()) {
					BasesDetalle cd = new BasesDetalle();
					cd.setId_cliente(rs.getInt("COD_CLIENTE"));
					cd.setNombres(rs.getString("TXT_NOMBRES"));
					cd.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cd.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cd.setIdGestion(rs.getInt("COD_GESTION"));
					cd.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					cd.setActividad(rs.getString("ACTIVIDAD"));
					cd.setIdColorActividad(rs.getInt("COD_PARAM_COLOR"));
					cd.setId_direccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cd.setDireccion(rs.getString("DIRECCION"));
					cd.setReferencia_direccion(rs.getString("TXT_REFERENCIA"));
					cd.setDistrito(rs.getString("DISTRITO"));
					cd.setLatitud(rs.getString("DBL_LATITUD"));
					cd.setLongitud(rs.getString("DBL_LONGITUD"));
					cd.setId_tipo_documento(rs.getInt("COD_PARAM_TIPO_DOCUMENTO"));
					cd.setTipo_documento(rs.getString("PARAM_TIPO_DOCUMENTO"));
					cd.setNumero_documento(rs.getString("TXT_NRO_DOCUMENTO"));

					if (listarCarteraDetalle_IN.getIdActividad() == 2 || listarCarteraDetalle_IN.getIdActividad() == 0){
						cd.setPago(rs.getInt("PAGO"));
					}
					
					//ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarCarteraDetalle_IN.getNro_documento(), cd.getId_cliente());
					//cd.setInformacion(agendaDAOImpl.listarClienteInfo(obtenerInfoCliente_IN, connObj));

					listaBasesDetalle.add(cd);
				}
			}
			
			listarBasesDetalle_OUT.setCodigo_validacion(cs.getString(8));
			listarBasesDetalle_OUT.setMensaje(cs.getString(9));
			listarBasesDetalle_OUT.setBases_detalle(listaBasesDetalle);
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarBasesDetalle_OUT;
	}

	@Override
	public ListarBasesDetalle_OUT listarOportunidadXFiltro_OUT(ListarBasesDetalle_IN listarCarteraDetalle_IN)
			throws SQLException {

		ListarBasesDetalle_OUT listarBasesDetalle_OUT = new ListarBasesDetalle_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_OPORTUNIDAD_XFILTRO)){

			ArrayList<BasesDetalle> listaBasesDetalle = new ArrayList<BasesDetalle>();
			Array listaInfo;
			Array listaUbig;

			// Filtro Info
			StructDescriptor structDescAct = StructDescriptor.createDescriptor("TYPE_INFO", connObj);
			ArrayDescriptor arrDescAct = ArrayDescriptor.createDescriptor("TYPE_INFO_TAB", connObj);

			if (listarCarteraDetalle_IN.getInfo() != null) {
				Object[] array = new Object[listarCarteraDetalle_IN.getInfo().size()];
				Object[] objeto = new Object[2];

				for (int i = 0; i< listarCarteraDetalle_IN.getInfo().size(); i++) {
					objeto[0] = listarCarteraDetalle_IN.getInfo().get(i).getIdTipoInfo();
					objeto[1] = listarCarteraDetalle_IN.getInfo().get(i).getDescInfo();

					STRUCT oracleObjeto = new STRUCT(structDescAct, connObj, objeto);
					array[i] = oracleObjeto;
				}
				listaInfo = new ARRAY(arrDescAct, connObj, array);
			} else {
				listaInfo = new ARRAY(arrDescAct, connObj, new Object[0]);
			}

			// Filtro Ubigeo
			StructDescriptor structDescUbig = StructDescriptor.createDescriptor("TYPE_UBIGEO", connObj);
			ArrayDescriptor arrDescUbig = ArrayDescriptor.createDescriptor("TYPE_UBIGEO_TAB", connObj);

			if (listarCarteraDetalle_IN.getUbigeos() != null) {
				Object[] array2 = new Object[listarCarteraDetalle_IN.getUbigeos().size()];
				Object[] objeto2 = new Object[1];

				for (int i = 0; i< listarCarteraDetalle_IN.getUbigeos().size(); i++) {
					objeto2[0] = listarCarteraDetalle_IN.getUbigeos().get(i).getIdUbigeo();

					STRUCT oracleObjeto2 = new STRUCT(structDescUbig, connObj, objeto2);
					array2[i] = oracleObjeto2;
				}
				listaUbig = new ARRAY(arrDescUbig, connObj, array2);
			} else {

				listaUbig = new ARRAY(arrDescUbig, connObj, new Object[0]);
			}

			cs.setString(1, listarCarteraDetalle_IN.getNro_documento());
			cs.setDouble(2, listarCarteraDetalle_IN.getLatActual());
			cs.setDouble(3, listarCarteraDetalle_IN.getLonActual());
			cs.setInt(4, listarCarteraDetalle_IN.getIdActividad());
			cs.setInt(5, listarCarteraDetalle_IN.getIdBase());
			cs.setString(6, listarCarteraDetalle_IN.getNroDocCliente());
			cs.setArray(7, listaInfo);
			cs.setArray(8, listaUbig);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.registerOutParameter(11, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(11)){
				while (rs.next()) {
					BasesDetalle cd = new BasesDetalle();
					cd.setId_cliente(rs.getInt("COD_CLIENTE"));
					cd.setNombres(rs.getString("TXT_NOMBRES"));
					cd.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cd.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cd.setIdGestion(rs.getInt("COD_GESTION"));
					cd.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					cd.setIdBase(rs.getInt("COD_BASE"));
					cd.setActividad(rs.getString("ACTIVIDAD"));
					cd.setIdColorActividad(rs.getInt("COD_PARAM_COLOR"));
					cd.setId_direccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cd.setDireccion(rs.getString("DIRECCION"));
					cd.setReferencia_direccion(rs.getString("TXT_REFERENCIA"));
					cd.setDistrito(rs.getString("DISTRITO"));
					cd.setLatitud(rs.getString("DBL_LATITUD"));
					cd.setLongitud(rs.getString("DBL_LONGITUD"));
					cd.setId_tipo_documento(rs.getInt("COD_PARAM_TIPO_DOCUMENTO"));
					cd.setTipo_documento(rs.getString("PARAM_TIPO_DOCUMENTO"));
					cd.setNumero_documento(rs.getString("TXT_NRO_DOCUMENTO"));

					//ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarCarteraDetalle_IN.getNro_documento(), cd.getId_cliente());
					//cd.setInformacion(agendaDAOImpl.listarClienteInfo(obtenerInfoCliente_IN, connObj));

					listaBasesDetalle.add(cd);
				}
			}

			listarBasesDetalle_OUT.setCodigo_validacion(cs.getString(9));
			listarBasesDetalle_OUT.setMensaje(cs.getString(10));
			listarBasesDetalle_OUT.setBases_detalle(listaBasesDetalle);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarBasesDetalle_OUT;
	}

	/**
	 * @param nroDocumento: del usuario
	 * @return Lista de actividades que existe en la cartera del usuario
	 * @throws SQLException
	 */
	public ArrayList<Actividad> ListarActividadCartera_OUT(String nroDocumento) throws SQLException{

		ArrayList<Actividad> actividades = new ArrayList<Actividad>();	
		int tipo = 1;
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
		     CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_ACTIVIDAD_FILTRO)){

			cs.setInt(1, tipo);
			cs.setString(2, nroDocumento);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5)){
				while (rs.next()) {
					Actividad a = new Actividad();
					a.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					a.setActividad(rs.getString("ACTIVIDAD"));
					a.setIdColor(rs.getInt("COD_PARAM_COLOR"));
					actividades.add(a);	
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return actividades;

	}

	@Override
	public ListarUltimosPagos_OUT listarUltimosPagos_OUT(ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException {

		ListarUltimosPagos_OUT listarUltimosPagos_OUT = new ListarUltimosPagos_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_ACT_PAGOS)){

			ArrayList<UltimoPago> pagos = new ArrayList<>();

			cs.setString(1, listarInfoCliente_IN.getNro_documento());
			cs.setInt(2, listarInfoCliente_IN.getId_cliente());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5)){
				while (rs.next()) {
					UltimoPago up = new UltimoPago();
					up.setFecha(rs.getString("FECHA"));
					up.setEstado(rs.getString("ESTADO"));

					pagos.add(up);
				}

			}

			listarUltimosPagos_OUT.setCodigo_validacion(cs.getString(3));
			listarUltimosPagos_OUT.setMensaje(cs.getString(4));
			listarUltimosPagos_OUT.setPagos(pagos);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarUltimosPagos_OUT;
	}
}
