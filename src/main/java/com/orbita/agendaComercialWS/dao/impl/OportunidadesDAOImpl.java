package com.orbita.agendaComercialWS.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMiRadar_IN;
import com.orbita.agendaComercialWS.dto.response.ListarActividadOportunidad_OUT;
import com.orbita.agendaComercialWS.model.Actividad;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.OportunidadesDAO;
import com.orbita.agendaComercialWS.dto.request.ListarCarteraInactivo_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerInfoCliente_IN;
import com.orbita.agendaComercialWS.dto.response.ListarBasesOportunidad_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarCarteraInactivo_OUT;
import com.orbita.agendaComercialWS.model.BasesOportunidad;
import com.orbita.agendaComercialWS.model.CarteraMapa;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class OportunidadesDAOImpl implements OportunidadesDAO {

	private static final Logger logger = LoggerFactory.getLogger(OportunidadesDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public ListarCarteraInactivo_OUT listarCarteraInactivo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException {

		ListarCarteraInactivo_OUT listarCarteraInactivo_OUT = new ListarCarteraInactivo_OUT();
		int tipo = 2;

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_MI_RADAR)){

			ArrayList<CarteraMapa> listaCarteraMapa = new ArrayList<CarteraMapa>();

			cs.setInt(1, tipo);
			cs.setString(2, listarMiRadar_IN.getNro_documento());
			cs.setString(3, listarMiRadar_IN.getFecha());
			cs.setDouble(4, listarMiRadar_IN.getLatActual());
			cs.setDouble(5, listarMiRadar_IN.getLonActual());
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(8)){
				while (rs.next()) {
					
					CarteraMapa cm = new CarteraMapa();
					cm.setId_cliente(rs.getInt("COD_CLIENTE"));
					cm.setNombres(rs.getString("TXT_NOMBRES"));
					cm.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cm.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cm.setId_tipo_documento(rs.getInt("COD_PARAM_TIPO_DOCUMENTO"));
					cm.setTipo_documento(rs.getString("TIPO_DOCUMENTO"));
					cm.setNro_documento(rs.getString("TXT_NRO_DOCUMENTO"));
					cm.setId_base(rs.getInt("COD_BASE"));
					cm.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					cm.setActividad(rs.getString("ACTIVIDAD"));
					cm.setIdColorActividad(rs.getInt("COD_PARAM_COLOR"));
					cm.setIdClienteDireccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cm.setDireccion(rs.getString("DIRECCION"));
					cm.setReferencia(rs.getString("TXT_REFERENCIA"));
					cm.setDistrito(rs.getString("DISTRITO"));
					cm.setLatitud(rs.getString("DBL_LATITUD"));
					cm.setLongitud(rs.getString("DBL_LONGITUD"));
					cm.setTipoDireccion(rs.getInt("TIPO_DIRECCION"));
					
					listaCarteraMapa.add(cm);
				}
			}

			listarCarteraInactivo_OUT.setCodigo_validacion(cs.getString(6));
			listarCarteraInactivo_OUT.setMensaje(cs.getString(7));
			listarCarteraInactivo_OUT.setCartera_mapa(listaCarteraMapa);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCarteraInactivo_OUT;
	}

	@Override
	public ListarBasesOportunidad_OUT listarBasesOportunidad_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {

		ListarBasesOportunidad_OUT listarBasesOportunidad_OUT = new ListarBasesOportunidad_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_BASES_OPORTUNIDAD);){

			ArrayList<BasesOportunidad> listaBasesOportunidad = new ArrayList<BasesOportunidad>();
			
			cs.setString(1, obtenerInfoCliente_IN.getNro_documento());
			cs.setInt(2, obtenerInfoCliente_IN.getId_cliente());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5);){
				while (rs.next()) {

					BasesOportunidad bc = new BasesOportunidad();
					bc.setIdBase(rs.getInt("COD_BASE"));
					bc.setBase(rs.getString("TXT_DESCRIPCION"));
					bc.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					listaBasesOportunidad.add(bc);
			
				}
			}
			
			listarBasesOportunidad_OUT.setCodigo_validacion(cs.getString(3));
			listarBasesOportunidad_OUT.setMensaje(cs.getString(4));
			listarBasesOportunidad_OUT.setBasesOportunidad(listaBasesOportunidad);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarBasesOportunidad_OUT;
	}

	@Override
	public ListarActividadOportunidad_OUT listarActividadOportunidad_OUT(General_IN general_IN) throws SQLException {

		ListarActividadOportunidad_OUT listarActividadOportunidad_OUT = new ListarActividadOportunidad_OUT();

		int tipo = 2;
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_ACTIVIDAD_FILTRO)){

			ArrayList<Actividad> actividades = new ArrayList<>();

			cs.setInt(1, tipo);
			cs.setString(2, general_IN.getNro_documento());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5)){
				while (rs.next()) {
					Actividad a = new Actividad();
					a.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					a.setActividad(rs.getString("ACTIVIDAD"));
					a.setIdColor(rs.getInt("COD_PARAM_COLOR"));
					actividades.add(a);
				}
			}

			listarActividadOportunidad_OUT.setCodigo_validacion(cs.getString(3));
			listarActividadOportunidad_OUT.setMensaje(cs.getString(4));
			listarActividadOportunidad_OUT.setActividades(actividades);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarActividadOportunidad_OUT;

	}

}
