package com.orbita.agendaComercialWS.dao.impl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.ReportesDAO;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class ReportesDAOImpl implements ReportesDAO {

	private static final Logger logger = LoggerFactory.getLogger(ReportesDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * Obtiene el reporte de clientes asignados.
	 */
	@Override
	public ReporteAsignados_OUT reporteAsignados_OUT(ReporteAsignados_IN reporteAsignados_IN)
			throws SQLException {

		ReporteAsignados_OUT reporteAsignados_OUT = new ReporteAsignados_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_REPORTE_ASIGNADOS);){

			ArrayList<ReporteAsignados> reporteAsignados = new ArrayList<ReporteAsignados>();
			
			cs.setInt(1, reporteAsignados_IN.getNroDocumento());
			cs.setInt(2, reporteAsignados_IN.getIdActividad());
			cs.setInt(3, reporteAsignados_IN.getIdBase());
			cs.setInt(4, reporteAsignados_IN.getAnio());
			cs.setInt(5, reporteAsignados_IN.getMes());
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.registerOutParameter(9, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(9);){
				while (rs.next()) {

					ReporteAsignados ra = new ReporteAsignados();
					ra.setIdGestionEstado(rs.getInt("COD_GESTION_ESTADO"));
					ra.setCantXEstado(rs.getInt("CANT_X_ESTADO"));
					ra.setPctXEstado(rs.getInt("PCT_X_ESTADO"));
					reporteAsignados.add(ra);
				}
			}
			
			reporteAsignados_OUT.setCodigo_validacion(cs.getString(6));
			reporteAsignados_OUT.setMensaje(cs.getString(7));
			reporteAsignados_OUT.setTotalAsignados(cs.getInt(8));
			reporteAsignados_OUT.setReporteAsignados(reporteAsignados);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return reporteAsignados_OUT;
	}

	/**
	 * Obtiene el reporte de la efectividad en la gesti�n de clientes
	 */
	@Override
	public ReporteGestion_OUT reporteGestion_OUT(ReporteGestion_IN reporteGestion_IN) throws SQLException {

		ReporteGestion_OUT reporteGestion_OUT = new ReporteGestion_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_REPORTE_GESTION);){
			
			ArrayList<ReporteGestion> reporteGestion = new ArrayList<ReporteGestion>();

			cs.setInt(1, reporteGestion_IN.getNroDocumento());
			cs.setString(2, reporteGestion_IN.getFecInicio());
			cs.setString(3, reporteGestion_IN.getFecFin());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.registerOutParameter(8, OracleTypes.INTEGER);
			cs.registerOutParameter(9, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(9);){
				while (rs.next()) {

					ReporteGestion rg = new ReporteGestion();
					rg.setCliente(rs.getString("CLIENTE"));
					rg.setActividad(rs.getString("ACTIVIDAD"));
					rg.setIdColor(rs.getInt("COD_PARAM_COLOR"));
					rg.setEfectivo(rs.getInt("IND_EFECTIVIDAD"));
					reporteGestion.add(rg);
				}
			}
			
			reporteGestion_OUT.setCodigo_validacion(cs.getString(4));
			reporteGestion_OUT.setMensaje(cs.getString(5));
			reporteGestion_OUT.setCantEfectivos(cs.getInt(6));
			reporteGestion_OUT.setCantNoEfectivos(cs.getInt(7));
			reporteGestion_OUT.setPctEfectividad(cs.getInt(8));
			reporteGestion_OUT.setGestionClientes(reporteGestion);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return reporteGestion_OUT;
	}

	/**
	 * Lista las actividades y sus bases
	 */
	@Override
	public ListarActividad_OUT listarActividad_OUT(ListarBasesDetalle_IN listarBasesDetalle_IN) throws SQLException {
		
		ListarActividad_OUT listarActividad_OUT = new ListarActividad_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_ACTIVIDAD);){
			
			ArrayList<ActividadBase> actividades = new ArrayList<ActividadBase>();

			cs.setString(1, listarBasesDetalle_IN.getNro_documento());
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();

			
			try (ResultSet rs = (ResultSet) cs.getObject(4);){
				while (rs.next()) {
					ActividadBase act = new ActividadBase();
					act.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					act.setActividad(rs.getString("ACTIVIDAD"));

					ListarBases_IN listarBases_in = new ListarBases_IN(act.getIdActividad());
					act.setBases(listarBase_OUT(listarBases_in).getBases());
					actividades.add(act);
					
				}
			}

			listarActividad_OUT.setCodigo_validacion(cs.getString(2));
			listarActividad_OUT.setMensaje(cs.getString(3));
			listarActividad_OUT.setActividades(actividades);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarActividad_OUT;
	}

	/**
	 * @return Lista bases seg�n actividad
	 */
	@Override
	public ListarBasesOUT listarBase_OUT(ListarBases_IN listarBases_in) throws SQLException{

		ListarBasesOUT listarBasesOUT = new ListarBasesOUT();
		ArrayList<Base> bases = new ArrayList<>();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_BASE);){

			cs.setInt(1, listarBases_in.getCodActividad());
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(4);){
				while (rs.next()) {
					Base b = new Base();
					b.setIdBase(rs.getInt("COD_BASE"));
					b.setBase(rs.getString("BASE"));
					bases.add(b);
				}
			}

			listarBasesOUT.setCodigo_validacion(cs.getString(2));
			listarBasesOUT.setMensaje(cs.getString(3));
			listarBasesOUT.setBases(bases);


		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} 
		
		return listarBasesOUT;
	}

	@Override
	public ListarReportes_OUT listarReportes_OUT(General_IN general_IN) throws SQLException {

		ListarReportes_OUT listarReportes_OUT = new ListarReportes_OUT();
		int tipo = 1;

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_REPORTES)){

			ArrayList<ListaReportes> reportes = new ArrayList<>();

			cs.setInt(1, tipo);
			cs.setString(2, general_IN.getNro_documento());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5)){
				while (rs.next()) {

					ListaReportes rep = new ListaReportes();
					rep.setNombre(rs.getString("REPORTE"));
					rep.setFecha(rs.getDate("FECHA"));
					reportes.add(rep);
				}
			}

			listarReportes_OUT.setCodigo_validacion(cs.getString(3));
			listarReportes_OUT.setMensaje(cs.getString(4));
			listarReportes_OUT.setReportes(reportes);


		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarReportes_OUT;
	}

	@Override
	public ObtenerReporte_OUT obtenerReporte_OUT(ObtenerReporte_IN obtenerReporte_IN) throws SQLException {

		ObtenerReporte_OUT obtenerReporte_OUT;
		ObtenerReporte_OUT obtenerReporteDetalle_OUT;

		obtenerReporte_OUT = (ObtenerReporte_OUT) ListarReportes(2, obtenerReporte_IN);

		return obtenerReporte_OUT;
	}


	public Object ListarReportes(int tipo, Object obj) throws SQLException{
			String servicio = null;

		if (tipo == 2){
			servicio = WebServicesConstants.SP_OBTENER_REPORTE_CAB;
		} else if (tipo == 3){
			servicio = WebServicesConstants.SP_OBTENER_REPORTE_DETA;
		}

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(servicio)){

			if (tipo == 1){
				ListarReportes_OUT listarReportes_OUT = new ListarReportes_OUT();
				General_IN general_IN = (General_IN) obj;
				ArrayList<ListaReportes> reportes = new ArrayList<>();

				cs.setInt(1, tipo);
				cs.setString(2, general_IN.getNro_documento());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5)){
					while (rs.next()) {

						ListaReportes rep = new ListaReportes();
						rep.setNombre(rs.getString("REPORTE"));
						rep.setFecha(rs.getDate("FECHA"));
						reportes.add(rep);
					}
				}

				listarReportes_OUT.setCodigo_validacion(cs.getString(3));
				listarReportes_OUT.setMensaje(cs.getString(4));
				listarReportes_OUT.setReportes(reportes);

				return listarReportes_OUT;

			} else if (tipo == 2) {
				ObtenerReporte_OUT obtenerReporte_OUT = new ObtenerReporte_OUT();
				ObtenerReporte_OUT obtenerReporteDetalle_OUT = new ObtenerReporte_OUT();
				ObtenerReporte_IN obtenerReporte_IN = (ObtenerReporte_IN) obj;
				ArrayList<Reporte> reporte = new ArrayList<>();

				cs.setInt(1, tipo);
				cs.setString(2, obtenerReporte_IN.getNro_documento());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.setString(6, obtenerReporte_IN.getNomReporte());
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5)){
					while (rs.next()) {

						Reporte rep = new Reporte();
						rep.setTipo(rs.getString("TIPO"));
						rep.setDescripcion(rs.getString("DESCRIPCION"));
						rep.setGrupo(rs.getString("GROUP_INFO"));
						obtenerReporte_IN.setGrupo(rep.getGrupo());
						obtenerReporteDetalle_OUT = (ObtenerReporte_OUT)ListarReportes(3, obtenerReporte_IN);
						rep.setDetalle(obtenerReporteDetalle_OUT.getReporte());
						reporte.add(rep);
					}
				}

				obtenerReporte_OUT.setCodigo_validacion(cs.getString(3));
				obtenerReporte_OUT.setMensaje(cs.getString(4));
				obtenerReporte_OUT.setReporte(reporte);

				return obtenerReporte_OUT;

			} else if (tipo == 3) {
				ObtenerReporte_OUT obtenerReporte_OUT = new ObtenerReporte_OUT();
				ObtenerReporte_IN obtenerReporte_IN = (ObtenerReporte_IN) obj;
				ArrayList<Reporte> reporte = new ArrayList<>();

				cs.setInt(1, tipo);
				cs.setString(2, obtenerReporte_IN.getNro_documento());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.setString(6, obtenerReporte_IN.getNomReporte());
				cs.setString(7, obtenerReporte_IN.getGrupo());
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5)){
					while (rs.next()) {

						Reporte rep = new Reporte();
						rep.setTipo(rs.getString("TIPO"));
						rep.setDescripcion(rs.getString("DESCRIPCION"));
						rep.setGrupo(rs.getString("GROUP_INFO"));
						reporte.add(rep);
					}
				}
				obtenerReporte_OUT.setReporte(reporte);

				return obtenerReporte_OUT;
			}


		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return null;
	}

}
