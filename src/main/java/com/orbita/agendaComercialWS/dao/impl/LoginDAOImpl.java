package com.orbita.agendaComercialWS.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.LoginDAO;
import com.orbita.agendaComercialWS.dto.request.ValidarUsuarioAC_IN;
import com.orbita.agendaComercialWS.dto.response.ValidarUsuarioAC_OUT;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class LoginDAOImpl implements LoginDAO {

	private static final Logger logger = LoggerFactory.getLogger(LoginDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public ValidarUsuarioAC_OUT validarUsuarioAC_OUT(ValidarUsuarioAC_IN validarUsuarioAC_IN)
			throws SQLException {
		
		ValidarUsuarioAC_OUT validarUsuarioAC_OUT = new ValidarUsuarioAC_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_VALIDAR_USUARIO_AC);) {
				
			cs.setString(1, validarUsuarioAC_IN.getUsuario());
			cs.setString(2, validarUsuarioAC_IN.getContrasena());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			
			cs.execute();

			validarUsuarioAC_OUT.setNombres(cs.getString(3));
			validarUsuarioAC_OUT.setApellido_paterno(cs.getString(4));
			validarUsuarioAC_OUT.setApellido_materno(cs.getString(5));
			validarUsuarioAC_OUT.setNro_documento(cs.getString(6));
			validarUsuarioAC_OUT.setId_usuario_session(cs.getString(7));
			validarUsuarioAC_OUT.setCodigo_validacion(cs.getString(8));
			validarUsuarioAC_OUT.setMensaje(cs.getString(9));
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return validarUsuarioAC_OUT;
	}
}
