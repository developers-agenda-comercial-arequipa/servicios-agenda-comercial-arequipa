package com.orbita.agendaComercialWS.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Objects;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.AgendaDAO;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

@Repository
public class AgendaDAOImpl implements AgendaDAO {

	private static final Logger logger = LoggerFactory.getLogger(AgendaDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public ListarCantidadCitasCalendario_OUT listarCitasPorMes_OUT(
			ListarCantidadCitasCalendario_IN listarCitasPorMes_IN) throws SQLException {

		ListarCantidadCitasCalendario_OUT listarCitasPorMes_OUT = new ListarCantidadCitasCalendario_OUT();

		try (Connection connObj = Objects.requireNonNull(jdbcTemplate.getDataSource()).getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CANTIDAD_CITAS_CALENDARIO);){

			ArrayList<CantidadCitaCalendario> listaCitaProgramadaMes = new ArrayList<CantidadCitaCalendario>();

			cs.setString(1, listarCitasPorMes_IN.getNro_documento());
			cs.setString(2, listarCitasPorMes_IN.getId_usuario_session());
			cs.setInt(3, listarCitasPorMes_IN.getMes());
			cs.setInt(4, listarCitasPorMes_IN.getAnio());
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(7);){
				while (rs.next()) {
					CantidadCitaCalendario ccc = new CantidadCitaCalendario();
					ccc.setFecha(rs.getString("FEC_CITA"));
					ccc.setCantidad(rs.getInt("INT_CANTIDAD_CITA"));

					ListarIndicadorActividad_IN listarIndicadorActividad_IN = new ListarIndicadorActividad_IN(listarCitasPorMes_IN.getNro_documento(), ccc.getFecha());
				    ccc.setGestionActividad(listarIndicadorActividad_OUT(listarIndicadorActividad_IN, connObj));

					listaCitaProgramadaMes.add(ccc);
				}
			}

			listarCitasPorMes_OUT.setCodigo_validacion(cs.getString(5));
			listarCitasPorMes_OUT.setMensaje(cs.getString(6));
			listarCitasPorMes_OUT.setCantidad_citas_calendario(listaCitaProgramadaMes);


		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		
		return listarCitasPorMes_OUT;
	}

	@Override
	public ListarCitasPorDia_OUT listarCitasPorDia_OUT(ListarCitasPorDia_IN listarCitasPorDia_IN) throws SQLException {

		ListarCitasPorDia_OUT listarCitasPorDia_OUT = new ListarCitasPorDia_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CITAS_POR_DIA);){
			
			ArrayList<CitaProgramadaDia> listaCitaProgramada = new ArrayList<CitaProgramadaDia>();

			cs.setString(1, listarCitasPorDia_IN.getNro_documento());
			cs.setString(2, listarCitasPorDia_IN.getId_usuario_session());
			cs.setString(3, listarCitasPorDia_IN.getFecha());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6)){
				while (rs.next()) {

					CitaProgramadaDia cpd = new CitaProgramadaDia();
					cpd.setId_cita(rs.getInt("COD_CITA"));
					cpd.setId_cliente(rs.getInt("COD_CLIENTE"));
					cpd.setNombres(rs.getString("TXT_NOMBRES"));
					cpd.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cpd.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cpd.setIdGestion(rs.getInt("COD_GESTION"));
					cpd.setFecha(rs.getString("FEC_CITA"));
					cpd.setEstado_cita(rs.getString("PARAM_ESTADO_CITA"));
					cpd.setCantGeo(rs.getString("CANT_GEO"));
					cpd.setEstBloqueo(rs.getInt("EST_BLOQUEO"));
					cpd.setMsjeBloqueo(rs.getString("MSJE_BLOQUEO"));
					cpd.setCantFoto(rs.getInt("FOTO"));
					
					ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarCitasPorDia_IN.getNro_documento(), cpd.getId_cliente());
					cpd.setActividades(listarClienteActividad_OUT(3, obtenerInfoCliente_IN, connObj, cpd.getIdGestion()));
					
					listaCitaProgramada.add(cpd);
				}

			} 
			
			listarCitasPorDia_OUT.setCodigo_validacion(cs.getString(4));
			listarCitasPorDia_OUT.setMensaje(cs.getString(5));
			listarCitasPorDia_OUT.setCitas_programadas_dia(listaCitaProgramada);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return listarCitasPorDia_OUT;
	}

	@Override
	public ListarCitasPorDiaGeo_OUT listarCitasPorDiaGeo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException {

		ListarCitasPorDiaGeo_OUT listarCitasPorDiaGeo_OUT = new ListarCitasPorDiaGeo_OUT();
		int tipo = 1;

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_MI_RADAR);){
			
			ArrayList<CitaProgramadaDiaGeo> listaCitaProgramada = new ArrayList<CitaProgramadaDiaGeo>();

			cs.setInt(1, tipo);
			cs.setString(2, listarMiRadar_IN.getNro_documento());
			cs.setString(3, listarMiRadar_IN.getFecha());
			cs.setDouble(4, listarMiRadar_IN.getLatActual());
			cs.setDouble(5, listarMiRadar_IN.getLonActual());
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(8)) {

				while (rs.next()) {

					CitaProgramadaDiaGeo cpd = new CitaProgramadaDiaGeo();
					cpd.setId_cita(rs.getInt("COD_CITA"));
					cpd.setId_cliente(rs.getInt("COD_CLIENTE"));
					cpd.setNombres(rs.getString("TXT_NOMBRES"));
					cpd.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cpd.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cpd.setId_tipo_documento(rs.getInt("COD_PARAM_TIPO_DOCUMENTO"));
					cpd.setIdTipoDireccion(rs.getInt("TIPO_DIRECCION"));
					cpd.setTipo_documento(rs.getString("TIPO_DOCUMENTO"));
					cpd.setNro_documento(rs.getString("TXT_NRO_DOCUMENTO"));
					cpd.setIdGestion(rs.getInt("COD_GESTION"));
					cpd.setIdBase(rs.getInt("COD_ACTIVIDAD"));
					cpd.setBase(rs.getString("ACTIVIDAD"));
					cpd.setIdColorBase(rs.getInt("COD_PARAM_COLOR"));
					cpd.setFecha(rs.getString("FEC_CITA"));
					cpd.setId_direccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cpd.setDireccion(rs.getString("DIRECCION"));
					cpd.setReferencia_direccion(rs.getString("TXT_REFERENCIA"));
					cpd.setDistrito(rs.getString("DISTRITO"));
					cpd.setLatitud(rs.getString("DBL_LATITUD"));
					cpd.setLongitud(rs.getString("DBL_LONGITUD"));
					cpd.setEstado_cita(rs.getString("PARAM_ESTADO_CITA"));
					
					ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarMiRadar_IN.getNro_documento(), cpd.getId_cliente());
					cpd.setInformacion(listarClienteInfo(2, obtenerInfoCliente_IN, connObj));
					
					listaCitaProgramada.add(cpd);
				}
			}

			listarCitasPorDiaGeo_OUT.setCodigo_validacion(cs.getString(6));
			listarCitasPorDiaGeo_OUT.setMensaje(cs.getString(7));
			listarCitasPorDiaGeo_OUT.setCitas_programadas_dia(listaCitaProgramada);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return listarCitasPorDiaGeo_OUT;
	}

	@Override
	public ObtenerDetalleCita_OUT obtenerDetalleCita_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException {

		ObtenerDetalleCita_OUT obtenerDetalleCita_OUT = new ObtenerDetalleCita_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_OBTENER_DETALLE_CITA);) {

			cs.setString(1, obtenerDetalleCita_IN.getNro_documento());
			cs.setString(2, obtenerDetalleCita_IN.getId_usuario_session());
			cs.setInt(3, obtenerDetalleCita_IN.getId_cita());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.registerOutParameter(7, OracleTypes.INTEGER);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.registerOutParameter(11, OracleTypes.VARCHAR);
			cs.registerOutParameter(12, OracleTypes.INTEGER);
			cs.registerOutParameter(13, OracleTypes.VARCHAR);
			cs.registerOutParameter(14, OracleTypes.VARCHAR);
			cs.registerOutParameter(15, OracleTypes.VARCHAR);
			cs.registerOutParameter(16, OracleTypes.VARCHAR);
			cs.registerOutParameter(17, OracleTypes.VARCHAR);
			cs.registerOutParameter(18, OracleTypes.VARCHAR);

			cs.execute();

			obtenerDetalleCita_OUT.setCodigo_validacion(cs.getString(4));
			obtenerDetalleCita_OUT.setMensaje(cs.getString(5));
			obtenerDetalleCita_OUT.setId_cita(cs.getInt(6));
			obtenerDetalleCita_OUT.setId_cliente(cs.getInt(7));
			obtenerDetalleCita_OUT.setNombres(cs.getString(8));
			obtenerDetalleCita_OUT.setApellido_paterno(cs.getString(9));
			obtenerDetalleCita_OUT.setApellido_materno(cs.getString(10));
			obtenerDetalleCita_OUT.setBase(cs.getString(11));
			obtenerDetalleCita_OUT.setId_color_base(cs.getInt(12));
			obtenerDetalleCita_OUT.setFecha(cs.getString(13));
			obtenerDetalleCita_OUT.setDireccion(cs.getString(14));
			obtenerDetalleCita_OUT.setReferencia_direccion(cs.getString(15));
			obtenerDetalleCita_OUT.setDistrito(cs.getString(16));
			obtenerDetalleCita_OUT.setLatitud(cs.getString(17));
			obtenerDetalleCita_OUT.setLongitud(cs.getString(18));
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return obtenerDetalleCita_OUT;
	}

	@Override
	public ListarMotivosNoContactado_OUT listarMotivosNoContactados_OUT(
			ListarMotivosNoContactado_IN listarMotivosNoContactados_IN) throws SQLException {

		ListarMotivosNoContactado_OUT listarMotivosNoContactados_OUT = new ListarMotivosNoContactado_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_MOTIVOS_NO_CONTACTADOS);){
			
			ArrayList<MotivoNoContactado> listaMotivoNoContactado = new ArrayList<MotivoNoContactado>();

			cs.setString(1, listarMotivosNoContactados_IN.getNro_documento());
			cs.setString(2, listarMotivosNoContactados_IN.getId_usuario_session());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5);){
				while (rs.next()) {
					MotivoNoContactado mnc = new MotivoNoContactado();
					mnc.setId_motivo(rs.getInt("COD_REACCION"));
					mnc.setDescripcion_motivo(rs.getString("TXT_DESCRIPCION"));

					listaMotivoNoContactado.add(mnc);
				}
			}
			
			listarMotivosNoContactados_OUT.setCodigo_validacion(cs.getString(3));
			listarMotivosNoContactados_OUT.setMensaje(cs.getString(4));
			listarMotivosNoContactados_OUT.setMotivos_no_contactados(listaMotivoNoContactado);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} 

		return listarMotivosNoContactados_OUT;
	}

	@Override
	public ListarResultadosCita_OUT listarResultadosCita_OUT(ListarResultadosCita_IN listarResultadosCita_IN)
			throws SQLException {

		ListarResultadosCita_OUT listarResultadosCita_OUT = new ListarResultadosCita_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_REACCIONES);){
			
			ArrayList<ReaccionCita> listaResultadoCita = new ArrayList<ReaccionCita>();

			cs.setString(1, listarResultadosCita_IN.getNro_documento());
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(4);) {
				while (rs.next()) {
					ReaccionCita rc = new ReaccionCita();
					rc.setId_actividad(rs.getInt("COD_ACTIVIDAD"));
					rc.setActividad(rs.getString("ACTIVIDAD"));
					rc.setIdColorActividad(rs.getInt("COLOR_ACTIVIDAD"));
					rc.setId_reaccion(rs.getInt("COD_REACCION"));
					rc.setDescripcion_reaccion(rs.getString("TXT_DESCRIPCION"));
					rc.setNueva_visita(rs.getInt("BLN_NUEVA_VISITA"));
					
					ListarResultadosCitaDetalle_OUT listDetalle = new ListarResultadosCitaDetalle_OUT();
					ListarResultadosCitaDetalle_IN request = new ListarResultadosCitaDetalle_IN(listarResultadosCita_IN.getNro_documento(), listarResultadosCita_IN.getId_usuario_session(), rs.getInt("COD_REACCION"));
					listDetalle = listarResultadosCitaDetalle_OUT(request);
					
					rc.setResultadoDetalle(listDetalle.getResultado_cita_detalle());
					
					listaResultadoCita.add(rc);
				}
			}

			listarResultadosCita_OUT.setCodigo_validacion(cs.getString(2));
			listarResultadosCita_OUT.setMensaje(cs.getString(3));
			listarResultadosCita_OUT.setResultado_cita(listaResultadoCita);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarResultadosCita_OUT;
	}

	@Override
	public ListarResultadosCitaDetalle_OUT listarResultadosCitaDetalle_OUT(
			ListarResultadosCitaDetalle_IN listarResultadosCitaDetalle_IN) throws SQLException {

		ListarResultadosCitaDetalle_OUT listarResultadosCitaDetalle_OUT = new ListarResultadosCitaDetalle_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_REACCIONES_DETALLE);){
			
			ArrayList<ResultadoCitaDetalle> listaResultadoDetalleCita = new ArrayList<ResultadoCitaDetalle>();
			
			cs.setString(1, listarResultadosCitaDetalle_IN.getNro_documento());
			cs.setString(2, listarResultadosCitaDetalle_IN.getId_usuario_session());
			cs.setInt(3, listarResultadosCitaDetalle_IN.getId_resultado_cita());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6);){
			   while (rs.next()) {
				ResultadoCitaDetalle rcd = new ResultadoCitaDetalle();
				rcd.setId_reaccion_detalle(rs.getInt("COD_REACCION"));
				rcd.setDescripcion_reaccion_detalle(rs.getString("TXT_DESCRIPCION"));
				rcd.setNueva_visita(rs.getInt("BLN_NUEVA_VISITA"));
				rcd.setMisti(rs.getInt("BLN_MISTI"));

				listaResultadoDetalleCita.add(rcd);
				}
			}

			listarResultadosCitaDetalle_OUT.setCodigo_validacion(cs.getString(4));
			listarResultadosCitaDetalle_OUT.setMensaje(cs.getString(5));
			listarResultadosCitaDetalle_OUT.setResultado_cita_detalle(listaResultadoDetalleCita);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarResultadosCitaDetalle_OUT;
	}

	@Override
	public GuardarResultadoCitaNC_OUT guardarResultadoCitaNC_OUT(GuardarResultadoCitaNC_IN guardarResultadoCitaNC_IN)
			throws SQLException {

		GuardarResultadoCitaNC_OUT guardarResultadoCitaNC_OUT = new GuardarResultadoCitaNC_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_RESULT_CITA_NC);){
			
			cs.setString(1, guardarResultadoCitaNC_IN.getNro_documento());
			cs.setString(2, guardarResultadoCitaNC_IN.getId_usuario_session());
			cs.setInt(3, guardarResultadoCitaNC_IN.getId_cita());
			cs.setInt(4, guardarResultadoCitaNC_IN.getIdReaccion());
			cs.setString(5, guardarResultadoCitaNC_IN.getComentario());
			cs.setInt(6, guardarResultadoCitaNC_IN.getIdGestion());
			cs.setInt(7, guardarResultadoCitaNC_IN.getOrigen());
			cs.setInt(8, guardarResultadoCitaNC_IN.getId_cliente());
			cs.setInt(9, guardarResultadoCitaNC_IN.getId_direccion_visita());
			cs.setDouble(10, guardarResultadoCitaNC_IN.getLatitud());
			cs.setDouble(11, guardarResultadoCitaNC_IN.getLongitud());
			cs.setInt(12, guardarResultadoCitaNC_IN.getReprogramar());
			cs.setString(13, guardarResultadoCitaNC_IN.getFecNuevaCita());
			cs.registerOutParameter(14, OracleTypes.VARCHAR);
			cs.registerOutParameter(15, OracleTypes.VARCHAR);
			cs.registerOutParameter(16, OracleTypes.INTEGER);
			cs.execute();

			guardarResultadoCitaNC_OUT.setCodigo_validacion(cs.getString(14));
			guardarResultadoCitaNC_OUT.setMensaje(cs.getString(15));
			guardarResultadoCitaNC_OUT.setId_cita(cs.getInt(16));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return guardarResultadoCitaNC_OUT;
	}

	@Override
	public GuardarResultadoCitaSC_OUT guardarResultadoCitaSC_OUT(GuardarResultadoCitaSC_IN guardarResultadoCitaSC_IN)
			throws SQLException {

		GuardarResultadoCitaSC_OUT guardarResultadoCitaSC_OUT = new GuardarResultadoCitaSC_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_RESULT_CITA_SC);){
			
			StructDescriptor structDesc = StructDescriptor.createDescriptor("TBL_CITA_RESULTADO_TYPE", connObj);
			ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("TBL_CITA_RESULTADO_TAB", connObj);
			
			Object[] array = new Object[guardarResultadoCitaSC_IN.getResultados().size()];
            Object[] objeto = new Object[6];

            for (int i = 0; i< guardarResultadoCitaSC_IN.getResultados().size(); i++) {
                objeto[0] = guardarResultadoCitaSC_IN.getResultados().get(i).getIdGestion();
                objeto[1] = guardarResultadoCitaSC_IN.getResultados().get(i).getIdReaccion();
                objeto[2] = guardarResultadoCitaSC_IN.getResultados().get(i).getIdReaccionDetalle();
                objeto[3] = guardarResultadoCitaSC_IN.getResultados().get(i).getComentario();
                objeto[4] = guardarResultadoCitaSC_IN.getResultados().get(i).getReprogramar();
                objeto[5] = guardarResultadoCitaSC_IN.getResultados().get(i).getFecNuevaCita();
                
                STRUCT oracleObjeto = new STRUCT(structDesc, connObj, objeto);

                array[i] = oracleObjeto;
            }
			
            Array lista = new ARRAY(arrDesc, connObj, array);
            
			cs.setString(1, guardarResultadoCitaSC_IN.getNro_documento());
			cs.setString(2, guardarResultadoCitaSC_IN.getId_usuario_session());
			cs.setInt(3, guardarResultadoCitaSC_IN.getId_cita());
			cs.setInt(4, guardarResultadoCitaSC_IN.getIdGestion());
			cs.setInt(5, guardarResultadoCitaSC_IN.getOrigen());
			cs.setInt(6, guardarResultadoCitaSC_IN.getId_cliente());
			cs.setInt(7, guardarResultadoCitaSC_IN.getId_direccion_visita());
			cs.setDouble(8, guardarResultadoCitaSC_IN.getLatitud());
			cs.setDouble(9, guardarResultadoCitaSC_IN.getLongitud());
			cs.setObject(10, lista);
			cs.registerOutParameter(11, OracleTypes.VARCHAR);
			cs.registerOutParameter(12, OracleTypes.VARCHAR);
			cs.registerOutParameter(13, OracleTypes.INTEGER);
			cs.execute();

			guardarResultadoCitaSC_OUT.setCodigo_validacion(cs.getString(11));
			guardarResultadoCitaSC_OUT.setMensaje(cs.getString(12));
			guardarResultadoCitaSC_OUT.setId_cita(cs.getInt(13));


		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return guardarResultadoCitaSC_OUT;
	}

	@Override
	public GuardarFotoCita_OUT guardarFotoCita_OUT(GuardarFotoCita_IN guardarFotoCita_IN) throws SQLException {

		GuardarFotoCita_OUT guardarFotoCita_OUT = new GuardarFotoCita_OUT();		

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_FOTO_CITA);){

			InputStream is = null;
			
			cs.setString(1, guardarFotoCita_IN.getNro_documento());
			cs.setString(2, guardarFotoCita_IN.getId_usuario_session());
			cs.setInt(3, guardarFotoCita_IN.getId_cita());
			cs.setString(4, guardarFotoCita_IN.getNombre_foto());

			String cadenaCorrecta = guardarFotoCita_IN.getArchivo_foto().replace("\n", "");
			byte[] decodedImg = Base64.getDecoder().decode(cadenaCorrecta.getBytes(StandardCharsets.UTF_8));
			is = new ByteArrayInputStream(decodedImg);

			cs.setBinaryStream(5, is, decodedImg.length);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.execute();

			guardarFotoCita_OUT.setCodigo_validacion(cs.getString(6));
			guardarFotoCita_OUT.setMensaje(cs.getString(7));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} 

		return guardarFotoCita_OUT;
	}

	@Override
	public ListarCitasPorMes_OUT listarCitasPorMes_OUT(ListarCitasPorMes_IN listarCitasPorMes_IN) throws SQLException {

		ListarCitasPorMes_OUT listarCitasPorMes_OUT = new ListarCitasPorMes_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CITAS_POR_MES);){

			ArrayList<CitaProgramadaMes> listaCitaProgramadaMes = new ArrayList<CitaProgramadaMes>();

			cs.setString(1, listarCitasPorMes_IN.getNro_documento());
			cs.setString(2, listarCitasPorMes_IN.getId_usuario_session());
			cs.setString(3, listarCitasPorMes_IN.getFecha());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6);){
				while (rs.next()) {

					CitaProgramadaMes cpm = new CitaProgramadaMes();
					cpm.setId_cita(rs.getInt("COD_CITA"));
					cpm.setId_cliente(rs.getInt("COD_CLIENTE"));
					cpm.setNombres(rs.getString("TXT_NOMBRES"));
					cpm.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cpm.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cpm.setFecha(rs.getString("FEC_CITA"));
					cpm.setEstado_cita(rs.getString("PARAM_ESTADO_CITA"));
					cpm.setEstBloqueo(rs.getInt("EST_BLOQUEO"));
					cpm.setMsjeBloqueo(rs.getString("MSJE_BLOQUEO"));
					cpm.setCantFoto(rs.getInt("FOTO"));
					
					ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarCitasPorMes_IN.getNro_documento(), cpm.getId_cliente());
					cpm.setActividades(listarClienteActividad_OUT(1, obtenerInfoCliente_IN, connObj, 0));
					
					listaCitaProgramadaMes.add(cpm);
				}

			}
			
			listarCitasPorMes_OUT.setCodigo_validacion(cs.getString(4));
			listarCitasPorMes_OUT.setMensaje(cs.getString(5));
			listarCitasPorMes_OUT.setCitas_programadas_mes(listaCitaProgramadaMes);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCitasPorMes_OUT;
	}

	@Override
	public ListarCarteraMapa_OUT listarCarteraMapa_OUT(ListarMiRadar_IN listarMiRadar_IN) throws SQLException {
			
		ListarCarteraMapa_OUT listarCarteraMapa_OUT = new ListarCarteraMapa_OUT();
		int tipo = 3;
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_MI_RADAR)){
			
			ArrayList<CarteraMapa> listaCarteraMapa = new ArrayList<CarteraMapa>();

			cs.setInt(1, tipo);
			cs.setString(2, listarMiRadar_IN.getNro_documento());
			cs.setString(3, listarMiRadar_IN.getFecha());
			cs.setDouble(4, listarMiRadar_IN.getLatActual());
			cs.setDouble(5, listarMiRadar_IN.getLonActual());
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.CURSOR);
			cs.execute();
			
			try (ResultSet rs = (ResultSet) cs.getObject(8);){
				while (rs.next()) {

					CarteraMapa cm = new CarteraMapa();
					cm.setId_cliente(rs.getInt("COD_CLIENTE"));
					cm.setNombres(rs.getString("TXT_NOMBRES"));
					cm.setApellido_paterno(rs.getString("TXT_APELLIDO_PATERNO"));
					cm.setApellido_materno(rs.getString("TXT_APELLIDO_MATERNO"));
					cm.setId_tipo_documento(rs.getInt("COD_PARAM_TIPO_DOCUMENTO"));
					cm.setTipo_documento(rs.getString("TIPO_DOCUMENTO"));
					cm.setNro_documento(rs.getString("TXT_NRO_DOCUMENTO"));
					cm.setIdGestion(rs.getInt("COD_GESTION"));
					cm.setGestionEstado(rs.getInt("COD_GESTION_ESTADO"));
					cm.setId_base(rs.getInt("COD_ACTIVIDAD"));
					cm.setActividad(rs.getString("ACTIVIDAD"));
					cm.setIdColorActividad(rs.getInt("COD_PARAM_COLOR"));
					cm.setIdClienteDireccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cm.setDireccion(rs.getString("DIRECCION"));
					cm.setReferencia(rs.getString("TXT_REFERENCIA"));
					cm.setDistrito(rs.getString("DISTRITO"));
					cm.setLatitud(rs.getString("DBL_LATITUD"));
					cm.setLongitud(rs.getString("DBL_LONGITUD"));
					cm.setTipoDireccion(rs.getInt("TIPO_DIRECCION"));
					cm.setPago(rs.getInt("PAGO"));

					ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(listarMiRadar_IN.getNro_documento(), cm.getId_cliente());
					cm.setInformacion(listarClienteInfo(2, obtenerInfoCliente_IN, connObj));

					listaCarteraMapa.add(cm);
				}
			}
			
			listarCarteraMapa_OUT.setCodigo_validacion(cs.getString(6));
			listarCarteraMapa_OUT.setMensaje(cs.getString(7));
			listarCarteraMapa_OUT.setCartera_mapa(listaCarteraMapa);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCarteraMapa_OUT;
	}

	@Override
	public ObtenerInfoCliente_OUT obtenerInfoCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {

		ObtenerInfoCliente_OUT obtenerInfoCliente_OUT = new ObtenerInfoCliente_OUT();
		int tipo = 3;

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
		     CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_OBTENER_INFO_CLIENTE)){
			
			ArrayList<ClienteDirecciones> listaClienteDirecciones = new ArrayList<ClienteDirecciones>();
			
			cs.setString(1, obtenerInfoCliente_IN.getNro_documento());
			cs.setString(2, obtenerInfoCliente_IN.getId_usuario_session());
			cs.setInt(3, obtenerInfoCliente_IN.getId_cliente());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.INTEGER);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.registerOutParameter(11, OracleTypes.VARCHAR);
			cs.registerOutParameter(12, OracleTypes.CURSOR);
			cs.execute();

			try(ResultSet rs = (ResultSet) cs.getObject(12);){
				while (rs.next()) {

					ClienteDirecciones cd = new ClienteDirecciones();
					cd.setId_direccion(rs.getInt("COD_CLIENTE_DIRECCION"));
					cd.setDireccion(rs.getString("TXT_DESCRIPCION"));
					cd.setReferencia(rs.getString("TXT_REFERENCIA"));
					cd.setId_tipo_direccion(rs.getInt("COD_PARAM_TIPO_DIRECCION"));
					cd.setTipo_direccion(rs.getString("PARAM_TIPO_DIRECCION"));
					cd.setId_departamento(rs.getString("COD_PARAM_DEPARTAMENTO"));
					cd.setDepartamento(rs.getString("PARAM_DEPARTAMENTO"));
					cd.setId_provincia(rs.getString("COD_PARAM_PROVINCIA"));
					cd.setProvincia(rs.getString("PARAM_PROVINCIA"));
					cd.setId_distrito(rs.getString("COD_PARAM_DISTRITO"));
					cd.setDistrito(rs.getString("PARAM_DISTRITO"));
					cd.setLatitud(rs.getString("DBL_LATITUD"));
					cd.setLongitud(rs.getString("DBL_LONGITUD"));
					listaClienteDirecciones.add(cd);
				}
			}
			
			obtenerInfoCliente_OUT.setCodigo_validacion(cs.getString(4));
			obtenerInfoCliente_OUT.setMensaje(cs.getString(5));
			obtenerInfoCliente_OUT.setNombres(cs.getString(6));
			obtenerInfoCliente_OUT.setApellido_paterno(cs.getString(7));
			obtenerInfoCliente_OUT.setApellido_materno(cs.getString(8));
			obtenerInfoCliente_OUT.setId_tipo_documento(cs.getInt(9));
			obtenerInfoCliente_OUT.setTipo_documento(cs.getString(10));
			obtenerInfoCliente_OUT.setNro_documento(cs.getString(11));
			
			obtenerInfoCliente_OUT.setInformacion(listarClienteInfo_OUT(1, obtenerInfoCliente_IN).getInformacion());
			obtenerInfoCliente_OUT.setCliente_direcciones(listaClienteDirecciones);
			if (obtenerInfoCliente_IN.getIdGestion() == 0) {
				tipo = 4;
			}
			obtenerInfoCliente_OUT.setActividades(listarClienteActividad_OUT(tipo, obtenerInfoCliente_IN, connObj, obtenerInfoCliente_IN.getIdGestion()));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return obtenerInfoCliente_OUT;
	}

	@Override
	public GuardarInfoCliente_OUT guardarInfoCliente_OUT(GuardarInfoCliente_IN guardarInfoCliente_IN)
			throws SQLException {

		GuardarInfoCliente_OUT guardarInfoCliente_OUT = new GuardarInfoCliente_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
		     CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_INFO_CLIENTE);){
			
			cs.setString(1, guardarInfoCliente_IN.getNro_documento());
			cs.setInt(2, guardarInfoCliente_IN.getId_cliente());
			cs.setInt(3, guardarInfoCliente_IN.getTipoInfo());
			cs.setString(4, guardarInfoCliente_IN.getDescInfo());
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();

			guardarInfoCliente_OUT.setCodigo_validacion(cs.getString(5));
			guardarInfoCliente_OUT.setMensaje(cs.getString(6));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return guardarInfoCliente_OUT;
	}
	
	@Override
	public GuardarInfoClienteDireccion_OUT guardarInfoClienteDireccion_OUT(
			GuardarInfoClienteDireccion_IN guardarInfoClienteDireccion_IN) throws SQLException {

		GuardarInfoClienteDireccion_OUT guardarInfoClienteDireccion_OUT = new GuardarInfoClienteDireccion_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_CLIENTE_DIRECCION);){
			
			String codDep = guardarInfoClienteDireccion_IN.getIdUbigeo().substring(0,2);
			String codProv = guardarInfoClienteDireccion_IN.getIdUbigeo().substring(2,4);
			String codDist = guardarInfoClienteDireccion_IN.getIdUbigeo().substring(4);
			
			cs.setString(1, guardarInfoClienteDireccion_IN.getNro_documento());
			cs.setInt(2, guardarInfoClienteDireccion_IN.getId_cliente());
			cs.setString(3, guardarInfoClienteDireccion_IN.getDireccion());
			cs.setString(4, guardarInfoClienteDireccion_IN.getReferencia());
			cs.setInt(5, guardarInfoClienteDireccion_IN.getId_tipo_direccion());
			cs.setString(6, codDep);
			cs.setString(7, codProv);
			cs.setString(8, codDist);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.execute();

			guardarInfoClienteDireccion_OUT.setCodigo_validacion(cs.getString(9));
			guardarInfoClienteDireccion_OUT.setMensaje(cs.getString(10));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return guardarInfoClienteDireccion_OUT;
	}

	@Override
	public ListarDireccionesCliente_OUT listarDireccionesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {

		ListarDireccionesCliente_OUT listarDireccionesCliente_OUT = new ListarDireccionesCliente_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_DIRECCION_CLIENTE)) {

				ArrayList<ClienteDirecciones> listaClienteDirecciones = new ArrayList<ClienteDirecciones>();

				cs.setInt(1, obtenerInfoCliente_IN.getId_cliente());
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(4);) {
					while (rs.next()) {

						ClienteDirecciones cd = new ClienteDirecciones();
						cd.setId_direccion(rs.getInt("COD_CLIENTE_DIRECCION"));
						cd.setDireccion(rs.getString("TXT_DESCRIPCION"));
						cd.setReferencia(rs.getString("TXT_REFERENCIA"));
						cd.setId_tipo_direccion(rs.getInt("COD_PARAM_TIPO_DIRECCION"));
						cd.setTipo_direccion(rs.getString("PARAM_TIPO_DIRECCION"));
						cd.setId_departamento(rs.getString("COD_PARAM_DEPARTAMENTO"));
						cd.setDepartamento(rs.getString("PARAM_DEPARTAMENTO"));
						cd.setId_provincia(rs.getString("COD_PARAM_PROVINCIA"));
						cd.setProvincia(rs.getString("PARAM_PROVINCIA"));
						cd.setId_distrito(rs.getString("COD_PARAM_DISTRITO"));
						cd.setDistrito(rs.getString("PARAM_DISTRITO"));
						cd.setLatitud(rs.getString("DBL_LATITUD"));
						cd.setLongitud(rs.getString("DBL_LONGITUD"));
						listaClienteDirecciones.add(cd);
					}
				}

				listarDireccionesCliente_OUT.setCodigo_validacion(cs.getString(2));
				listarDireccionesCliente_OUT.setMensaje(cs.getString(3));
				listarDireccionesCliente_OUT.setCliente_direcciones(listaClienteDirecciones);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarDireccionesCliente_OUT;
	}

	@Override
	public GuardarCliente_OUT guardarCliente_OUT(GuardarCliente_IN guardarCliente_IN) throws SQLException {
		
		GuardarCliente_OUT guardarCliente_OUT = new GuardarCliente_OUT();
		Array lista;

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_CLIENTE)) {

				StructDescriptor structDesc = StructDescriptor.createDescriptor("TBL_CLIE_INFO_TYPE", connObj);
				ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("TBL_CLIE_INFO_TAB", connObj);

				if (guardarCliente_IN.getInformacion() != null) {
					Object[] array = new Object[guardarCliente_IN.getInformacion().size()];
					Object[] objeto = new Object[2];

					for (int i = 0; i < guardarCliente_IN.getInformacion().size(); i++) {
						objeto[0] = guardarCliente_IN.getInformacion().get(i).getIdTipoInfo();
						objeto[1] = guardarCliente_IN.getInformacion().get(i).getDescInfo();

						STRUCT oracleObjeto = new STRUCT(structDesc, connObj, objeto);

						array[i] = oracleObjeto;
					}
					lista = new ARRAY(arrDesc, connObj, array);
				} else {
					lista = new ARRAY(arrDesc, connObj, new Object[0]);
				}


				cs.setInt(1, guardarCliente_IN.getOrigen());
				cs.setString(2, guardarCliente_IN.getNro_documento());
				cs.setInt(3, guardarCliente_IN.getIdCliente());
				cs.setString(4, guardarCliente_IN.getNombres());
				cs.setString(5, guardarCliente_IN.getApePaterno());
				cs.setString(6, guardarCliente_IN.getApeMaterno());
				cs.setInt(7, guardarCliente_IN.getCodTipoDoc());
				cs.setString(8, guardarCliente_IN.getNroDocumentoCliente());
				cs.setString(9, guardarCliente_IN.getFechaCita());
				cs.setInt(10, guardarCliente_IN.getOrigen_cita());
				cs.setArray(11, lista);
				cs.registerOutParameter(12, OracleTypes.VARCHAR);
				cs.registerOutParameter(13, OracleTypes.VARCHAR);
				cs.registerOutParameter(14, OracleTypes.VARCHAR);
				cs.registerOutParameter(15, OracleTypes.VARCHAR);

				cs.execute();

				guardarCliente_IN.setIdCliente(Integer.parseInt(cs.getString(14).toString()));

				for (ClienteDireccionesGuardar cdg : guardarCliente_IN.getCliente_direcciones()) {

					try (CallableStatement cs2 = connObj.prepareCall(WebServicesConstants.SP_GUARDAR_CLIENTE_DIRECCION);) {

						cs2.setString(1, guardarCliente_IN.getNro_documento());
						cs2.setInt(2, guardarCliente_IN.getIdCliente());
						cs2.setString(3, cdg.getDireccion());
						cs2.setString(4, cdg.getReferencia());
						cs2.setInt(5, cdg.getId_tipo_direccion());
						cs2.setString(6, cdg.getId_departamento());
						cs2.setString(7, cdg.getId_provincia());
						cs2.setString(8, cdg.getId_distrito());
						cs2.registerOutParameter(9, OracleTypes.VARCHAR);
						cs2.registerOutParameter(10, OracleTypes.VARCHAR);
						cs2.execute();

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						throw e;
					}

				}

				guardarCliente_OUT.setCodigo_validacion(cs.getString(12));
				guardarCliente_OUT.setMensaje(cs.getString(13));
				guardarCliente_OUT.setIdCliente(Integer.parseInt(cs.getString(14)));
				guardarCliente_OUT.setIdGestion(Integer.parseInt(cs.getString(15)));

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		}

		return guardarCliente_OUT;
	}

	@Override
	public ListarBasesCliente_OUT listarBasesCliente_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException {

		ListarBasesCliente_OUT listarBasesCliente_OUT = new ListarBasesCliente_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_BASES_CLIENTE)) {

				ArrayList<BasesCliente> listaBasesCliente = new ArrayList<BasesCliente>();

				cs.setString(1, obtenerInfoCliente_IN.getNro_documento());
				cs.setInt(2, obtenerInfoCliente_IN.getId_cliente());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5);) {
					if (rs == null) {
						listarBasesCliente_OUT.setCodigo_validacion("0");
						listarBasesCliente_OUT.setMensaje("El cliente no califica a las bases del analista.");
						listarBasesCliente_OUT.setBasesCliente(listaBasesCliente);
					} else {
						while (rs.next()) {

							BasesCliente bc = new BasesCliente();
							bc.setIdGestion(rs.getInt("COD_GESTION"));
							bc.setBase(rs.getString("TXT_DESCRIPCION"));
							bc.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
							listaBasesCliente.add(bc);

						}

						listarBasesCliente_OUT.setCodigo_validacion(cs.getString(3));
						listarBasesCliente_OUT.setMensaje(cs.getString(4));
						listarBasesCliente_OUT.setBasesCliente(listaBasesCliente);
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarBasesCliente_OUT;
	}

	/**
	 * @return Lista los resultados de las citas
	 */
	@Override
	public ListarCitaResultado_OUT listarCitaResultado_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN)
			throws SQLException {
		
		ListarCitaResultado_OUT listarCitaResultado_OUT = new ListarCitaResultado_OUT();

		try (Connection cnn = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_CITA_RESULTADOS)) {

				ArrayList<CitaResultado> listaResultados = new ArrayList<CitaResultado>();

				cs.setInt(1, obtenerDetalleCita_IN.getId_cita());
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.registerOutParameter(7, OracleTypes.VARCHAR);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				cs.registerOutParameter(9, OracleTypes.VARCHAR);
				cs.registerOutParameter(10, OracleTypes.VARCHAR);
				cs.registerOutParameter(11, OracleTypes.VARCHAR);
				cs.registerOutParameter(12, OracleTypes.VARCHAR);
				cs.registerOutParameter(13, OracleTypes.VARCHAR);
				cs.registerOutParameter(14, OracleTypes.VARCHAR);
				cs.registerOutParameter(15, OracleTypes.VARCHAR);
				cs.registerOutParameter(16, OracleTypes.VARCHAR);
				cs.registerOutParameter(17, OracleTypes.VARCHAR);
				cs.registerOutParameter(18, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(18);) {
					while (rs.next()) {

						CitaResultado cr = new CitaResultado();
						cr.setIdCitaResultado(rs.getInt("COD_CITA_RESULTADO"));
						cr.setIdBase(rs.getInt("COD_GESTION"));
						cr.setBase(rs.getString("ACTIVIDAD_BASE"));
						cr.setIdColor(rs.getInt("ACTIVIDAD_COLOR"));
						cr.setResultCita(rs.getString("REACCION_CITA"));
						cr.setResultCitaDetalle(rs.getString("REACCION_DETALLE"));
						cr.setComentario(rs.getString("TXT_COMENTARIO"));
						cr.setFecCita(rs.getString("FEC_CITA"));
						listaResultados.add(cr);

					}
				}

				listarCitaResultado_OUT.setCodigo_validacion(cs.getString(2));
				listarCitaResultado_OUT.setMensaje(cs.getString(3));
				listarCitaResultado_OUT.setId_cita(cs.getInt(4));
				listarCitaResultado_OUT.setId_cliente(cs.getInt(5));
				listarCitaResultado_OUT.setNombres(cs.getString(6));
				listarCitaResultado_OUT.setApellido_paterno(cs.getString(7));
				listarCitaResultado_OUT.setApellido_materno(cs.getString(8));
				listarCitaResultado_OUT.setId_tipo_documento(cs.getInt(9));
				listarCitaResultado_OUT.setTipo_documento(cs.getString(10));
				listarCitaResultado_OUT.setNro_documento(cs.getString(11));
				listarCitaResultado_OUT.setFecha(cs.getString(12));
				listarCitaResultado_OUT.setId_direccion(cs.getInt(13));
				listarCitaResultado_OUT.setDireccion(cs.getString(14));
				listarCitaResultado_OUT.setReferencia_direccion(cs.getString(15));
				listarCitaResultado_OUT.setDistrito(cs.getString(16));
				listarCitaResultado_OUT.setEstado_cita(cs.getString(17));

				ObtenerInfoCliente_IN obtenerInfoCliente_IN = new ObtenerInfoCliente_IN(obtenerDetalleCita_IN.getNro_documento(), listarCitaResultado_OUT.getId_cliente());
				listarCitaResultado_OUT.setInformacion(listarClienteInfo_OUT(2, obtenerInfoCliente_IN).getInformacion());
				listarCitaResultado_OUT.setResultados(listaResultados);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCitaResultado_OUT;

	}	
	
	/**
	 * Realiza la baja de la informaci�n del cliente
	 */
	@Override
	public ResponseAgenda_OUT bajaInfoCliente_OUT(BajaInfoCliente_IN bajaInfoCliente_IN) throws SQLException {

		ResponseAgenda_OUT responseAgenda_OUT = new ResponseAgenda_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_BAJA_INFO_CLIENTE)) {

				cs.setInt(1, bajaInfoCliente_IN.getTipo());
				cs.setString(2, bajaInfoCliente_IN.getNroDocumento());
				cs.setInt(3, bajaInfoCliente_IN.getIdInfo());
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.execute();

				responseAgenda_OUT.setCodigo_validacion(cs.getString(4));
				responseAgenda_OUT.setMensaje(cs.getString(5));

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return responseAgenda_OUT;
	}
	
	/**
	 * @return Lista la informaci�n del cliente
	 */
	@Override
	public ListarClienteInfo_OUT listarClienteInfo_OUT(int tipo, ObtenerInfoCliente_IN obtenerInfoCliente_IN) 
			throws SQLException{

		ListarClienteInfo_OUT listarClienteInfo_OUT = new ListarClienteInfo_OUT();

		try (Connection cnn = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_INFO_CLIENTE)) {

				ArrayList<ClienteInfo> listaInfoCliente = new ArrayList<ClienteInfo>();

				cs.setInt(1, tipo);
				cs.setString(2, obtenerInfoCliente_IN.getNro_documento());
				cs.setInt(3, obtenerInfoCliente_IN.getId_cliente());
				cs.setInt(4, obtenerInfoCliente_IN.getIdGestion());
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.registerOutParameter(7, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(7)) {
					while (rs.next()) {

						ClienteInfo ci = new ClienteInfo();
						ci.setIdInfo(rs.getInt("COD_INFO"));
						ci.setIdTipoInfo(rs.getInt("COD_TIPO_INFO"));
						ci.setTipoInfo(rs.getString("TIPO_INFO"));
						ci.setDescInfo(rs.getString("DESC_INFO"));
						ci.setIdGroupInfo(rs.getString("GROUP_INFO"));
						ci.setIdActividad(rs.getInt("COD_ACTIVIDAD"));

						obtenerInfoCliente_IN.setIdGrupoInfo(ci.getIdGroupInfo());
						ci.setInfoDetalle(listarClienteInfoDetalle(obtenerInfoCliente_IN, cnn));

						listaInfoCliente.add(ci);

					}
				}

				listarClienteInfo_OUT.setCodigo_validacion(cs.getString(5));
				listarClienteInfo_OUT.setMensaje(cs.getString(6));
				listarClienteInfo_OUT.setInformacion(listaInfoCliente);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarClienteInfo_OUT;
	}

	public ArrayList<ClienteInfo> listarClienteInfo(int tipo, ObtenerInfoCliente_IN obtenerInfoCliente_IN, Connection cnn) 
			throws SQLException{
		//Connection cnn = jdbcTemplate.getDataSource().getConnection();

		ListarClienteInfo_OUT listarClienteInfo_OUT = new ListarClienteInfo_OUT();
		ArrayList<ClienteInfo> listaInfoCliente = new ArrayList<ClienteInfo>();
		
		try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_INFO_CLIENTE)){
			
			cs.setInt(1, tipo);
			cs.setString(2, obtenerInfoCliente_IN.getNro_documento());
			cs.setInt(3, obtenerInfoCliente_IN.getId_cliente());
			cs.setInt(4, obtenerInfoCliente_IN.getIdGestion());
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(7)){
				while (rs.next()) {

					ClienteInfo ci = new ClienteInfo();
					ci.setIdInfo(rs.getInt("COD_INFO"));
					ci.setIdTipoInfo(rs.getInt("COD_TIPO_INFO"));
					ci.setTipoInfo(rs.getString("TIPO_INFO"));
					ci.setDescInfo(rs.getString("DESC_INFO"));
					listaInfoCliente.add(ci);
			
				}
			}
			
			listarClienteInfo_OUT.setCodigo_validacion(cs.getString(5));
			listarClienteInfo_OUT.setMensaje(cs.getString(6));
			listarClienteInfo_OUT.setInformacion(listaInfoCliente);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listaInfoCliente;
	}

	public ArrayList<ClienteInfo> listarClienteInfoDetalle(ObtenerInfoCliente_IN obtenerInfoCliente_IN, Connection cnn)
			throws SQLException{
		//Connection cnn = jdbcTemplate.getDataSource().getConnection();

		ListarClienteInfo_OUT listarClienteInfo_OUT = new ListarClienteInfo_OUT();
		ArrayList<ClienteInfo> listaInfoCliente = new ArrayList<ClienteInfo>();

		try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_INFO_CLIENTE_DETALLE)){

			cs.setString(1, obtenerInfoCliente_IN.getNro_documento());
			cs.setInt(2, obtenerInfoCliente_IN.getId_cliente());
			cs.setInt(3, obtenerInfoCliente_IN.getIdGestion());
			cs.setString(4, obtenerInfoCliente_IN.getIdGrupoInfo());
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.registerOutParameter(7, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(7)){
				while (rs.next()) {

					ClienteInfo ci = new ClienteInfo();
					ci.setIdInfo(rs.getInt("COD_INFO"));
					ci.setTipoInfo(rs.getString("TIPO_INFO"));
					ci.setDescInfo(rs.getString("DESC_INFO"));
					ci.setIdGroupInfo(rs.getString("GROUP_INFO"));
					listaInfoCliente.add(ci);

				}
			}

			listarClienteInfo_OUT.setCodigo_validacion(cs.getString(5));
			listarClienteInfo_OUT.setMensaje(cs.getString(6));
			listarClienteInfo_OUT.setInformacion(listaInfoCliente);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listaInfoCliente;
	}

	/**
	 * @return Lista de indicadores de actividad para el calendario
	 */
	public ArrayList<IndicadorActividad> listarIndicadorActividad_OUT(ListarIndicadorActividad_IN listarIndicadorActividad_IN, Connection cnn) 
			throws SQLException{

		ArrayList<IndicadorActividad> listaIndicadores = new ArrayList<IndicadorActividad>();

		try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_INDICADOR_ACTIVIDAD);){

			cs.setString(1, listarIndicadorActividad_IN.getNro_documento());
			cs.setString(2, listarIndicadorActividad_IN.getFecha());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5);){
				while (rs.next()) {
					IndicadorActividad ia = new IndicadorActividad();
					ia.setIdColor(rs.getInt("COD_PARAM_COLOR"));
					ia.setGestionado(rs.getInt("INT_CANT_GESTIONADO"));
					
					listaIndicadores.add(ia);
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listaIndicadores;
		
	}


	/**
	 * @param tipo: Tipo de consulta
	 * @return: lista actividades del cliente seg�n el tipo
	 */
	public ArrayList<ClienteActividad> listarClienteActividad_OUT(int tipo, ObtenerInfoCliente_IN obtenerInfoCliente_IN, Connection cnn, int idGestion)
			throws SQLException{
		
		ArrayList<ClienteActividad> listaActividad = new ArrayList<ClienteActividad>();

		try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_ACTIVIDAD_CLIENTE)){

			cs.setInt(1, tipo);
			cs.setString(2, obtenerInfoCliente_IN.getNro_documento());
			cs.setInt(3, obtenerInfoCliente_IN.getId_cliente());
			cs.setInt(4, idGestion);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(5)){

				if (tipo == 1) {
					while (rs.next()) {

						ClienteActividad ca = new ClienteActividad();
						ca.setIdColor(rs.getInt("COD_PARAM_COLOR"));
						ca.setActividad(rs.getString("ACTIVIDAD"));
						listaActividad.add(ca);
				
					}
				} else if (tipo == 2 || tipo == 3 || tipo == 4) {
					while (rs.next()) {

						ClienteActividad ca = new ClienteActividad();
						ca.setIdColor(rs.getInt("COD_PARAM_COLOR"));
						ca.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
						ca.setActividad(rs.getString("ACTIVIDAD"));
						ca.setBases(rs.getString("BASES"));
						listaActividad.add(ca);
				
					}
				}

			}
						
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listaActividad;
	}

	@Override
	public ListarClienteTelef_OUT listarClienteTelef_OUT(ListarTelefCliente_IN listarTelefCliente_IN)
			throws SQLException {


		ListarClienteTelef_OUT listarClienteTelef_OUT = new ListarClienteTelef_OUT();

		try (Connection cnn = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_TELEF_CLIENTE)) {

				ArrayList<ClienteTelef> listaTelefCliente = new ArrayList<ClienteTelef>();

				cs.setString(1, listarTelefCliente_IN.getNroDocumento());
				cs.setInt(2, listarTelefCliente_IN.getIdCliente());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5);) {
					while (rs.next()) {

						ClienteTelef ci = new ClienteTelef();
						ci.setIdInfo(rs.getInt("COD_INFO"));
						ci.setIdTipoInfo(rs.getInt("TIPO"));
						ci.setDescInfo(rs.getString("INFO"));
						ci.setEstado(rs.getInt("ESTADO"));
						listaTelefCliente.add(ci);

					}

				}

				listarClienteTelef_OUT.setCodigo_validacion(cs.getString(3));
				listarClienteTelef_OUT.setMensaje(cs.getString(4));
				listarClienteTelef_OUT.setTelefonos(listaTelefCliente);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarClienteTelef_OUT;
	}

	@Override
	public ListarClienteDireccion_OUT listarClienteDireccion_OUT(ListarDireccCliente_IN listarDireccCliente_IN)
			throws SQLException {

		
		ListarClienteDireccion_OUT listarClienteDireccion_OUT = new ListarClienteDireccion_OUT();

		try (Connection cnn = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = cnn.prepareCall(WebServicesConstants.SP_LISTAR_DIRECC_CLIENTE)) {

				ArrayList<ClienteDireccion> listaDireccCliente = new ArrayList<ClienteDireccion>();

				cs.setString(1, listarDireccCliente_IN.getNroDocumento());
				cs.setInt(2, listarDireccCliente_IN.getIdCliente());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5);) {
					while (rs.next()) {

						ClienteDireccion cd = new ClienteDireccion();
						cd.setIdDireccion(rs.getInt("COD_DIRECCION"));
						cd.setIdTipo(rs.getInt("TIPO"));
						cd.setDireccion(rs.getString("DIRECCION"));
						cd.setReferencia(rs.getString("REFERENCIA"));
						cd.setUbigeo(rs.getString("UBIGEO"));
						cd.setEstado(rs.getInt("ESTADO"));
						listaDireccCliente.add(cd);

					}
				}

				listarClienteDireccion_OUT.setCodigo_validacion(cs.getString(3));
				listarClienteDireccion_OUT.setMensaje(cs.getString(4));
				listarClienteDireccion_OUT.setDirecciones(listaDireccCliente);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarClienteDireccion_OUT;
	}
	
	@Override
	public ListarCitaFotos_OUT listarCitaFotos_OUT(ObtenerDetalleCita_IN obtenerDetalleCita_IN) throws SQLException {

		ListarCitaFotos_OUT listarCitaFotos_OUT = new ListarCitaFotos_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CITA_FOTOS)) {

				ArrayList<CitaFoto> citaFotos = new ArrayList<CitaFoto>();

				cs.setInt(1, obtenerDetalleCita_IN.getId_cita());
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(4);) {
					while (rs.next()) {

						CitaFoto cf = new CitaFoto();
						cf.setIdFoto(rs.getInt("COD_FOTO"));

						if (rs.getBlob("FOTO") != null) {
							byte[] byteArray = rs.getBlob("FOTO").getBytes(1,
									(int) rs.getBlob("FOTO").length());
							String imageString = Base64.getEncoder().encodeToString(byteArray);
							cf.setArchivoFoto(imageString);
						} else {
							cf.setArchivoFoto(null);
						}

						citaFotos.add(cf);
					}
				}

				listarCitaFotos_OUT.setCodigo_validacion(cs.getString(2));
				listarCitaFotos_OUT.setMensaje(cs.getString(3));
				listarCitaFotos_OUT.setCitaFotos(citaFotos);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarCitaFotos_OUT;
	}

	@Override
	public ListarClienteResultado_OUT listarClienteResultado_OUT(ListarInfoCliente_IN listarInfoCliente_IN) throws SQLException {

		ListarClienteResultado_OUT listarClienteResultado_OUT = new ListarClienteResultado_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection()) {
			try (CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_CLIENTE_RESULTADO)) {

				ArrayList<ClienteResultado> resultados = new ArrayList<>();

				cs.setString(1, listarInfoCliente_IN.getNro_documento());
				cs.setInt(2, listarInfoCliente_IN.getId_cliente());
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.CURSOR);
				cs.execute();

				try (ResultSet rs = (ResultSet) cs.getObject(5)) {
					while (rs.next()) {

						ClienteResultado cr = new ClienteResultado();
						cr.setFecha(rs.getDate("FECHA"));
						cr.setOrigen(rs.getString("ORIGEN"));
						cr.setReaccion(rs.getString("REACCION"));
						cr.setReaccionDetalle(rs.getString("REACCION_DETALLE"));
						cr.setComentario(rs.getString("COMENTARIO"));
						cr.setUsuario(rs.getString("USUARIO"));

						resultados.add(cr);
					}
				}

				listarClienteResultado_OUT.setCodigo_validacion(cs.getString(3));
				listarClienteResultado_OUT.setMensaje(cs.getString(4));
				listarClienteResultado_OUT.setResultados(resultados);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarClienteResultado_OUT;
	}


}
