package com.orbita.agendaComercialWS.dao.impl;

import com.orbita.agendaComercialWS.dao.SimuladorDAO;
import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarTarifario_IN;
import com.orbita.agendaComercialWS.dto.response.ListarProductoSegmento_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarTarifario_OUT;
import com.orbita.agendaComercialWS.model.ComboString;
import com.orbita.agendaComercialWS.model.Tarifario;
import com.orbita.agendaComercialWS.util.WebServicesConstants;
import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class SimuladorDAOImpl implements SimuladorDAO {

    private static final Logger logger = LoggerFactory.getLogger(HomeDAOImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public ListarProductoSegmento_OUT listarProductoSegmento_OUT(General_IN general_IN) throws SQLException {

        ListarProductoSegmento_OUT listarProductoSegmento_OUT = new ListarProductoSegmento_OUT();

        try(Connection connObj = jdbcTemplate.getDataSource().getConnection();
            CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_PRODUCTO_SEGMENTO)){

            ArrayList<ComboString> productos = new ArrayList<>();
            ArrayList<ComboString> segmentos = new ArrayList<>();

            cs.setString(1, general_IN.getNro_documento());
            cs.registerOutParameter(2, OracleTypes.VARCHAR);
            cs.registerOutParameter(3, OracleTypes.VARCHAR);
            cs.registerOutParameter(4, OracleTypes.CURSOR);
            cs.execute();

            try (ResultSet rs = (ResultSet) cs.getObject(4)){
                while (rs.next()) {

                    ComboString cmb = new ComboString();

                    if (rs.getString("TIPO").equals("P")){
                        cmb.setDescripcion(rs.getString("DESCRIPCION"));
                        productos.add(cmb);
                    } else {
                        cmb.setDescripcion(rs.getString("DESCRIPCION"));
                        segmentos.add(cmb);
                    }
                }

            }

            listarProductoSegmento_OUT.setCodigo_validacion(cs.getString(2));
            listarProductoSegmento_OUT.setMensaje(cs.getString(3));
            listarProductoSegmento_OUT.setProductos(productos);
            listarProductoSegmento_OUT.setSegmentos(segmentos);

        } catch (Exception e){
            logger.error(e.getMessage(), e);
            throw e;
        }

        return listarProductoSegmento_OUT;
    }

    @Override
    public ListarTarifario_OUT listarTarifario_OUT(ListarTarifario_IN listarTarifario_IN) throws SQLException {
        ListarTarifario_OUT listarTarifario_OUT = new ListarTarifario_OUT();

        try(Connection connObj = jdbcTemplate.getDataSource().getConnection();
            CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_TARIFARIO)){

            ArrayList<Tarifario> tarifario = new ArrayList<>();

            cs.setString(1, listarTarifario_IN.getNro_documento());
            cs.setInt(2, listarTarifario_IN.getMoneda());
            cs.setDouble(3, listarTarifario_IN.getMonto());
            cs.setString(4, listarTarifario_IN.getProducto());
            cs.setString(5, listarTarifario_IN.getSegmento());
            cs.registerOutParameter(6, OracleTypes.DATE);
            cs.registerOutParameter(7, OracleTypes.VARCHAR);
            cs.registerOutParameter(8, OracleTypes.VARCHAR);
            cs.registerOutParameter(9, OracleTypes.CURSOR);
            cs.execute();

            try (ResultSet rs = (ResultSet) cs.getObject(9)){
                while (rs.next()) {

                    Tarifario tar = new Tarifario();
                    tar.setMonto(rs.getString("MONTO"));
                    tar.setPlazo(rs.getString("PLAZO"));
                    tar.setTasa(rs.getDouble("TASA"));

                    tarifario.add(tar);
                }

            }

            listarTarifario_OUT.setFecha(cs.getDate(6));
            listarTarifario_OUT.setCodigo_validacion(cs.getString(7));
            listarTarifario_OUT.setMensaje(cs.getString(8));
            listarTarifario_OUT.setTarifario(tarifario);

        } catch (Exception e){
            logger.error(e.getMessage(), e);
            throw e;
        }

        return listarTarifario_OUT;
    }
}
