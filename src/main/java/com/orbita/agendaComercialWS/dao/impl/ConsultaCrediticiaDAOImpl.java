package com.orbita.agendaComercialWS.dao.impl;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import com.orbita.agendaComercialWS.dto.request.AgendarCitaMasivo_IN;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.ConsultaCrediticiaDAO;
import com.orbita.agendaComercialWS.dto.request.AgendarCita_IN;
import com.orbita.agendaComercialWS.dto.request.ValidarCliente_IN;
import com.orbita.agendaComercialWS.dto.response.AgendarCita_OUT;
import com.orbita.agendaComercialWS.dto.response.ValidarCliente_OUT;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class ConsultaCrediticiaDAOImpl implements ConsultaCrediticiaDAO {

	private static final Logger logger = LoggerFactory.getLogger(ConsultaCrediticiaDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public AgendarCita_OUT agendarCita_OUT(AgendarCita_IN agendarCita_IN) throws SQLException {

		AgendarCita_OUT agendarCita_OUT = new AgendarCita_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_AGENDAR_CITA);){
			
			cs.setString(1, agendarCita_IN.getNro_documento());
			cs.setString(2, agendarCita_IN.getId_usuario_session());
			cs.setInt(3, agendarCita_IN.getId_cliente());
			cs.setInt(4, agendarCita_IN.getIdGestion());
			cs.setString(5, agendarCita_IN.getFecha());
			cs.setInt(6, agendarCita_IN.getOrigen());
			cs.setInt(7, agendarCita_IN.getNuevaCita());
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.execute();

			agendarCita_OUT.setCodigo_validacion(cs.getString(8));
			agendarCita_OUT.setMensaje(cs.getString(9));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return agendarCita_OUT;
	}

	@Override
	public AgendarCita_OUT agendarCitaMasivo_OUT(AgendarCitaMasivo_IN agendarCitaMasivo_IN) throws SQLException {
		AgendarCita_OUT agendarCita_OUT = new AgendarCita_OUT();
		Array lista;

		try (Connection connObj = Objects.requireNonNull(jdbcTemplate.getDataSource()).getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_AGENDAR_CITA_MASIVO)){

			StructDescriptor structDesc = StructDescriptor.createDescriptor("TP_AGENDAR_CITA", connObj);
			ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("TP_AGENDAR_CITA_TAB", connObj);

			if (agendarCitaMasivo_IN.getCitas() != null) {
				Object[] array = new Object[agendarCitaMasivo_IN.getCitas().size()];
				Object[] objeto = new Object[5];

				for (int i = 0; i < agendarCitaMasivo_IN.getCitas().size(); i++) {
					objeto[0] = agendarCitaMasivo_IN.getCitas().get(i).getId_cliente();
					objeto[1] = agendarCitaMasivo_IN.getCitas().get(i).getIdGestion();
					objeto[2] = agendarCitaMasivo_IN.getCitas().get(i).getFecha();
					objeto[3] = agendarCitaMasivo_IN.getCitas().get(i).getOrigen();
					objeto[4] = agendarCitaMasivo_IN.getCitas().get(i).getNuevaCita();

					STRUCT oracleObjeto = new STRUCT(structDesc, connObj, objeto);

					array[i] = oracleObjeto;
				}
				lista = new ARRAY(arrDesc, connObj, array);
			} else {
				lista = new ARRAY(arrDesc, connObj, new Object[0]);
			}

			cs.setString(1, agendarCitaMasivo_IN.getNro_documento());
			cs.setArray(2, lista);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();

			agendarCita_OUT.setCodigo_validacion(cs.getString(3));
			agendarCita_OUT.setMensaje(cs.getString(4));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return agendarCita_OUT;
	}

	@Override
	public ValidarCliente_OUT validarCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException {

		ValidarCliente_OUT validarCliente_OUT = new ValidarCliente_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_VALIDAR_CLIENTE);){

			cs.setString(1, validarCliente_IN.getNro_documento());
			cs.setString(2, validarCliente_IN.getId_usuario_session());
			cs.setString(3, validarCliente_IN.getNro_documento_cliente());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			cs.execute();

			validarCliente_OUT.setCodigo_validacion(cs.getString(4));
			validarCliente_OUT.setMensaje(cs.getString(5));
			validarCliente_OUT.setId_cliente(cs.getInt(6));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return validarCliente_OUT;
	}

	@Override
	public ValidarCliente_OUT validarEjecutivoCliente_OUT(ValidarCliente_IN validarCliente_IN) throws SQLException {
		
		ValidarCliente_OUT validarCliente_OUT = new ValidarCliente_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_VALIDAR_EJECUTIVO_CLIENTE);){
			
			cs.setString(1, validarCliente_IN.getNro_documento());
			cs.setString(2, validarCliente_IN.getNro_documento_cliente());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.INTEGER);
			cs.registerOutParameter(6, OracleTypes.INTEGER);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.execute();

			validarCliente_OUT.setCodigo_validacion(cs.getString(3));
			validarCliente_OUT.setMensaje(cs.getString(4));
			validarCliente_OUT.setId_cliente(cs.getInt(5));
			validarCliente_OUT.setIdBase(cs.getInt(6));
			validarCliente_OUT.setIdGestion(cs.getInt(7));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return validarCliente_OUT;
	}

}
