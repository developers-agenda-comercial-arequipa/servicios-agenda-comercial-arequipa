package com.orbita.agendaComercialWS.dao.impl;

import java.io.InputStream;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orbita.agendaComercialWS.dao.HomeDAO;
import com.orbita.agendaComercialWS.dto.request.GuardarError_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMonitoreoCliente_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerContadorCitas_IN;
import com.orbita.agendaComercialWS.dto.request.Version_IN;
import com.orbita.agendaComercialWS.dto.response.ListarMonitoreoCliente_OUT;
import com.orbita.agendaComercialWS.dto.response.ObtenerContadorCitas_OUT;
import com.orbita.agendaComercialWS.dto.response.ResponseAgenda_OUT;
import com.orbita.agendaComercialWS.dto.response.Version_OUT;
import com.orbita.agendaComercialWS.model.MonitoreoMapa;
import com.orbita.agendaComercialWS.util.WebServicesConstants;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

@Repository
public class HomeDAOImpl implements HomeDAO {

	private static final Logger logger = LoggerFactory.getLogger(HomeDAOImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public ObtenerContadorCitas_OUT obtenerContadorCitas_OUT(ObtenerContadorCitas_IN obtenerContadorCitas_IN)
			throws SQLException {

		ObtenerContadorCitas_OUT obtenerContadorCitas_OUT = new ObtenerContadorCitas_OUT();

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_OBTENER_CONTADOR_CITAS);){
			
			cs.setString(1, obtenerContadorCitas_IN.getNro_documento());
			cs.setString(2, obtenerContadorCitas_IN.getId_usuario_session());
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.INTEGER);

			cs.execute();

			obtenerContadorCitas_OUT.setCodigo_validacion(cs.getString(3));
			obtenerContadorCitas_OUT.setMensaje(cs.getString(4));
			obtenerContadorCitas_OUT.setCantidad_citas(cs.getInt(5));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return obtenerContadorCitas_OUT;
	}
	
	@Override
	public ListarMonitoreoCliente_OUT listarMonitoreoCliente_OUT(ListarMonitoreoCliente_IN listarMonitoreoCliente_IN)
			throws SQLException {

		ListarMonitoreoCliente_OUT listarMonitoreoCliente_OUT = new ListarMonitoreoCliente_OUT();
		
		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_LISTAR_MONITOREO_CLIENTE);){
			
			ArrayList<MonitoreoMapa> listaMonitoreoCliente = new ArrayList<MonitoreoMapa>();
			
			cs.setString(1, listarMonitoreoCliente_IN.getNro_documento());
			cs.setDouble(2, listarMonitoreoCliente_IN.getLatActual());
			cs.setDouble(3, listarMonitoreoCliente_IN.getLonActual());
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();

			try (ResultSet rs = (ResultSet) cs.getObject(6);){
				while (rs.next()) {
					
					MonitoreoMapa cm = new MonitoreoMapa();
					cm.setIdCliente(rs.getInt("COD_CLIENTE"));
					cm.setCliente(rs.getString("CLIENTE"));
					cm.setIdActividad(rs.getInt("COD_ACTIVIDAD"));
					cm.setActividad(rs.getString("ACTIVIDAD"));
					cm.setIdColorActividad(rs.getInt("COLOR_ACTIVIDAD"));
					cm.setOrigen(rs.getInt("ORIGEN"));
					
					listaMonitoreoCliente.add(cm);
				}

			}
			
			listarMonitoreoCliente_OUT.setCodigo_validacion(cs.getString(4));
			listarMonitoreoCliente_OUT.setMensaje(cs.getString(5));
			listarMonitoreoCliente_OUT.setMonitoreoCliente(listaMonitoreoCliente);
			/*listarMonitoreoCliente_OUT.setCodigo_validacion("1");
			listarMonitoreoCliente_OUT.setMensaje(" ");
			listarMonitoreoCliente_OUT.setMonitoreoCliente(listaMonitoreoCliente);*/

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

		return listarMonitoreoCliente_OUT;
	}
	
	@Override
	public ResponseAgenda_OUT GuardarError(GuardarError_IN guardarError_IN) throws SQLException {

		try (Connection connObj = jdbcTemplate.getDataSource().getConnection();
			 CallableStatement cs = connObj.prepareCall(WebServicesConstants.SP_G_AUDT_ERRORES);){

			StructDescriptor structDesc = StructDescriptor.createDescriptor("TBL_AUDT_ERRORS_TYPE", connObj);
			ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("TBL_AUDT_ERRORS_TAB", connObj);
			
			Object[] array = new Object[guardarError_IN.getErrores().size()];
            Object[] objeto = new Object[6];

            for (int i = 0; i< guardarError_IN.getErrores().size(); i++) {
                objeto[0] = guardarError_IN.getErrores().get(i).getEstadoCnn();
            	objeto[1] = guardarError_IN.getErrores().get(i).getFecHoraError();
                objeto[2] = guardarError_IN.getErrores().get(i).getIdError();
                objeto[3] = guardarError_IN.getErrores().get(i).getDescripcionError();
                objeto[4] = guardarError_IN.getErrores().get(i).getUltimaAccion();
                objeto[5] = guardarError_IN.getErrores().get(i).getUserApp();
                
                STRUCT oracleObjeto = new STRUCT(structDesc, connObj, objeto);

                array[i] = oracleObjeto;
            }
			
            Array lista = new ARRAY(arrDesc, connObj, array);
            
			cs.setObject(1, lista);
			cs.execute();
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		
		return null;
		
	}
	
	@Override
	public Version_OUT version_OUT(Version_IN version_IN) throws SQLException {
		Version_OUT version_OUT = new Version_OUT();
		
		Properties prop = new Properties();
		String propFileName = "config.properties";
		
		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			prop.load(inputStream);
				
			String version = prop.getProperty("app_version");
			String idVal = prop.getProperty("app_idval");
				
			if (version_IN.getIdVal().equalsIgnoreCase(idVal)) {
				version_OUT.setVersion(version);
			} else {
				version_OUT.setVersion("Código de validación errado.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return version_OUT;
	}

}
