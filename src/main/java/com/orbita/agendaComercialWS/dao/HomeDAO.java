package com.orbita.agendaComercialWS.dao;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.GuardarError_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMonitoreoCliente_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerContadorCitas_IN;
import com.orbita.agendaComercialWS.dto.request.Version_IN;
import com.orbita.agendaComercialWS.dto.response.ListarMonitoreoCliente_OUT;
import com.orbita.agendaComercialWS.dto.response.ObtenerContadorCitas_OUT;
import com.orbita.agendaComercialWS.dto.response.ResponseAgenda_OUT;
import com.orbita.agendaComercialWS.dto.response.Version_OUT;

public interface HomeDAO {
	public ObtenerContadorCitas_OUT obtenerContadorCitas_OUT(ObtenerContadorCitas_IN obtenerContadorCitas_IN)
			throws SQLException;
	
	public ListarMonitoreoCliente_OUT listarMonitoreoCliente_OUT(ListarMonitoreoCliente_IN listarMonitoreoCliente_IN)
			throws SQLException;
	
	public ResponseAgenda_OUT GuardarError(GuardarError_IN guardarError_IN)
			throws SQLException;
	
	public Version_OUT version_OUT(Version_IN version_IN)
			throws SQLException;
}