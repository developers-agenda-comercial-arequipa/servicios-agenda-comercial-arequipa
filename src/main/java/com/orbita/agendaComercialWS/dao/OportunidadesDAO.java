package com.orbita.agendaComercialWS.dao;

import java.sql.SQLException;

import com.orbita.agendaComercialWS.dto.request.General_IN;
import com.orbita.agendaComercialWS.dto.request.ListarMiRadar_IN;
import com.orbita.agendaComercialWS.dto.request.ObtenerInfoCliente_IN;
import com.orbita.agendaComercialWS.dto.response.ListarActividadOportunidad_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarBasesOportunidad_OUT;
import com.orbita.agendaComercialWS.dto.response.ListarCarteraInactivo_OUT;

public interface OportunidadesDAO {
	public ListarCarteraInactivo_OUT listarCarteraInactivo_OUT(ListarMiRadar_IN listarMiRadar_IN)
			throws SQLException;
	
	public ListarBasesOportunidad_OUT listarBasesOportunidad_OUT(ObtenerInfoCliente_IN obtenerInfoCliente_IN)
			throws SQLException;

	public ListarActividadOportunidad_OUT listarActividadOportunidad_OUT(General_IN general_IN)
			throws SQLException;
}
