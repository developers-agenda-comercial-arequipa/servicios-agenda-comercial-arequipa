package com.orbita.agendaComercialWS.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.orbita.agendaComercialWS.dto.request.*;
import com.orbita.agendaComercialWS.dto.response.*;
import com.orbita.agendaComercialWS.model.Base;

public interface ReportesDAO {
	public ReporteAsignados_OUT reporteAsignados_OUT(ReporteAsignados_IN reporteAsignados_IN)
			throws SQLException;
	
	public ReporteGestion_OUT reporteGestion_OUT(ReporteGestion_IN reporteGestion_IN) 
			throws SQLException;
	
	public ListarActividad_OUT listarActividad_OUT(ListarBasesDetalle_IN listarBasesDetalle_IN)
			throws SQLException;

	public ListarBasesOUT listarBase_OUT(ListarBases_IN listarBases_in) throws SQLException;

	public ListarReportes_OUT listarReportes_OUT(General_IN general_IN)
			throws SQLException;

	public ObtenerReporte_OUT obtenerReporte_OUT(ObtenerReporte_IN obtenerReporte_IN)
			throws SQLException;
	
}
